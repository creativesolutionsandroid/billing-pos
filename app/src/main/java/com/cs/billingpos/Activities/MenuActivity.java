package com.cs.billingpos.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.cs.billingpos.Adapters.MenuAdapter;
import com.cs.billingpos.Adapters.MenuCheckoutAdapter;
import com.cs.billingpos.Models.MenuItems;
import com.cs.billingpos.Models.Order;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.MyDatabase;
import com.cs.billingpos.R;
import com.cs.billingpos.Utils.Constants;
import com.cs.billingpos.Utils.CustomListView;

import java.util.ArrayList;

public class MenuActivity extends Activity {

    private LinearLayout categoryList;
    private ListView menuListView;
    CustomListView checkoutListView;
    private MenuAdapter menuAdapter;
    private ArrayList<RoutePlanDetailsResponse.SKUlist> skUlists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList> nonTobaccoSKULists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.RoutePlanDetail> customersLists = new ArrayList<>();
    private ArrayList<MenuItems> menuItemsList = new ArrayList<>();
    private ArrayList<String> categoryNames = new ArrayList<>();
    private ArrayList<Order> orderList = new ArrayList<>();
    private int categorySelected = 0;
    private String TAG = "TAG";
    private MyDatabase myDbHelper;
    private LinearLayout checkoutLayout, checkOutItemsLayout;
    private TextView subTotaltv, vatTv, netAmountTv;
    private Button checkOutBtn;
    private ImageView backBtn;
    private int customerPos;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        customerPos = getIntent().getIntExtra("customerpos", 0);
        customersLists = (ArrayList<RoutePlanDetailsResponse.RoutePlanDetail>) getIntent().getSerializableExtra("customer");
        skUlists = (ArrayList<RoutePlanDetailsResponse.SKUlist>) getIntent().getSerializableExtra("sku");
        nonTobaccoSKULists = (ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList>) getIntent().getSerializableExtra("non_tobacco");
        myDbHelper = new MyDatabase(MenuActivity.this);

        categoryList = (LinearLayout) findViewById(R.id.categories);
        checkoutLayout = (LinearLayout) findViewById(R.id.checkout_layout);
        checkOutItemsLayout = (LinearLayout) findViewById(R.id.checkout_items_layout);
        menuListView = (ListView) findViewById(R.id.menu_list);
        checkoutListView = (CustomListView) findViewById(R.id.checkout_list);

        subTotaltv = (TextView) findViewById(R.id.sub_total);
        vatTv = (TextView) findViewById(R.id.vat);
        netAmountTv = (TextView) findViewById(R.id.grand_total);
        checkOutBtn = (Button) findViewById(R.id.checkout_btn);
        backBtn = (ImageView) findViewById(R.id.back_btn);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        checkOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, CheckOutActivity.class);
                intent.putExtra("customer", customersLists);
                intent.putExtra("customerpos", customerPos);
                intent.putExtra("sku", skUlists);
                intent.putExtra("non_tobacco", nonTobaccoSKULists);
                startActivity(intent);
            }
        });

        broadcastReceiverSetupForSession();
        prepareMenu();
    }

    private void setCategoryData(){
        Log.d(TAG, "setCategoryData: "+menuItemsList.get(categorySelected).getSubCategory().size());
        menuAdapter = new MenuAdapter(MenuActivity.this, menuItemsList.get(categorySelected).getSubCategory(),
                menuItemsList.get(categorySelected).getSKUCategoryId(), customersLists.get(customerPos).getOutletName());
        menuListView.setAdapter(menuAdapter);
        menuAdapter.notifyDataSetChanged();

        categoryList.removeAllViews();
        for (int i = 0; i < categoryNames.size(); i++) {
            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.menu_category_list, null);
            TextView button = v.findViewById(R.id.category_name);
            button.setText(categoryNames.get(i));

            if(categorySelected == i){
                button.setTextColor(getResources().getColor(R.color.menu_light_blue));
                button.setBackground(getResources().getDrawable(R.drawable.seletedshape));
            }
            else {
                button.setTextColor(getResources().getColor(R.color.billing_grey_text_color));
                button.setBackground(getResources().getDrawable(R.drawable.unseletedshape));
            }

            final int finalI = i;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categorySelected = finalI;
                    setCategoryData();
                }
            });
            categoryList.addView(v);
        }
    }

    private void prepareMenu(){
        categoryNames.clear();
        menuItemsList.clear();
        for (int i = 0; i < skUlists.size(); i++){
            boolean isCategoryAdded = false;
            for (int c = 0; c < categoryNames.size(); c++){
                if(categoryNames.get(c).equals(skUlists.get(i).getSKUCategoryName())){
                    isCategoryAdded = true;
                }
            }
            if(!isCategoryAdded){
                categoryNames.add(skUlists.get(i).getSKUCategoryName());

                MenuItems menuItems = new MenuItems();
                menuItems.setSKUCategoryId(skUlists.get(i).getSKUCategoryId());
                menuItems.setSKUCategoryName(skUlists.get(i).getSKUCategoryName());
                ArrayList<MenuItems.SubCategory> subCategories = new ArrayList<>();
                ArrayList<String> addedSubC = new ArrayList<>();
                for (int j = 0; j < skUlists.size(); j++) {
                    boolean isComp = false;
                    for (int c = 0; c < addedSubC.size(); c++){
                        if(addedSubC.get(c).equals(skUlists.get(j).getCompanyName())){
                            isComp = true;
                        }
                    }
                    if ((skUlists.get(i).getSKUCategoryName().equals(skUlists.get(j).getSKUCategoryName())) &&
                            !isComp) {
                        addedSubC.add(skUlists.get(j).getCompanyName());
                        MenuItems.SubCategory subCategory = new MenuItems.SubCategory();
                        subCategory.setCompanyId(skUlists.get(j).getCompanyId());
                        subCategory.setCompanyName(skUlists.get(j).getCompanyName());
                        ArrayList<MenuItems.Items> itemsList = new ArrayList<>();
                        for (int k = 0; k < skUlists.size(); k++) {
                            if (skUlists.get(j).getCompanyName().equals(skUlists.get(k).getCompanyName())) {
                                MenuItems.Items items = new MenuItems.Items();
                                items.setSKUId(skUlists.get(k).getSKUId());
                                items.setSKUName(skUlists.get(k).getSKUName());
                                items.setPackImage(skUlists.get(k).getPackImage());
                                items.setPackBalance(skUlists.get(k).getPackBalance());
                                items.setPacksPerCarton(skUlists.get(k).getPacksPerCarton());
                                items.setCartonBalance(skUlists.get(k).getCartonBalance());
                                items.setPackPrice(skUlists.get(k).getPackPrice());
                                items.setCartonPrice(skUlists.get(k).getCartonPrice());
                                itemsList.add(items);
                            }
                        }
                        subCategory.setItems(itemsList);
                        subCategories.add(subCategory);
                    }
                }
                menuItems.setSubCategory(subCategories);
                menuItemsList.add(menuItems);
            }
        }
        for (int i = 0; i < nonTobaccoSKULists.size(); i++){
            boolean isCategoryAdded = false;
            for (int c = 0; c < categoryNames.size(); c++){
                if(categoryNames.get(c).equals(nonTobaccoSKULists.get(i).getSKUCategoryName())){
                    isCategoryAdded = true;
                }
            }
            if(!isCategoryAdded){
                categoryNames.add(nonTobaccoSKULists.get(i).getSKUCategoryName());

                MenuItems menuItems = new MenuItems();
                menuItems.setSKUCategoryId(nonTobaccoSKULists.get(i).getCategoryId());
                menuItems.setSKUCategoryName(nonTobaccoSKULists.get(i).getSKUCategoryName());
                ArrayList<MenuItems.SubCategory> subCategories = new ArrayList<>();
                ArrayList<String> addedSubC = new ArrayList<>();
                for (int j = 0; j < nonTobaccoSKULists.size(); j++) {
                    boolean isComp = false;
                    for (int c = 0; c < addedSubC.size(); c++){
                        if(addedSubC.get(c).equals(nonTobaccoSKULists.get(j).getCompanyName())){
                            isComp = true;
                        }
                    }
                    if ((nonTobaccoSKULists.get(i).getSKUCategoryName().equals(nonTobaccoSKULists.get(j).getSKUCategoryName())) &&
                            !isComp) {
                        addedSubC.add(nonTobaccoSKULists.get(j).getCompanyName());
                        MenuItems.SubCategory subCategory = new MenuItems.SubCategory();
//                        subCategory.setCompanyId(nonTobaccoSKULists.get(j).getCompanyId());
                        subCategory.setCompanyName(nonTobaccoSKULists.get(j).getCompanyName());
                        ArrayList<MenuItems.Items> itemsList = new ArrayList<>();
                        for (int k = 0; k < nonTobaccoSKULists.size(); k++) {
                            if (nonTobaccoSKULists.get(j).getCompanyName().equals(nonTobaccoSKULists.get(k).getCompanyName())) {
                                MenuItems.Items items = new MenuItems.Items();
                                items.setSKUId(nonTobaccoSKULists.get(k).getSKUId());
                                items.setSKUName(nonTobaccoSKULists.get(k).getSKUName());
                                items.setPackImage(nonTobaccoSKULists.get(k).getPackImage());
                                items.setPackBalance(nonTobaccoSKULists.get(k).getPackBalance());
                                items.setCartonBalance(nonTobaccoSKULists.get(k).getCartonBalance());
                                items.setPackPrice(nonTobaccoSKULists.get(k).getPackPrice());
                                items.setCartonPrice(nonTobaccoSKULists.get(k).getCartonPrice());
                                itemsList.add(items);
                            }
                        }
                        subCategory.setItems(itemsList);
                        subCategories.add(subCategory);
                    }
                }
                menuItems.setSubCategory(subCategories);
                menuItemsList.add(menuItems);
            }
        }

        setCategoryData();
    }

    private void broadcastReceiverSetupForSession(){
        LocalBroadcastManager.getInstance(MenuActivity.this).registerReceiver(
                mSessionReceiver, new IntentFilter("Session"));
    }

    private BroadcastReceiver mSessionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            orderList = myDbHelper.getOrderInfo();
            if(orderList.size() > 0){
                checkOutItemsLayout.setVisibility(View.VISIBLE);
                checkoutLayout.setVisibility(View.VISIBLE);

                MenuCheckoutAdapter menuCheckoutAdapter = new MenuCheckoutAdapter(MenuActivity.this, orderList, menuItemsList.get(categorySelected).getSubCategory(), menuItemsList.get(categorySelected).getSKUCategoryId(), customersLists.get(customerPos).getOutletName());
                checkoutListView.setAdapter(menuCheckoutAdapter);
                menuCheckoutAdapter.notifyDataSetChanged();
                menuAdapter.notifyDataSetChanged();

                float subTotal = myDbHelper.getTotalOrderPrice();
                double vat = subTotal * 0.05;
                double netAmount = subTotal + vat;

                subTotaltv.setText(Constants.priceFormat.format(subTotal));
                vatTv.setText(Constants.priceFormat.format(vat));
                netAmountTv.setText(Constants.priceFormat.format(netAmount));
            }
            else {
                checkOutItemsLayout.setVisibility(View.GONE);
                checkoutLayout.setVisibility(View.GONE);
            }
        }
    };

    @Override
    protected void onResume() {
//        myDbHelper.deleteOrderTable();
        super.onResume();
        orderList = myDbHelper.getOrderInfo();
        if(orderList.size() > 0){
            checkOutItemsLayout.setVisibility(View.VISIBLE);
            checkoutLayout.setVisibility(View.VISIBLE);

            MenuCheckoutAdapter menuCheckoutAdapter = new MenuCheckoutAdapter(MenuActivity.this, orderList, menuItemsList.get(categorySelected).getSubCategory(), menuItemsList.get(categorySelected).getSKUCategoryId(), customersLists.get(customerPos).getOutletName());
            checkoutListView.setAdapter(menuCheckoutAdapter);
            menuCheckoutAdapter.notifyDataSetChanged();

            float subTotal = myDbHelper.getTotalOrderPrice();
            double vat = subTotal * 0.05;
            double netAmount = subTotal + vat;

            subTotaltv.setText(Constants.priceFormat.format(subTotal));
            vatTv.setText(Constants.priceFormat.format(vat));
            netAmountTv.setText(Constants.priceFormat.format(netAmount));
        }
        else {
            checkOutItemsLayout.setVisibility(View.GONE);
            checkoutLayout.setVisibility(View.GONE);
        }
    }
}

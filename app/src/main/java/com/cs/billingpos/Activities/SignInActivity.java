package com.cs.billingpos.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.billingpos.Models.SignInResponce;
import com.cs.billingpos.R;
import com.cs.billingpos.Rest.APIInterface;
import com.cs.billingpos.Rest.ApiClient;
import com.cs.billingpos.Utils.Constants;
import com.cs.billingpos.Utils.NetworkUtil;

import java.io.IOException;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class SignInActivity extends Activity implements View.OnClickListener {

//    private TextView forgotPassword;
    private String strUserName, strPassword;
    private EditText inputUserName, inputPassword;
    private Button continueBtn;
    private SharedPreferences userPrefs;
    private SharedPreferences.Editor userPrefsEditor;
    private String TAG = "TAG";

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_activity);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        inputPassword= (EditText) findViewById(R.id.password);
        inputUserName = (EditText) findViewById(R.id.email);
        continueBtn = (Button) findViewById(R.id.getstart);
//        forgotPassword = (TextView) findViewById(R.id.forgotpassword);

//        forgotPassword.setOnClickListener(this);
        continueBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.getstart:
                if(validations()){
                    String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        signInApi();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
                break;

//            case R.id.forgotpassword:
//                Intent intent1 = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
//                startActivity(intent1);
//                break;
        }
    }

    private boolean validations(){
        strUserName = inputUserName.getText().toString();
        strPassword = inputPassword.getText().toString();

        if (strUserName.length() == 0) {
            inputUserName.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            return false;
        }
        else if (strPassword.length() == 0){
            inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            return false;
        }
        return true;
    }

    private void signInApi(){
        final ACProgressFlower dialog = new ACProgressFlower.Builder(SignInActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();;
        dialog.show();
        final String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);

        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<SignInResponce> call = apiService.signIn(strUserName, strPassword);
        call.enqueue(new Callback<SignInResponce>() {

            public void onResponse(Call<SignInResponce> call, Response<SignInResponce> response) {
                if(response.isSuccessful()){
                    SignInResponce registrationResponse = response.body();
                    try {
                        //status true case
                        String userId = registrationResponse.getSuccess().getEmpid();
                        userPrefsEditor.putString("EmpId", userId);
                        userPrefsEditor.commit();
                        Intent intent = new Intent(SignInActivity.this, DashboardActivity.class);
                        startActivity(intent);
                        finish();
                    } catch (Exception e) {
                        Constants.showOneButtonAlertDialog(registrationResponse.getFailure(),
                                getResources().getString(R.string.app_name), "ok", SignInActivity.this);
                    }

                    if(dialog != null)
                        dialog.dismiss();
                }
                else{
                    try {
                        Log.d(TAG, "onResponse: "+response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(dialog != null)
                        dialog.dismiss();
                    Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
            }

            public void onFailure(Call<SignInResponce> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    Toast.makeText(SignInActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

                if(dialog != null)
                    dialog.dismiss();
            }
        });
    }
}







package com.cs.billingpos.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.cs.billingpos.Adapters.MenuAdapter;
import com.cs.billingpos.Adapters.ProductAdapter;
import com.cs.billingpos.Models.MenuItems;
import com.cs.billingpos.Models.MenuItemsWithoutSub;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.R;

import java.util.ArrayList;

public class ProductActivity extends Activity {

    private ListView productListView;
    private ProductAdapter productAdapter;
    private ArrayList<RoutePlanDetailsResponse.SKUlist> skUlists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList> nonTobaccoSKULists = new ArrayList<>();
    private ArrayList<MenuItemsWithoutSub> menuItemsList = new ArrayList<>();
    ImageView back_btn;
    ImageView add;
    private LinearLayout categoryList;
    private ArrayList<String> categoryNames = new ArrayList<>();
    private int categorySelected = 0;
    boolean isMenuVisible = false;
    LinearLayout products;
    TextView addStock, addProduct;
    private ArrayList<RoutePlanDetailsResponse.CategoryList>  categoryLists = new ArrayList<>();


    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        categoryLists = (ArrayList<RoutePlanDetailsResponse.CategoryList>) getIntent().getSerializableExtra("catlist");
        skUlists = (ArrayList<RoutePlanDetailsResponse.SKUlist>) getIntent().getSerializableExtra("sku");
        nonTobaccoSKULists = (ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList>) getIntent().getSerializableExtra("non_tobacco");

        addStock = (TextView) findViewById(R.id.add_stock);
        addProduct = (TextView) findViewById(R.id.add_product);
        back_btn=(ImageView) findViewById(R.id.back_btn);
        categoryList = (LinearLayout) findViewById(R.id.categories);
        products = (LinearLayout) findViewById(R.id.products);
        productListView = (ListView) findViewById(R.id.product_List);
        add = (ImageView)findViewById(R.id.add) ;
        prepareMenu();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMenuVisible) {
                    isMenuVisible = false;
                    products.setVisibility(View.GONE);
                }
                else {
                    isMenuVisible = true;
                    products.setVisibility(View.VISIBLE);
                }
             }
        });

        addStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isMenuVisible = false;
                products.setVisibility(View.GONE);
                Intent intent = new Intent(ProductActivity.this, AddStockActivity.class);
                intent.putExtra("sku", skUlists);
                intent.putExtra("non_tobacco", nonTobaccoSKULists);

                startActivity(intent);
                finish();
            }
        });

        addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isMenuVisible = false;
                products.setVisibility(View.GONE);
                Intent intent = new Intent(ProductActivity.this, AddProductActivity.class);
                intent.putExtra("sku", skUlists);
                intent.putExtra("non_tobacco", nonTobaccoSKULists);
                intent.putExtra("catlist", categoryLists);

                startActivity(intent);
                finish();
            }
        });
    }
    private void setCategoryData(){
        Log.d("TAG", "categorySelected: "+categorySelected);
        productAdapter = new ProductAdapter(ProductActivity.this, menuItemsList.get(categorySelected).getItems());
        productListView.setAdapter(productAdapter);
        productAdapter.notifyDataSetChanged();

        categoryList.removeAllViews();
        for (int i = 0; i < categoryNames.size(); i++) {
            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.menu_category_list, null);
            TextView button = v.findViewById(R.id.category_name);
            button.setText(categoryNames.get(i));

            if(categorySelected == i){
                button.setTextColor(getResources().getColor(R.color.menu_light_blue));
                button.setBackground(getResources().getDrawable(R.drawable.seletedshape));
            }
            else {
                button.setTextColor(getResources().getColor(R.color.billing_grey_text_color));
                button.setBackground(getResources().getDrawable(R.drawable.unseletedshape));
            }

            final int finalI = i;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categorySelected = finalI;
                    setCategoryData();
                }
            });
            categoryList.addView(v);
        }
    }
    private void prepareMenu(){
        categoryNames.clear();
        menuItemsList.clear();
        for (int i = 0; i < skUlists.size(); i++){
            boolean isCategoryAdded = false;
            for (int c = 0; c < categoryNames.size(); c++){
                if(categoryNames.get(c).equals(skUlists.get(i).getSKUCategoryName())){
                    isCategoryAdded = true;
                }
            }
            if(!isCategoryAdded){
                categoryNames.add(skUlists.get(i).getSKUCategoryName());

                MenuItemsWithoutSub menuItems = new MenuItemsWithoutSub();
                menuItems.setSKUCategoryId(skUlists.get(i).getSKUCategoryId());
                menuItems.setSKUCategoryName(skUlists.get(i).getSKUCategoryName());
                        ArrayList<MenuItemsWithoutSub.Items> itemsList = new ArrayList<>();
                        for (int k = 0; k < skUlists.size(); k++) {
                            if (skUlists.get(i).getSKUCategoryName().equals(skUlists.get(k).getSKUCategoryName())) {
                                MenuItemsWithoutSub.Items items = new MenuItemsWithoutSub.Items();
                                items.setSKUId(skUlists.get(k).getSKUId());
                                items.setSKUName(skUlists.get(k).getSKUName());
                                items.setPackImage(skUlists.get(k).getPackImage());
                                items.setPackBalance(skUlists.get(k).getPackBalance());
                                items.setCartonBalance(skUlists.get(k).getCartonBalance());
                                items.setPackPrice(skUlists.get(k).getPackPrice());
                                items.setCartonPrice(skUlists.get(k).getCartonPrice());
                                itemsList.add(items);
                            }
                        }
                menuItems.setItems(itemsList);
                menuItemsList.add(menuItems);
            }
        }
        for (int i = 0; i < nonTobaccoSKULists.size(); i++){
            boolean isCategoryAdded = false;
            for (int c = 0; c < categoryNames.size(); c++){
                if(categoryNames.get(c).equals(nonTobaccoSKULists.get(i).getSKUCategoryName())){
                    isCategoryAdded = true;
                }
            }
            if(!isCategoryAdded){
                categoryNames.add(nonTobaccoSKULists.get(i).getSKUCategoryName());

                MenuItemsWithoutSub menuItems = new MenuItemsWithoutSub();
                menuItems.setSKUCategoryId(nonTobaccoSKULists.get(i).getSalesmanId());
                menuItems.setSKUCategoryName(nonTobaccoSKULists.get(i).getSKUCategoryName());
                ArrayList<MenuItemsWithoutSub.Items> itemsList = new ArrayList<>();
                for (int k = 0; k < nonTobaccoSKULists.size(); k++) {
                    if (nonTobaccoSKULists.get(i).getSKUCategoryName().equals(nonTobaccoSKULists.get(k).getSKUCategoryName())) {
                        MenuItemsWithoutSub.Items items = new MenuItemsWithoutSub.Items();
                        items.setSKUId(nonTobaccoSKULists.get(k).getSKUId());
                        items.setSKUName(nonTobaccoSKULists.get(k).getSKUName());
                        items.setPackImage(nonTobaccoSKULists.get(k).getPackImage());
                        items.setPackBalance(nonTobaccoSKULists.get(k).getPackBalance());
                        items.setCartonBalance(nonTobaccoSKULists.get(k).getCartonBalance());
                        items.setPackPrice(nonTobaccoSKULists.get(k).getPackPrice());
                        items.setCartonPrice(nonTobaccoSKULists.get(k).getCartonPrice());
                        itemsList.add(items);
                    }
                }
                menuItems.setItems(itemsList);
                menuItemsList.add(menuItems);
            }
        }

        Log.d("TAG", "prepareMenu: "+menuItemsList.size());
        setCategoryData();
    }




}

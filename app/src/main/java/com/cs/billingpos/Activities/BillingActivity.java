package com.cs.billingpos.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;

import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.cs.billingpos.Adapters.BillingCustomerAdapter;
import com.cs.billingpos.Models.Order;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.MyDatabase;
import com.cs.billingpos.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class BillingActivity extends Activity {

    private SharedPreferences userPrefs;
    private String TAG = "TAG";
    private ListView customerListView;
    private ArrayList<RoutePlanDetailsResponse.RoutePlanDetail> customersLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.SKUlist> skUlists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList> nonTobaccoSKULists = new ArrayList<>();
    private BillingCustomerAdapter mAdapter;
    private ImageView backBtn;
    private SharedPreferences.Editor userPrefsEditor;
    private MyDatabase myDbHelper;
    private ArrayList<Order> orderList = new ArrayList<>();
    AlertDialog clearAlert;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing);

        myDbHelper = new MyDatabase(BillingActivity.this);
        orderList = myDbHelper.getOrderInfo();

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        customersLists = (ArrayList<RoutePlanDetailsResponse.RoutePlanDetail>) getIntent().getSerializableExtra("customer");
        skUlists = (ArrayList<RoutePlanDetailsResponse.SKUlist>) getIntent().getSerializableExtra("sku");
        nonTobaccoSKULists = (ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList>) getIntent().getSerializableExtra("non_tobacco");

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        backBtn = (ImageView) findViewById(R.id.back_btn);

        customerListView = (ListView) findViewById(R.id.customers_list);
        mAdapter = new BillingCustomerAdapter(BillingActivity.this, customersLists);
        customerListView.setAdapter(mAdapter);

        customerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                userPrefsEditor.putString("dateofvisit", getDateofVisit());
                userPrefsEditor.putString("startdate", getStartDate());
                userPrefsEditor.commit();
                showMenuDialog(position);
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private String getDateofVisit(){
        String dateStr = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);

        Calendar calendar = Calendar.getInstance();
        dateStr = sdf.format(calendar.getTime());

        return dateStr;
    }

    private String getStartDate(){
        String dateStr = "";
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.US);

        Calendar calendar = Calendar.getInstance();
        dateStr = sdf.format(calendar.getTime());

        return dateStr;
    }

    private void showMenuDialog(final int position){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BillingActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.billing_alert_dialog;

        final View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        Button dialogbox1 =(Button) dialogView.findViewById(R.id.invoice_btn);
        Button dialogbox2 =(Button) dialogView.findViewById(R.id.collection_btn);

        ImageView cancel = (ImageView) dialogView.findViewById(R.id.close_popup);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
            }
        });

        dialogbox1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderList = myDbHelper.getOrderInfo();
                if(orderList.size() > 0) {
                    if(orderList.get(0).getOutletName().equals(customersLists.get(position).getOutletName())){
                        Intent intent = new Intent(BillingActivity.this, MenuActivity.class);
                        intent.putExtra("customer", customersLists);
                        intent.putExtra("customerpos", position);
                        intent.putExtra("sku", skUlists);
                        intent.putExtra("non_tobacco", nonTobaccoSKULists);
                        startActivity(intent);
                        finalCustomDialog.dismiss();
                    }
                    else {
                        showtwoButtonsAlertDialog(position);
                    }
                }
                else {
                    Intent intent = new Intent(BillingActivity.this, MenuActivity.class);
                    intent.putExtra("customer", customersLists);
                    intent.putExtra("customerpos", position);
                    intent.putExtra("sku", skUlists);
                    intent.putExtra("non_tobacco", nonTobaccoSKULists);
                    startActivity(intent);
                    finalCustomDialog.dismiss();
                }
            }
        });

        dialogbox2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BillingActivity.this, CollectionActivity.class);
                intent.putExtra("customer", customersLists);
                intent.putExtra("customerpos", position);
                intent.putExtra("sku", skUlists);
                intent.putExtra("non_tobacco", nonTobaccoSKULists);
                startActivity(intent);
                finalCustomDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void showtwoButtonsAlertDialog(int position) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BillingActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        final TextView title = (TextView) dialogView.findViewById(R.id.title);
        final TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        final TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        final TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

        title.setText("Billing Pos");
        yes.setText("Yes");
        no.setText("No");
        desc.setText("Another invoice is in process for "+orderList.get(0).getOutletName()+". Are you sure to clear previous Invoice?");

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDbHelper.deleteOrderTable();
                Intent intent = new Intent(BillingActivity.this, MenuActivity.class);
                intent.putExtra("customer", customersLists);
                intent.putExtra("customerpos", position);
                intent.putExtra("sku", skUlists);
                intent.putExtra("non_tobacco", nonTobaccoSKULists);
                startActivity(intent);
                clearAlert.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAlert.dismiss();
            }
        });

        clearAlert = dialogBuilder.create();
        clearAlert.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = clearAlert.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}

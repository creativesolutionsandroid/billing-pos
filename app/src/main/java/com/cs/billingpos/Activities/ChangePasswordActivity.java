package com.cs.billingpos.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.cs.billingpos.Models.ChangePasswordResponse;
import com.cs.billingpos.R;
import com.cs.billingpos.Rest.APIInterface;
import com.cs.billingpos.Rest.ApiClient;
import com.cs.billingpos.Utils.Constants;
import com.cs.billingpos.Utils.NetworkUtil;

import java.io.IOException;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends Activity {

    EditText oldpassword,newpassword,confirampassword;
    Button sumbmit;
    private String stroldpassword, strnewpassword,strconfirampassword;
    private String TAG = "TAG";
    SharedPreferences userPrefs;
    ImageView backbtn;
    String userId ;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password_activity);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("EmpId","0");

        backbtn = (ImageView) findViewById(R.id.back_btn);
        oldpassword =(EditText)findViewById(R.id.oldpassword);
        newpassword =(EditText)findViewById(R.id.newpassword);
        confirampassword =(EditText)findViewById(R.id.confirmpassword);
        sumbmit =(Button) findViewById(R.id.submit);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        sumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validations()){
                    String networkStatus = NetworkUtil.getConnectivityStatusString(ChangePasswordActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        passwordInApi();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
  }

    private boolean validations(){
        stroldpassword = oldpassword.getText().toString();
        strnewpassword = newpassword.getText().toString();
        strconfirampassword = confirampassword.getText().toString();

        if (stroldpassword.length() == 0) {
            oldpassword.setError("Please enter old password");
            return false;
        }
        else if (strnewpassword.length() == 0){
            newpassword.setError("Please enter new password");
            return false;
        }
        else if (strconfirampassword.length() == 0){
            confirampassword.setError("Please enter confirm password");
            return false;
        }
        else if (!strconfirampassword.equals(strnewpassword)){
            confirampassword.setError("Passwords not matched");
            return false;
        }else {

            return true;

        }
    }

    private void passwordInApi(){
        final ACProgressFlower dialog = new ACProgressFlower.Builder(ChangePasswordActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();;
        dialog.show();
        final String networkStatus = NetworkUtil.getConnectivityStatusString(ChangePasswordActivity.this);

        final APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<ChangePasswordResponse> call = apiService.changePassword(userId, stroldpassword, strnewpassword);
        call.enqueue(new Callback<ChangePasswordResponse>() {

            public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                if(response.isSuccessful()){
                    Log.d(TAG, "onResponse: "+response.message());
                    Log.d(TAG, "onResponse: "+ userId +" "+ stroldpassword +" " + strnewpassword);
                    ChangePasswordResponse registrationResponse = response.body();
                    try {
                        Log.d(TAG, "onResponse: "+registrationResponse.getSuccess());
                        if (registrationResponse.getSuccess().equals("Change Password successful")) {
                            Toast.makeText(ChangePasswordActivity.this, "Password changed successfully", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    } catch (Exception e) {
                        Constants.showOneButtonAlertDialog(registrationResponse.getFailure(),
                                getResources().getString(R.string.app_name), "ok", ChangePasswordActivity.this);
                    }

                    if(dialog != null)
                        dialog.dismiss();
                }
                else{
                    try {
                        Log.d(TAG, "onResponse: "+response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(dialog != null)
                        dialog.dismiss();
                    Toast.makeText(ChangePasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
            }

            public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    Toast.makeText(ChangePasswordActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(ChangePasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

                if(dialog != null)
                    dialog.dismiss();
            }
        });
    }
  }

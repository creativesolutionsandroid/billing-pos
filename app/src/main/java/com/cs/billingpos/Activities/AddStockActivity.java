package com.cs.billingpos.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.cs.billingpos.Adapters.AddStockAdapter;
import com.cs.billingpos.Models.InsertStockDetails;
import com.cs.billingpos.Models.Items;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.MyDatabase;
import com.cs.billingpos.R;
import com.cs.billingpos.Rest.APIInterface;
import com.cs.billingpos.Rest.ApiClient;
import com.cs.billingpos.Utils.Constants;
import com.cs.billingpos.Utils.NetworkUtil;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Puli on 23-04-2019.
 */

public class AddStockActivity extends Activity {

    private ListView productListView;
    private AddStockAdapter addStockAdapter;
    private ArrayList<RoutePlanDetailsResponse.SKUlist> skUlists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList> nonTobaccoSKULists = new ArrayList<>();
    private ArrayList<Items> menuItemsList = new ArrayList<>();
    ImageView back_btn;
    private LinearLayout categoryList;
    private ArrayList<String> categoryNames = new ArrayList<>();
    private int categorySelected = 0;
    Button savebtn;
    SharedPreferences userPrefs;
    private SharedPreferences.Editor userPrefsEditor;
    String userId ;
    private ArrayList<RoutePlanDetailsResponse.CategoryList>  categoryLists = new ArrayList<>();
    private MyDatabase myDbHelper;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_stock);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("EmpId","0");
        userPrefsEditor = userPrefs.edit();

        categoryLists = (ArrayList<RoutePlanDetailsResponse.CategoryList>) getIntent().getSerializableExtra("catlist");
        skUlists = (ArrayList<RoutePlanDetailsResponse.SKUlist>) getIntent().getSerializableExtra("sku");
        nonTobaccoSKULists = (ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList>) getIntent().getSerializableExtra("non_tobacco");
        myDbHelper = new MyDatabase(AddStockActivity.this);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        categoryList = (LinearLayout) findViewById(R.id.categories);
        productListView = (ListView) findViewById(R.id.product_List);
        productListView = (ListView) findViewById(R.id.addstocklist);
        savebtn = findViewById(R.id.save_btn);
        prepareMenu();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        savebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String networkStatus = NetworkUtil.getConnectivityStatusString(AddStockActivity.this);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                    new InsertStockDetail().execute();

                } else {
                    offlineDataSave();
//                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //    private void setCategoryData(){
//        Log.d("TAG", "categorySelected: "+categorySelected);
//        AddStockAdapter = new AddStockAdapter(AddStockActivity.this, menuItemsList.get(categorySelected).getItems());
//        productListView.setAdapter(AddStockAdapter);
//        AddStockAdapter.notifyDataSetChanged();
//
//        categoryList.removeAllViews();
//        for (int i = 0; i < categoryNames.size(); i++) {
//            LayoutInflater inflater = getLayoutInflater();
//            View v = inflater.inflate(R.layout.menu_category_list, null);
//            TextView button = v.findViewById(R.id.category_name);
//            button.setText(categoryNames.get(i));
//
//            if(categorySelected == i){
//                button.setTextColor(getResources().getColor(R.color.menu_light_blue));
//                button.setBackground(getResources().getDrawable(R.drawable.seletedshape));
//            }
//            else {
//                button.setTextColor(getResources().getColor(R.color.billing_grey_text_color));
//                button.setBackground(getResources().getDrawable(R.drawable.unseletedshape));
//            }
//
//            final int finalI = i;
//            button.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    categorySelected = finalI;
//                    setCategoryData();
//                }
//            });
//            categoryList.addView(v);
//        }
//    }
    private void prepareMenu() {
        categoryNames.clear();
        menuItemsList.clear();
        ArrayList<Items> itemsList = new ArrayList<>();
        for (int k = 0; k < skUlists.size(); k++) {
            Items items = new Items();
            items.setSKUId(skUlists.get(k).getSKUId());
            items.setSKUName(skUlists.get(k).getSKUName());
            items.setPackImage(skUlists.get(k).getPackImage());
            items.setPackBalance(skUlists.get(k).getPackBalance());
            items.setCartonBalance(skUlists.get(k).getCartonBalance());
            items.setPackPrice(skUlists.get(k).getPackPrice());
            items.setCartonPrice(skUlists.get(k).getCartonPrice());
            items.setTypeId(skUlists.get(k).getSKUCategoryId());
            items.setPackPerCarton(skUlists.get(k).getPacksPerCarton());
            menuItemsList.add(items);
        }

        for (int k = 0; k < nonTobaccoSKULists.size(); k++) {
            Items items = new Items();
            items.setSKUId(nonTobaccoSKULists.get(k).getSKUId());
            items.setSKUName(nonTobaccoSKULists.get(k).getSKUName());
            items.setPackImage(nonTobaccoSKULists.get(k).getPackImage());
            items.setPackBalance(nonTobaccoSKULists.get(k).getPackBalance());
            items.setCartonBalance(nonTobaccoSKULists.get(k).getCartonBalance());
            items.setPackPrice(nonTobaccoSKULists.get(k).getPackPrice());
            items.setTypeId(nonTobaccoSKULists.get(k).getCategoryId());
            items.setPackPerCarton(nonTobaccoSKULists.get(k).getPackPerCarton());
            menuItemsList.add(items);
        }

        addStockAdapter = new AddStockAdapter(AddStockActivity.this, menuItemsList);
        productListView.setAdapter(addStockAdapter);
    }

    private class InsertStockDetail extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            dialog = new ACProgressFlower.Builder(AddStockActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(AddStockActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<InsertStockDetails> call = apiService.GetInsertstock(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<InsertStockDetails>() {
                @Override
                public void onResponse(Call<InsertStockDetails> call, Response<InsertStockDetails> response) {
                    if (response.isSuccessful()) {
                        Log.d("TAG", "onResponse: " + response);
                        InsertStockDetails verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getSuccess().getMessage().equals("Values Inserted Successfully")) {

                                new InsertStockBalance().execute();

//                                data = verifyMobileResponse.getData().get(0).getWr();
//                                if(data.size() == 0){
//                                    Constants.showOneButtonAlertDialog("No history found", getResources().getString(R.string.app_name),
//                                            getResources().getString(R.string.ok), getActivity());
//                                }
//                                mAdapter = new FeedbackAdapter(getActivity(), data, "En");
//                                mListView.setAdapter(mAdapter);
//                                setData();
//
//                                try {
//                                    name.setText(verifyMobileResponse.getData().get(0).getNameEn());
//                                    Glide.with(FeedbackFragment.this)
//                                            .load(Constants.WORKSHOP_IMAGES+verifyMobileResponse.getData().get(0).getLogo())
//                                            .into(profilePic);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
                            } else {
                                //                          status false case
                                String failureResponse = verifyMobileResponse.getSuccess().getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), AddStockActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(AddStockActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(AddStockActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<InsertStockDetails> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(AddStockActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddStockActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson() {
        JSONObject parentObj = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < AddStockAdapter.productuList.size(); i++) {

                if (AddStockAdapter.productuList.get(i).getModified()) {

                    int Totalpack = (AddStockAdapter.productuList.get(i).getCartonBalance() * AddStockAdapter.productuList.get(i).getPackPerCarton())
                            + AddStockAdapter.productuList.get(i).getPackBalance();

                    JSONObject addproduct = new JSONObject();

                    String dateStr = "";
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                    Calendar calendar = Calendar.getInstance();
                    dateStr = sdf.format(calendar.getTime());

                    addproduct.put("SKUId", AddStockAdapter.productuList.get(i).getSKUId());
                    addproduct.put("Carton", AddStockAdapter.productuList.get(i).getCartonBalance());
                    addproduct.put("Pack", AddStockAdapter.productuList.get(i).getPackBalance());
                    addproduct.put("TotalPack", Totalpack);
                    addproduct.put("SalesmanId", userId);
                    addproduct.put("TypeId", AddStockAdapter.productuList.get(i).getTypeId());
                    addproduct.put("StockDate", dateStr);

                    jsonArray.put(addproduct);
                }
            }
            parentObj.put("AddStockDetails", jsonArray);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }

    private class InsertStockBalance extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareStockBalanceJson();
            dialog = new ACProgressFlower.Builder(AddStockActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(AddStockActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<InsertStockDetails> call = apiService.InsertstockBalance(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<InsertStockDetails>() {
                @Override
                public void onResponse(Call<InsertStockDetails> call, Response<InsertStockDetails> response) {
                    if (response.isSuccessful()) {
                        Log.d("TAG", "onResponse: " + response);
                        InsertStockDetails verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getSuccess().getMessage().equals("Values Inserted Successfully")) {
                                Intent intent = new Intent(AddStockActivity.this, ProductActivity.class);
                                intent.putExtra("catlist", categoryLists);
                                intent.putExtra("sku", skUlists);
                                intent.putExtra("non_tobacco", nonTobaccoSKULists);
                                startActivity(intent);
                                finish();
//                                data = verifyMobileResponse.getData().get(0).getWr();
//                                if(data.size() == 0){
//                                    Constants.showOneButtonAlertDialog("No history found", getResources().getString(R.string.app_name),
//                                            getResources().getString(R.string.ok), getActivity());
//                                }
//                                mAdapter = new FeedbackAdapter(getActivity(), data, "En");
//                                mListView.setAdapter(mAdapter);
//                                setData();
//
//                                try {
//                                    name.setText(verifyMobileResponse.getData().get(0).getNameEn());
//                                    Glide.with(FeedbackFragment.this)
//                                            .load(Constants.WORKSHOP_IMAGES+verifyMobileResponse.getData().get(0).getLogo())
//                                            .into(profilePic);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
                            } else {
                                //                          status false case
                                String failureResponse = verifyMobileResponse.getSuccess().getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), AddStockActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(AddStockActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(AddStockActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<InsertStockDetails> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(AddStockActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddStockActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareStockBalanceJson() {
        JSONObject parentObj = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < AddStockAdapter.productuList.size(); i++) {

                if (AddStockAdapter.productuList.get(i).getModified()) {

                    if(AddStockAdapter.productuList.get(i).getTypeId() == 1) {
                        for (int j = 0; j < skUlists.size(); j++) {
                            if(skUlists.get(j).getSKUName().equals(AddStockAdapter.productuList.get(i).getSKUName())){
                                AddStockAdapter.productuList.get(i).setCartonBalance(AddStockAdapter.productuList.get(i).getCartonBalance() +
                                        skUlists.get(j).getCartonBalance());

                                AddStockAdapter.productuList.get(i).setPackBalance(AddStockAdapter.productuList.get(i).getPackBalance() +
                                        skUlists.get(j).getPackBalance());

                                Gson json = new Gson();
                                RoutePlanDetailsResponse routeResponse = json.fromJson(userPrefs.getString("json", ""), RoutePlanDetailsResponse.class);
                                routeResponse.getSKUlist().get(j).setCartonBalance(AddStockAdapter.productuList.get(i).getCartonBalance() +
                                        skUlists.get(j).getCartonBalance());
                                routeResponse.getSKUlist().get(j).setPackBalance(AddStockAdapter.productuList.get(i).getPackBalance() +
                                        skUlists.get(j).getPackBalance());
                                Gson gson = new Gson();
                                String json1 = gson.toJson(routeResponse);
                                userPrefsEditor.putString("json", json1);
                                userPrefsEditor.commit();
                            }
                        }
                    }
                    else {
                        for (int j = 0; j < nonTobaccoSKULists.size(); j++) {
                            if(nonTobaccoSKULists.get(j).getSKUName().equals(AddStockAdapter.productuList.get(i).getSKUName())){
                                AddStockAdapter.productuList.get(i).setCartonBalance(AddStockAdapter.productuList.get(i).getCartonBalance() +
                                        nonTobaccoSKULists.get(j).getCartonBalance());

                                AddStockAdapter.productuList.get(i).setPackBalance(AddStockAdapter.productuList.get(i).getPackBalance() +
                                        nonTobaccoSKULists.get(j).getPackBalance());

                                Gson json = new Gson();
                                RoutePlanDetailsResponse routeResponse = json.fromJson(userPrefs.getString("json", ""), RoutePlanDetailsResponse.class);
                                routeResponse.getSKUlist().get(j).setCartonBalance(AddStockAdapter.productuList.get(i).getCartonBalance() +
                                        nonTobaccoSKULists.get(j).getCartonBalance());
                                routeResponse.getSKUlist().get(j).setPackBalance(AddStockAdapter.productuList.get(i).getPackBalance() +
                                        nonTobaccoSKULists.get(j).getPackBalance());
                                Gson gson = new Gson();
                                String json1 = gson.toJson(routeResponse);
                                userPrefsEditor.putString("json", json1);
                                userPrefsEditor.commit();
                            }
                        }
                    }

                    int Totalpack = (AddStockAdapter.productuList.get(i).getCartonBalance() * AddStockAdapter.productuList.get(i).getPackPerCarton())
                            + AddStockAdapter.productuList.get(i).getPackBalance();

                    JSONObject addproduct = new JSONObject();

                    String dateStr = "";
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                    Calendar calendar = Calendar.getInstance();
                    dateStr = sdf.format(calendar.getTime());

                    addproduct.put("SKUId", AddStockAdapter.productuList.get(i).getSKUId());
                    addproduct.put("Carton", AddStockAdapter.productuList.get(i).getCartonBalance());
                    addproduct.put("Pack", AddStockAdapter.productuList.get(i).getPackBalance());
                    addproduct.put("TotalPack", Totalpack);
                    addproduct.put("SalesmanId", userId);
                    addproduct.put("TypeId", AddStockAdapter.productuList.get(i).getTypeId());
                    addproduct.put("StockDate", dateStr);

                    jsonArray.put(addproduct);
                }
            }
            parentObj.put("AddStockBalnaceDetails", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }

    private void offlineDataSave(){
        String inputStr = prepareVerifyMobileJson();
        HashMap<String, String> values = new HashMap<>();
        values.put("Data", inputStr);
        values.put("Type", "stock");
        values.put("Status", "1");
        myDbHelper.insertOfflineData(values);

        String inputStr1 = prepareStockBalanceJson();
        HashMap<String, String> values1 = new HashMap<>();
        values1.put("Data", inputStr1);
        values1.put("Type", "balance");
        values1.put("Status", "1");
        myDbHelper.insertOfflineData(values1);

        Intent intent = new Intent(AddStockActivity.this, ProductActivity.class);
        intent.putExtra("catlist", categoryLists);
        intent.putExtra("sku", skUlists);
        intent.putExtra("non_tobacco", nonTobaccoSKULists);
        startActivity(intent);
        finish();
    }
}


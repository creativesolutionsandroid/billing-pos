package com.cs.billingpos.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.billingpos.Adapters.BluetoothDevicesAdapter;
import com.cs.billingpos.Models.InsertPromoterResponse;
import com.cs.billingpos.Models.Order;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.MyDatabase;
import com.cs.billingpos.R;
import com.cs.billingpos.Rest.APIInterface;
import com.cs.billingpos.Rest.ApiClient;
import com.cs.billingpos.Utils.Constants;
import com.cs.billingpos.Utils.GPSTracker;
import com.cs.billingpos.Utils.NetworkUtil;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.honeywell.mobility.print.LinePrinter;
import com.honeywell.mobility.print.LinePrinterException;
import com.honeywell.mobility.print.PrintProgressEvent;
import com.honeywell.mobility.print.PrintProgressListener;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckOutActivity extends Activity {

    private ArrayList<RoutePlanDetailsResponse.RoutePlanDetail> customersLists = new ArrayList<>();
    private MyDatabase myDbHelper;
    private TextView item, qty, invoicetv1, invoicetv2, grandTotaltv, outstandingtv1, netinvoicetv, grandTotaltv1, grandTotaltv2,
            outstandingtv2, netamounttv;
    private EditText discountEt, payOtherAmountEt;
    private CheckBox payTotalCb, payOtherCb;
    private Button payBtn;
    private ArrayList<Order> orderList = new ArrayList<>();
    private double subTotal, discount, payOtherAmt, netGrand;
    private double outStandingAmt;
    private int customerPos;
    private double netInvoice, outStandingBaltoInsert, amountPaid;
    private ArrayList<RoutePlanDetailsResponse.SKUlist> skUlists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList> nonTobaccoSKULists = new ArrayList<>();
    private SharedPreferences userPrefs;
    private SharedPreferences.Editor userPrefsEditor;
    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN
    };
    private static final int LOCATION_REQUEST = 3;

    private BluetoothAdapter mBluetoothAdapter;
    private ProgressDialog mProgressDlg;
    private ArrayList<BluetoothDevice> mDeviceList = new ArrayList<BluetoothDevice>();

    private int BLUETOOTH_PAIRED = 12;
    private int BLUETOOTH_NOT_PAIRED = 10;
    int deviceSelection = -1;
    private String jsonCmdAttribStr = null;

    private FusedLocationProviderClient mFusedLocationClient;
    private double latitude, longitude;
    double vat;
    int mItems = 0;
    int mQty = 0;
    String invoiceId = "";
    String TAG = "TAG";
    ImageView backBtn;
    ACProgressFlower progressdialog;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        customerPos = getIntent().getIntExtra("customerpos", 0);
        skUlists = (ArrayList<RoutePlanDetailsResponse.SKUlist>) getIntent().getSerializableExtra("sku");
        nonTobaccoSKULists = (ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList>) getIntent().getSerializableExtra("non_tobacco");
        customersLists = (ArrayList<RoutePlanDetailsResponse.RoutePlanDetail>) getIntent().getSerializableExtra("customer");
        myDbHelper = new MyDatabase(CheckOutActivity.this);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        mBluetoothAdapter	= BluetoothAdapter.getDefaultAdapter();

        backBtn = (ImageView) findViewById(R.id.back_btn);
        item = (TextView) findViewById(R.id.item);
        qty = (TextView) findViewById(R.id.qty);
        invoicetv1 = (TextView) findViewById(R.id.invoice);
        invoicetv2 = (TextView) findViewById(R.id.invoice_amount);
        grandTotaltv = (TextView) findViewById(R.id.grand_total_amount);
        outstandingtv1 = (TextView) findViewById(R.id.outstanding_amount);
        netinvoicetv = (TextView) findViewById(R.id.invoice_pay_amount);
        grandTotaltv1 = (TextView) findViewById(R.id.grand_total_pay_amount);
        grandTotaltv2 = (TextView) findViewById(R.id.grand_total_cb);
        outstandingtv2 = (TextView) findViewById(R.id.outstanding_amount_pay);
        netamounttv = (TextView) findViewById(R.id.net_amount);

        payBtn = (Button) findViewById(R.id.pay_btn);

        discountEt = (EditText) findViewById(R.id.discount_amount);
        payOtherAmountEt = (EditText) findViewById(R.id.pay_other_amount);

        payTotalCb = (CheckBox) findViewById(R.id.cb_grand_total);
        payOtherCb = (CheckBox) findViewById(R.id.cb_other_amount);

        orderList = myDbHelper.getOrderInfo();
        generateInvoiceId();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(CheckOutActivity.this);
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation() || !canAccessStorgae() || !canReadStorgae()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                gps = new GPSTracker(CheckOutActivity.this);
                try {
                    getGPSCoordinates();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            gps = new GPSTracker(CheckOutActivity.this);
            try {
                getGPSCoordinates();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        subTotal = myDbHelper.getTotalOrderPrice();
        vat = subTotal * 0.05;
        subTotal = subTotal + vat;

        for (int i = 0; i < orderList.size(); i++){
            int cartons = orderList.get(i).getTotalCarton();
            int packs = orderList.get(i).getTotalPack();
            int packsPerC = orderList.get(i).getPackPerCarton();
            mItems = mItems + (cartons+packs);
            mQty = mQty + ((cartons * packsPerC) + packs);
        }
        item.setText(""+mItems);
        qty.setText(""+mQty);
        invoicetv1.setText(Constants.priceFormat.format(subTotal));
        invoicetv2.setText(Constants.priceFormat.format(subTotal));

        try {
            outStandingAmt = customersLists.get(customerPos).getOutStandingBalance();
        } catch (Exception e) {
            e.printStackTrace();
            outStandingAmt = 0;
        }

        outstandingtv1.setText(Constants.priceFormat.format(outStandingAmt));
        changeData();

        payBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                insertOrder();
//                prepareJson();
//                createPDF();
//                showMenuDialog();
                progressdialog = new ACProgressFlower.Builder(CheckOutActivity.this)
                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                        .themeColor(Color.WHITE)
                        .fadeColor(Color.DKGRAY).build();
                progressdialog.show();
                final String networkStatus = NetworkUtil.getConnectivityStatusString(CheckOutActivity.this);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                     new insertInvoice().execute();
                }
                else {
                    offlineDataSave();
                }
            }
        });

        discountEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                try {
                    discount = Float.parseFloat(str);
                    if(discount > subTotal) {
                        discount = subTotal;
                        discountEt.setText(""+discount);
                    }
                    changeData();
                } catch (Exception e) {
                    discount = 0;
                    e.printStackTrace();
                }
            }
        });

        payOtherAmountEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                try {
                    double netInvoice1 = subTotal - discount;
                    double netGrand1 = netInvoice1 + outStandingAmt;
                    payOtherAmt = Float.parseFloat(str);
                    if(payOtherAmt > netGrand1) {
                        payOtherAmt = netGrand1;
                        payOtherAmountEt.setText(""+payOtherAmt);
                    }
                    changeData();
                } catch (Exception e) {
                    e.printStackTrace();
                    payOtherAmt = 0;
                }
            }
        });

        payTotalCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    payOtherCb.setChecked(false);
                    payOtherAmountEt.setEnabled(false);
                    payOtherAmountEt.setAlpha(0.3f);
                    changeData();
                }
            }
        });

        payOtherCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    payTotalCb.setChecked(false);
                    payOtherAmountEt.setEnabled(true);
                    payOtherAmountEt.setAlpha(1.0f);
                    changeData();
                    if(payOtherAmountEt.getText().toString().equals("0.00")){
                        payOtherAmountEt.setText("");
                        payOtherAmountEt.requestFocus();
                    }
                }
            }
        });

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter);
        registerReceiver(mPairReceiver, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));
    }

    private void changeData(){
        netInvoice = subTotal - discount;
        grandTotaltv.setText(Constants.priceFormat.format(netInvoice));
        netinvoicetv.setText(Constants.priceFormat.format(netInvoice));
        netGrand = netInvoice + outStandingAmt;
        grandTotaltv1.setText(Constants.priceFormat.format(netGrand));
        grandTotaltv2.setText(Constants.priceFormat.format(netGrand));
        if(payTotalCb.isChecked()){
            outstandingtv2.setText(Constants.priceFormat.format(netGrand));
            netamounttv.setText(Constants.priceFormat.format(netGrand));
            outStandingBaltoInsert = 0;
            amountPaid = netGrand;
        }
        if(payOtherCb.isChecked()){
            netGrand = netGrand - payOtherAmt;
            amountPaid = payOtherAmt;
            outstandingtv2.setText(Constants.priceFormat.format(netGrand));
            netamounttv.setText(Constants.priceFormat.format(payOtherAmt));
            outStandingBaltoInsert = netGrand;
        }
    }

    private void insertOrder(){
        for (int i = 0; i < orderList.size(); i++) {
            for (int j = 0; j < skUlists.size(); j++) {
                if (skUlists.get(j).getSKUName().equals(orderList.get(i).getItemName())) {
                    int cartonsBalance = skUlists.get(j).getCartonBalance();
                    int packsPerCarton = skUlists.get(j).getPacksPerCarton();
                    int cartonsUsed = orderList.get(i).getTotalCarton();
                    int packsUsed = orderList.get(i).getTotalPack();
                    if (cartonsUsed > cartonsBalance) {
                        cartonsUsed = cartonsBalance;
                        int p = cartonsUsed * packsPerCarton;
                        packsUsed = packsUsed - p;
                    } else {
                        cartonsUsed = cartonsBalance - cartonsUsed;
                        packsUsed = (int) (packsUsed % packsPerCarton);
                    }
                    Gson json = new Gson();
                    RoutePlanDetailsResponse routeResponse = json.fromJson(userPrefs.getString("json", ""), RoutePlanDetailsResponse.class);
                    routeResponse.getSKUlist().get(j).setCartonBalance(cartonsUsed);
                    routeResponse.getSKUlist().get(j).setPackBalance(packsUsed);
                    Gson gson = new Gson();
                    String json1 = gson.toJson(routeResponse);
                    userPrefsEditor.putString("json", json1);
                    userPrefsEditor.commit();
                }
            }

            for (int j = 0; j < nonTobaccoSKULists.size(); j++) {
                if (nonTobaccoSKULists.get(j).getSKUName().equals(orderList.get(i).getItemName())) {
                    int cartonsBalance = nonTobaccoSKULists.get(j).getCartonBalance();
                    int packsPerCarton = nonTobaccoSKULists.get(j).getPackPerCarton();
                    int cartonsUsed = orderList.get(i).getTotalCarton();
                    int packsUsed = 0;
                    if (cartonsUsed > cartonsBalance) {
                        cartonsUsed = cartonsBalance;
                        int p = cartonsUsed * packsPerCarton;
                        packsUsed = packsUsed - p;
                    } else {
                        cartonsUsed = cartonsBalance - cartonsUsed;
                        packsUsed = (int) (packsUsed % packsPerCarton);
                    }
                    Gson json = new Gson();
                    RoutePlanDetailsResponse routeResponse = json.fromJson(userPrefs.getString("json", ""), RoutePlanDetailsResponse.class);
                    routeResponse.getNonTobaccoSKUList().get(j).setCartonBalance(cartonsUsed);
                    routeResponse.getNonTobaccoSKUList().get(j).setPackBalance(packsUsed);
                    double outStandingBal = routeResponse.getTotaloutstandingBalance();
                    double dailySalesBal = routeResponse.getTodaysales();
                    Gson gson = new Gson();
                    String json1 = gson.toJson(routeResponse);
                    userPrefsEditor.putString("json", json1);
                    userPrefsEditor.commit();
                }
            }
        }
        Gson json = new Gson();
        RoutePlanDetailsResponse routeResponse = json.fromJson(userPrefs.getString("json", ""), RoutePlanDetailsResponse.class);
        routeResponse.getCustomersList().get(customerPos).setOutStandingBalance(outStandingBaltoInsert);
        double prevTotalOutStandingBal = routeResponse.getTotaloutstandingBalance();
        double prevDailySalesBal = routeResponse.getTodaysales();
        prevTotalOutStandingBal = prevTotalOutStandingBal - routeResponse.getCustomersList().get(customerPos).getOutStandingBalance();
        prevTotalOutStandingBal = prevTotalOutStandingBal + outStandingBaltoInsert;
        routeResponse.setTotaloutstandingBalance(prevTotalOutStandingBal);
        routeResponse.setTodaysales(prevDailySalesBal + amountPaid);
        Gson gson = new Gson();
        String json1 = gson.toJson(routeResponse);
        userPrefsEditor.putString("json", json1);
        userPrefsEditor.commit();

        showMenuDialog();
    }

    private void showMenuDialog(){
        if(progressdialog != null) {
            progressdialog.dismiss();
        }
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CheckOutActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.print_alert_dialog;

        final View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        RelativeLayout dialogbox1 =(RelativeLayout) dialogView.findViewById(R.id.pdf);
        RelativeLayout dialogbox2 =(RelativeLayout) dialogView.findViewById(R.id.print);

        ImageView cancel = (ImageView) dialogView.findViewById(R.id.close_popup);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myDbHelper.deleteOrderTable();
                Intent intent = new Intent(CheckOutActivity.this, DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
//                finalCustomDialog.dismiss();
            }
        });

        dialogbox1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!canAccessStorgae() && !canReadStorgae()) {
                    requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                }
                else {
                    createPDF();
                }
//                Intent intent = new Intent(CheckOutActivity.this, DashboardActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
//                finish();
            }
        });

        dialogbox2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mBluetoothAdapter == null) {
                    Toast.makeText(CheckOutActivity.this, "Bluetooth is unsupported by this device", Toast.LENGTH_SHORT).show();
                }
                else {
                    readAssetFiles();
                    if(!mBluetoothAdapter.isEnabled()) {
                        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(intent, 1000);
                    }
                    else {
                        Log.d(TAG, "searching devices: ");
                        mProgressDlg = new ProgressDialog(CheckOutActivity.this);

                        mProgressDlg.setMessage("Scanning devices...");
                        mProgressDlg.setCancelable(false);
                        mProgressDlg.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                mBluetoothAdapter.cancelDiscovery();
                            }
                        });
                        mProgressDlg.show();
                        mDeviceList.clear();
                        mBluetoothAdapter.startDiscovery();
                    }
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.95;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private String getEndDate(){
        String dateStr = "";
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.US);

        Calendar calendar = Calendar.getInstance();
        dateStr = sdf.format(calendar.getTime());

        return dateStr;
    }

    private String getInvoiceDate(){
        String dateStr = "";
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmmssSSS", Locale.US);

        Calendar calendar = Calendar.getInstance();
        dateStr = sdf.format(calendar.getTime());

        return dateStr;
    }

    public void getGPSCoordinates() throws IOException {

        if(gps != null){
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                try {

                    mFusedLocationClient.getLastLocation().addOnFailureListener(CheckOutActivity.this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.i("TAG","fused lat null");
                        }
                    })
                            .addOnSuccessListener(CheckOutActivity.this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {
                                        // Logic to handle location object
                                        latitude = location.getLatitude();
                                        longitude = location.getLongitude();
                                        Log.i("TAG","fused lat "+location.getLatitude());
                                        Log.i("TAG","fused long "+location.getLongitude());
                                    }
                                    else{
                                        Log.i("TAG","fused lat null");
                                    }
                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(CheckOutActivity.this);
                    try {
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(CheckOutActivity.this, "Location permission denied, Unable to save location", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessStorgae() {
        return (hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canReadStorgae() {
        return (hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(CheckOutActivity.this, perm));
    }

    private String prepareJson(){
        JSONObject parentObj = new JSONObject();
        JSONObject salesMasterObj = new JSONObject();
        generateInvoiceId();
        try {
            salesMasterObj.put("id", orderList.get(0).getInvoiceId());
            salesMasterObj.put("InvoiceNo", invoiceId);
            salesMasterObj.put("salesmanid", userPrefs.getString("EmpId", ""));
            salesMasterObj.put("TrackingNo", customersLists.get(customerPos).getTrackingNo());
            salesMasterObj.put("Subtotal", myDbHelper.getTotalOrderPrice());
            salesMasterObj.put("VatPercent", "5");
            salesMasterObj.put("VatAmount", vat);
            salesMasterObj.put("InvoiceAmount", subTotal);
            salesMasterObj.put("NoofItems", mItems);
            salesMasterObj.put("Quantity", mQty);
            salesMasterObj.put("DiscountAmount", discount);
            salesMasterObj.put("GrandTotalAmount", (subTotal - discount));
            salesMasterObj.put("DateofVisit", userPrefs.getString("dateofvisit", ""));
            salesMasterObj.put("Latitude", latitude);
            salesMasterObj.put("Longitude", longitude);
            salesMasterObj.put("StartTime", userPrefs.getString("startdate", ""));
            salesMasterObj.put("EndTime", getEndDate());
            salesMasterObj.put("customerName", customersLists.get(customerPos).getOutletName());

            JSONArray salesDetailsArray = new JSONArray();
            for (int i = 0; i < orderList.size(); i++){
                JSONObject itemsObj = new JSONObject();
                itemsObj.put("InvoiceNo", invoiceId);
                itemsObj.put("TypeId", orderList.get(i).getTypeId());
                itemsObj.put("SKUId", orderList.get(i).getSKUId());
                itemsObj.put("Carton", orderList.get(i).getCarton());
                itemsObj.put("Pack", orderList.get(i).getPack());
                itemsObj.put("TotalPack", (orderList.get(i).getCarton() * orderList.get(i).getPackPerCarton())
                        +orderList.get(i).getTotalPack());
                itemsObj.put("FreeCarton", orderList.get(i).getFreeCarton());
                itemsObj.put("FreePack", orderList.get(i).getFreePack());
                itemsObj.put("FreeTotalPack", (orderList.get(i).getFreeCarton() * orderList.get(i).getPackPerCarton())
                        + orderList.get(i).getFreePack());
                itemsObj.put("CartonPrice", orderList.get(i).getCartonPrice());
                itemsObj.put("PackPrice", orderList.get(i).getPackPrice());
                itemsObj.put("PackPrice", orderList.get(i).getPackPrice());
                itemsObj.put("TotalCartonPrice", orderList.get(i).getCartonPrice());
                itemsObj.put("TotalPackPrice", orderList.get(i).getPackPrice());
                itemsObj.put("TotalPrice", orderList.get(i).getTotalPrice());
                itemsObj.put("DateofVisit", userPrefs.getString("dateofvisit", ""));
                itemsObj.put("isActive", true);
                salesDetailsArray.put(itemsObj);
            }

            JSONObject outStandingObj = new JSONObject();
            outStandingObj.put("outstandingBalanceid", orderList.get(0).getOutstandingId());
            outStandingObj.put("TrackingNo", customersLists.get(customerPos).getTrackingNo());
            outStandingObj.put("InvoiceNo", invoiceId);
            outStandingObj.put("PreviousOutstanding", customersLists.get(customerPos).getOutStandingBalance());
            outStandingObj.put("AmountPaid", amountPaid);
            outStandingObj.put("OutStandingBalance", outStandingBaltoInsert);
            outStandingObj.put("DateofVisit", userPrefs.getString("dateofvisit", ""));
            outStandingObj.put("IsInvoice", true);

            parentObj.put("SalesMaster", salesMasterObj);
            parentObj.put("SalesDetail", salesDetailsArray);
            parentObj.put("OutStandingBalance", outStandingObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("TAG", "prepareJson: "+parentObj.toString());
        return parentObj.toString();
    }

    private void generateInvoiceId() {
        if (orderList.get(0).getInvoiceId().equals("0")) {
            invoiceId = "I" + userPrefs.getString("EmpId", "") + "V" + customersLists.get(customerPos).getZoneID() + "S" + getInvoiceDate();
        }
        else {
            invoiceId = orderList.get(0).getInvoiceNumber();
        }
    }

    private class insertInvoice extends AsyncTask<String, String, String> {

        String inputStr;
//        ACProgressFlower dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(CheckOutActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<InsertPromoterResponse> call = apiService.insertInvoice(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            Log.d("TAG", "onResponse1: "+inputStr);
            call.enqueue(new Callback<InsertPromoterResponse>() {
                @Override
                public void onResponse(Call<InsertPromoterResponse> call, Response<InsertPromoterResponse> response) {
                    Log.d("TAG", "onResponse: "+response);
                    if (response.isSuccessful()) {
                        InsertPromoterResponse PlacebidResponce = response.body();
                        try {
                            if (PlacebidResponce.getSuccess().getMessage().equalsIgnoreCase("Values Inserted Succcesfully")) {
                                insertOrder();
                            } else {
                                //                          status false case
                                String failureResponse = PlacebidResponce.getFailure();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), CheckOutActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(CheckOutActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if(progressdialog != null ){
                            progressdialog.dismiss();
                        }
                        Toast.makeText(CheckOutActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<InsertPromoterResponse> call, Throwable t) {
                    if(progressdialog != null ){
                        progressdialog.dismiss();
                    }
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(CheckOutActivity.this, R.string.connection_error, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(CheckOutActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });

            return null;
        }
    }

    private void createPDF(){
        ACProgressFlower dialog = new ACProgressFlower.Builder(CheckOutActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        dialog.show();
        Document doc = new Document();
        doc.newPage();

        try {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BillingPos";

            File dir = new File(path);
            if(!dir.exists())
                dir.mkdirs();

            File file = new File(dir, invoiceId+".pdf");
            FileOutputStream fOut = new FileOutputStream(file);

            PdfWriter.getInstance(doc, fOut);

            Font paraFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
                    Font.NORMAL);

            Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 22,
                    Font.BOLD);

            //open the document
            doc.open();
            doc.add(new Chunk(""));

            String text1 ="Billing POS";
            Paragraph p1 = new Paragraph(text1);
            p1.setAlignment(Paragraph.ALIGN_CENTER);
            p1.setFont(boldFont);
            doc.add(p1);

            String text2 = invoiceId ;
            Paragraph p2 = new Paragraph(text2);
            p2.setAlignment(Paragraph.ALIGN_CENTER);
            p2.setFont(boldFont);
            doc.add(p2);

            String text3 = "Cashier    : "+customersLists.get(customerPos).getEmpName();
            Paragraph p3 = new Paragraph(text3);
            p3.setAlignment(Paragraph.ALIGN_LEFT);
            p3.setFont(paraFont);
            doc.add(p3);

            String text4 = getTodayDate()+" "+getTodayTime();
            Paragraph p4 = new Paragraph(text4);
            p4.setAlignment(Paragraph.ALIGN_LEFT);
            p4.setFont(paraFont);
            doc.add(p4);

            String text5 = "Customer    : "+ customersLists.get(customerPos).getOutletName();
            Paragraph p5 = new Paragraph(text5);
            p5.setAlignment(Paragraph.ALIGN_LEFT);
            p5.setFont(paraFont);
            doc.add(p5);

            String text6 = "Phone         : "+ customersLists.get(customerPos).getMobile();
            Paragraph p6 = new Paragraph(text6);
            p6.setAlignment(Paragraph.ALIGN_LEFT);
            p6.setFont(paraFont);
            doc.add(p6);

            String text7 = "Addresss     : "+customersLists.get(customerPos).getStreatName();
            Paragraph p7 = new Paragraph(text7);
            p7.setAlignment(Paragraph.ALIGN_LEFT);
            p7.setFont(paraFont);
            doc.add(p7);

            String text17 = "WholeSaler  : "+customersLists.get(customerPos).getWholesalerName();
            Paragraph p17 = new Paragraph(text17);
            p17.setAlignment(Paragraph.ALIGN_LEFT);
            p17.setFont(paraFont);
            doc.add(p17);

            String text18 = "VAT        : "+customersLists.get(customerPos).getVATNumber();
            Paragraph p18 = new Paragraph(text18);
            p18.setAlignment(Paragraph.ALIGN_LEFT);
            p18.setFont(paraFont);
            doc.add(p18);

            doc.add(new Chunk(""));

            String text8 ="-------------------------------------------------------------------------------------------------------------------------------";
            Paragraph p8 = new Paragraph(text8);
            p8.setAlignment(Paragraph.ALIGN_RIGHT);
            p8.setFont(paraFont);
//            doc.add(p8);

            String text9 ="Item                                                  "+"UOM                    "+"Qty                 "+"Rate                   "+"Total";
            Paragraph p9 = new Paragraph(text9);
            p9.setAlignment(Paragraph.ALIGN_RIGHT);
            p9.setFont(paraFont);
//            doc.add(p9);

            String text10 ="--------------------------------------------------------------------------------------------------------------------------------";
            Paragraph p10 = new Paragraph(text10);
            p10.setAlignment(Paragraph.ALIGN_RIGHT);
            p10.setFont(paraFont);
//            doc.add(p10);

            PdfPTable table = new PdfPTable(5);
            table.setTotalWidth(1000);
            table.setComplete(true);
            table.setWidthPercentage(100);
            table.setWidths(new int[] {350, 75, 75, 200, 200});
            PdfPCell c1 = new PdfPCell(new Phrase("Item"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setMinimumHeight(20);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("UOM"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setMinimumHeight(20);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Qty"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setMinimumHeight(20);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Rate"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setMinimumHeight(20);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Total"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setMinimumHeight(20);
            table.addCell(c1);
            table.setHeaderRows(1);

//            PdfPTable table = new PdfPTable(new float[] { 3, 1, 1, 1, 1});
//            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_MIDDLE);
//            table.addCell("Item");
//            table.addCell("UOM");
//            table.addCell("Qty");
//            table.addCell("Rate");
//            table.addCell("Total");
//            table.setHeaderRows(1);
            PdfPCell[] cells = table.getRow(0).getCells();
//            for (int j=0;j<cells.length;j++){
//                cells[j].setMinimumHeight(20);
//            }
            for (int i = 0; i < orderList.size(); i++){
                String text11 = orderList.get(i).getItemName();
                StringBuilder text123 = new StringBuilder(text11.length());
                text123.append(orderList.get(i).getItemName());
                int lenght = text11.length();
//                if(text11.length() < 39){
//                    for (int j = 0; j < (39 - lenght); j++){
//                        text123.append(" ");
//                    }
//                }
//                else {
//                    text123.substring(0,38);
//                }
                Log.d(TAG, "createPDF: "+text123.length());

                if(orderList.get(i).getCarton() > 0) {

                    String text001 = Constants.priceFormat.format(orderList.get(i).getCartonPrice());
                    int lenght1 = text001.length();
//                    if(text001.length() < 17){
//                        for (int j = 0; j < (18 - lenght1); j++){
//                            text001 =  " "+text001;
//                        }
//                    }

                    String text002 = Constants.priceFormat.format(orderList.get(i).getCarton() * orderList.get(i).getCartonPrice());
                    int lenght2 = text002.length();
//                    if(text002.length() < 17){
//                        for (int j = 0; j < (18 - lenght2); j++){
//                            text002 =  " "+text002;
//                        }
//                    }
                    c1 = new PdfPCell(new Phrase(text123.toString()));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setMinimumHeight(20);
                    c1.setPaddingLeft(5);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("Ct"));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(""+orderList.get(i).getCarton()));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text001));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setMinimumHeight(20);
                    c1.setPaddingRight(5);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text002));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setMinimumHeight(20);
                    c1.setPaddingRight(5);
                    table.addCell(c1);

//                    text11 = text123.toString() + " " + "Ct                      " + orderList.get(i).getCarton() + "          " + text001 +" "+text002;
//                    Paragraph p11 = new Paragraph(text11);
//                    p11.setAlignment(Paragraph.ALIGN_RIGHT);
//                    p11.setFont(paraFont);
//                    doc.add(p11);
                }

                if(orderList.get(i).getPack() > 0) {
                    String text001 = Constants.priceFormat.format(orderList.get(i).getPackPrice());
                    int lenght1 = text001.length();
//                    if(text001.length() < 17){
//                        for (int j = 0; j < (18 - lenght1); j++){
//                            text001 =  " "+text001;
//                        }
//                    }

                    String text002 = Constants.priceFormat.format(orderList.get(i).getPack() * orderList.get(i).getPackPrice());
                    int lenght2 = text002.length();
//                    if(text002.length() < 17){
//                        for (int j = 0; j < (18 - lenght2); j++){
//                            text002 =  " "+text002;
//                        }
//                    }

                    c1 = new PdfPCell(new Phrase(text123.toString()));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setMinimumHeight(20);
                    c1.setPaddingLeft(5);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("Pk"));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(""+orderList.get(i).getPack()));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text001));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setPaddingRight(5);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text002));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setPaddingRight(5);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);
//                    text11 = text123.toString() + " " + "Pk                      " + orderList.get(i).getPack() + "          " + text001 + " "+text002;
//                    Paragraph p11 = new Paragraph(text11);
//                    p11.setAlignment(Paragraph.ALIGN_RIGHT);
//                    p11.setFont(paraFont);
//                    doc.add(p11);
                }

                if(orderList.get(i).getFreeCarton() > 0) {

                    String text001 = "0.00";
                    int lenght2 = text001.length();
//                    for (int j = 0; j < (18 - lenght2); j++){
//                        text001 =  " "+text001;
//                    }

                    String text002 = "0.00";
                    int lenght3 = text002.length();
//                    for (int j = 0; j < (18 - lenght3); j++){
//                        text002 =  " "+text002;
//                    }

                    c1 = new PdfPCell(new Phrase(text123.toString()));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setMinimumHeight(20);
                    c1.setPaddingLeft(5);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("Ct"));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(""+orderList.get(i).getFreeCarton()));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text001));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setPaddingRight(5);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text002));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setPaddingRight(5);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);
//                    text11 = text123.toString() + " " + "Ct                      " + orderList.get(i).getFreeCarton() + "          " + text001+ " "+text002;
//                    Paragraph p11 = new Paragraph(text11);
//                    p11.setAlignment(Paragraph.ALIGN_RIGHT);
//                    p11.setFont(paraFont);
//                    doc.add(p11);
                }

                if(orderList.get(i).getFreePack() > 0) {
                    String text001 = "0.00";
                    int lenght3 = text001.length();
//                    for (int j = 0; j < (18 - lenght3); j++){
//                        text001 =  " "+text001;
//                    }

                    String text002 = "0.00";
                    int lenght2 = text002.length();
//                    for (int j = 0; j < (18 - lenght2); j++){
//                        text002 =  " "+text002;
//                    }

                    c1 = new PdfPCell(new Phrase(text123.toString()));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setMinimumHeight(20);
                    c1.setPaddingLeft(5);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("Pk"));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(""+orderList.get(i).getFreePack()));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text001));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setPaddingRight(5);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text002));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setPaddingRight(5);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);
//                    text11 = text123.toString() + " " + "Pk                      " + orderList.get(i).getFreePack() + "          " + text001+ " "+text002;
//                    Paragraph p11 = new Paragraph(text11);
//                    p11.setAlignment(Paragraph.ALIGN_RIGHT);
//                    p11.setFont(paraFont);
//                    doc.add(p11);
                }

            }
            doc.add(table);

            String text12 ="---------------------------------------------------------------------------------------------------------------------------------";
            Paragraph p12 = new Paragraph(text12);
            p12.setAlignment(Paragraph.ALIGN_RIGHT);
            p12.setFont(paraFont);
//            doc.add(p12);

            String text001 = Constants.priceFormat.format(myDbHelper.getTotalOrderPrice());
            int lenght2 = text001.length();
            if(text001.length() < 25){
                for (int j = 0; j < (25 - lenght2); j++){
                    text001 =  " "+text001;
                }
            }

            String text13 = "Sub Total "+text001;
            Paragraph p13 = new Paragraph(text13);
            p13.setAlignment(Paragraph.ALIGN_RIGHT);
            p13.setFont(paraFont);
            doc.add(p13);

            String text002 = Constants.priceFormat.format(vat);
            int lenght1 = text002.length();
            if(text002.length() < 25){
                for (int j = 0; j < (25 - lenght1); j++){
                    text002 =  " "+text002;
                }
            }

            String text14 ="VAT(5%) "+text002;
            Paragraph p14 = new Paragraph(text14);
            p14.setAlignment(Paragraph.ALIGN_RIGHT);
            p14.setFont(paraFont);
            doc.add(p14);

            String text003 = Constants.priceFormat.format(subTotal);
            int lenght = text003.length();
            if(text003.length() < 25){
                for (int j = 0; j < (25 - lenght); j++){
                    text003 =  " "+text003;
                }
            }


            String text15 ="Total "+text003;
            Paragraph p15 = new Paragraph(text15);
            p15.setAlignment(Paragraph.ALIGN_RIGHT);
            p15.setFont(paraFont);
            doc.add(p15);

            String text19 ="----------------------------------------------------------------------------------------------------------------------------------";
            Paragraph p19 = new Paragraph(text19);
            p19.setAlignment(Paragraph.ALIGN_RIGHT);
            p19.setFont(paraFont);
            doc.add(p19);

            String text004 = Constants.priceFormat.format(outStandingAmt);
            int lenght4 = text004.length();
            if(text004.length() < 25){
                for (int j = 0; j < (25 - lenght4); j++){
                    text004 =  " "+text004;
                }
            }


            String text16 ="Previous Balance "+text004;
            Paragraph p16 = new Paragraph(text16);
            p16.setAlignment(Paragraph.ALIGN_RIGHT);
            p16.setFont(paraFont);
            doc.add(p16);

            String text005 = Constants.priceFormat.format(subTotal);
            int lenght5 = text005.length();
            if(text005.length() < 25){
                for (int j = 0; j < (25 - lenght5); j++){
                    text005 =  " "+text005;
                }
            }

            String text25 ="Invoice Amount "+text005;
            Paragraph p25 = new Paragraph(text25);
            p25.setAlignment(Paragraph.ALIGN_RIGHT);
            p25.setFont(paraFont);
            doc.add(p25);

            String text006 = Constants.priceFormat.format(amountPaid);
            int lenght6 = text006.length();
            if(text006.length() < 25){
                for (int j = 0; j < (25 - lenght6); j++){
                    text006 =  " "+text006;
                }
            }


            String text26 ="Amount Paid "+text006;
            Paragraph p26 = new Paragraph(text26);
            p26.setAlignment(Paragraph.ALIGN_RIGHT);
            p26.setFont(paraFont);
            doc.add(p26);

            String text007 = Constants.priceFormat.format(outStandingBaltoInsert);
            int lenght7 = text007.length();
            if(text007.length() < 25){
                for (int j = 0; j < (25 - lenght7); j++){
                    text007 =  " "+text007;
                }
            }

            String text20 ="Outstanding Balance "+text007;
            Paragraph p20 = new Paragraph(text20);
            p20.setAlignment(Paragraph.ALIGN_RIGHT);
            p20.setFont(paraFont);
            doc.add(p20);


            String text21 ="----------------------------------------------------------------------------------------------------------------------------------";
            Paragraph p21 = new Paragraph(text21);
            p21.setAlignment(Paragraph.ALIGN_RIGHT);
            p21.setFont(paraFont);
            doc.add(p21);


            String text22 ="                                                 "+"SIGN...........................................";
            Paragraph p22 = new Paragraph(text22);
            p22.setAlignment(Paragraph.ALIGN_LEFT);
            p22.setFont(paraFont);
            doc.add(p22);

            String text23 ="                                                 "+"SALES MAN .....................................";
            Paragraph p23 = new Paragraph(text23);
            p23.setAlignment(Paragraph.ALIGN_LEFT);
            p23.setFont(paraFont);
            doc.add(p23);

            String text24 ="                                                 "+"RECEIVER.......................................";
            Paragraph p24 = new Paragraph(text24);
            p24.setAlignment(Paragraph.ALIGN_LEFT);
            p24.setFont(paraFont);
            doc.add(p24);

            String text35 ="                                                           "+"''WE ARE PROUD TO SERVE YOU''";
            Paragraph p35 = new Paragraph(text35);
            p35.setAlignment(Paragraph.ALIGN_LEFT);
            p35.setFont(boldFont);
            doc.add(p35);
            //add paragraph to document
            Log.d("PDFCreator", "createPDF: ");

        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        }
        finally {
            doc.close();

        }

        if(dialog!=null) {
            dialog.dismiss();
        }

                viewPdf(invoiceId+".pdf");
    }
    // Method for opening a pdf file
    private void viewPdf(String file) {

        Intent intentShareFile = new Intent(Intent.ACTION_SEND);
        File fileWithinMyDir = new File(Environment.getExternalStorageDirectory() + "/BillingPos/" + file);

        if(fileWithinMyDir.exists()) {
            intentShareFile.setType("application/pdf");
            if(Build.VERSION.SDK_INT>=24){
                try{
                    Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                    m.invoke(null);
                    intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + Environment.getExternalStorageDirectory() + "/BillingPos/" + file));
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
            else {
                intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + Environment.getExternalStorageDirectory() + "/BillingPos/" + file));
            }
            intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                    "Sharing invoice...");
            intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing invoice...");

            startActivity(Intent.createChooser(intentShareFile, "Share invoice"));
        }

//        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/BillingPos/" + file);
//        Uri path = Uri.fromFile(pdfFile);
//
//        // Setting the intent for pdf reader
//        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
//        pdfIntent.setDataAndType(path, "application/pdf");
//        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//        try {
//            startActivity(pdfIntent);
//        } catch (ActivityNotFoundException e) {
//            Toast.makeText(CheckOutActivity.this, "Can't read pdf file", Toast.LENGTH_SHORT).show();
//        }
    }

    private String getTodayDate(){
        String dateStr = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        Calendar calendar = Calendar.getInstance();
        dateStr = sdf.format(calendar.getTime());

        return dateStr;
    }

    private String getTodayTime(){
        String dateStr = "";
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.US);

        Calendar calendar = Calendar.getInstance();
        dateStr = sdf.format(calendar.getTime());

        return dateStr;
    }

    @Override
    public void onPause() {
        if (mBluetoothAdapter != null) {
            if (mBluetoothAdapter.isDiscovering()) {
                mBluetoothAdapter.cancelDiscovery();
            }
        }

        super.onPause();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        unregisterReceiver(mPairReceiver);

        super.onDestroy();
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                mProgressDlg.dismiss();

                if(mDeviceList.size() > 0) {
                    showBluetoothDevicesDialog();
                }
                else {
                    Toast.makeText(CheckOutActivity.this, "No devices found", Toast.LENGTH_SHORT).show();
                }
            }else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                mDeviceList.add(device);
            }
        }
    };

    private void showBluetoothDevicesDialog(){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CheckOutActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.bluetooth_devices_alert_dialog;

        final View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        ListView devicesListView =(ListView) dialogView.findViewById(R.id.devices_list);
        ImageView cancel = (ImageView) dialogView.findViewById(R.id.close_popup);

        BluetoothDevicesAdapter mAdapter = new BluetoothDevicesAdapter(CheckOutActivity.this, mDeviceList);
        devicesListView.setAdapter(mAdapter);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
            }
        });

        devicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(mDeviceList.get(position).getBondState() == BLUETOOTH_NOT_PAIRED) {
                    pairDevice(mDeviceList.get(position));
                    deviceSelection = position;
                }
                else if(mDeviceList.get(position).getBondState() == BLUETOOTH_PAIRED) {
                    deviceSelection = position;
                    PrintTask task = new PrintTask();
                    // Executes PrintTask with the specified parameter which is passed
                    // to the PrintTask.doInBackground method.
                    task.execute("PR3", mDeviceList.get(deviceSelection).getAddress());
                }
                finalCustomDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.95;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void pairDevice(BluetoothDevice device) {
        try {
            Method method = device.getClass().getMethod("createBond", (Class[]) null);
            method.invoke(device, (Object[]) null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final BroadcastReceiver mPairReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final int state 		= intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                final int prevState	= intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, BluetoothDevice.ERROR);

                if (state == BluetoothDevice.BOND_BONDED && prevState == BluetoothDevice.BOND_BONDING) {
                    PrintTask task = new PrintTask();
                    // Executes PrintTask with the specified parameter which is passed
                    // to the PrintTask.doInBackground method.
                    task.execute("PR3", mDeviceList.get(deviceSelection).getAddress());
                }
            }
        }
    };

    private void readAssetFiles()
    {
        InputStream input = null;
        ByteArrayOutputStream output = null;
        AssetManager assetManager = getAssets();
        String[] files = { "printer_profiles.JSON"};
        int fileIndex = 0;
        int initialBufferSize;

        try
        {
            for (String filename : files)
            {
                input = assetManager.open(filename);
                initialBufferSize = (fileIndex == 0) ? 8000 : 2500;
                output = new ByteArrayOutputStream(initialBufferSize);

                byte[] buf = new byte[1024];
                int len;
                while ((len = input.read(buf)) > 0)
                {
                    output.write(buf, 0, len);
                }
                input.close();
                input = null;

                output.flush();
                output.close();
                switch (fileIndex)
                {
                    case 0:
                        jsonCmdAttribStr = output.toString();
                        break;
                }

                fileIndex++;
                output = null;
            }
        }
        catch (Exception ex)
        {
            Constants.showOneButtonAlertDialog("Error reading asset file: " + files[fileIndex], "Billing Pos", "Ok", CheckOutActivity.this);
        }
        finally
        {
            try
            {
                if (input != null)
                {
                    input.close();
                    input = null;
                }

                if (output != null)
                {
                    output.close();
                    output = null;
                }
            }
            catch (IOException e){}
        }
    }

    /**
     * This exception is thrown by the background thread to halt printing attempts and
     * return an error to the UI when the printer status indicates conditions that would
     * prevent successful printing such as "lid open" or "paper out".
     */
    public class BadPrinterStateException extends Exception
    {
        static final long serialVersionUID = 1;

        public BadPrinterStateException(String message)
        {
            super(message);
        }
    }

    /**
     * This class demonstrates printing in a background thread and updates
     * the UI in the UI thread.
     */
    public class PrintTask extends AsyncTask<String, Integer, String> {
        private static final String PROGRESS_CANCEL_MSG = "Printing cancelled\n";
        private static final String PROGRESS_COMPLETE_MSG = "Printing completed\n";
        private static final String PROGRESS_ENDDOC_MSG = "End of document\n";
        private static final String PROGRESS_FINISHED_MSG = "Printer connection closed\n";
        private static final String PROGRESS_NONE_MSG = "Unknown progress message\n";
        private static final String PROGRESS_STARTDOC_MSG = "Start printing document\n";
        ACProgressFlower dialog;

        /**
         * Runs on the UI thread before doInBackground(Params...).
         */
        @Override
        protected void onPreExecute()
        {
            dialog = new ACProgressFlower.Builder(CheckOutActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        /**
         * This method runs on a background thread. The specified parameters
         * are the parameters passed to the execute method by the caller of
         * this task. This method can call publishProgress to publish updates
         * on the UI thread.
         */
        @Override
        protected String doInBackground(String... args)
        {
            LinePrinter lp = null;
            String sResult = null;
            String sPrinterID = args[0];
            String sPrinterAddr = args[1];
            String sDocNumber = "1234567890";
            String sPrinterURI = null;

            // The printer address should be a Bluetooth MAC address.
            if (sPrinterAddr.contains(":") == false && sPrinterAddr.length() == 12)
            {
                // If the MAC address only contains hex digits without the
                // ":" delimiter, then add ":" to the MAC address string.
                char[] cAddr = new char[17];

                for (int i=0, j=0; i < 12; i += 2)
                {
                    sPrinterAddr.getChars(i, i+2, cAddr, j);
                    j += 2;
                    if (j < 17)
                    {
                        cAddr[j++] = ':';
                    }
                }

                sPrinterAddr = new String(cAddr);
            }

            sPrinterURI = "bt://" + sPrinterAddr;

            LinePrinter.ExtraSettings exSettings = new LinePrinter.ExtraSettings();

            exSettings.setContext(CheckOutActivity.this);

            PrintProgressListener progressListener =
                    new PrintProgressListener()
                    {
                        @Override
                        public void receivedStatus(PrintProgressEvent aEvent)
                        {
                            // Publishes updates on the UI thread.
                            publishProgress(aEvent.getMessageType());
                        }
                    };

            try
            {
                lp = new LinePrinter(
                        jsonCmdAttribStr,
                        sPrinterID,
                        sPrinterURI,
                        exSettings);

                // Registers to listen for the print progress events.
                lp.addPrintProgressListener(progressListener);

                //A retry sequence in case the bluetooth socket is temporarily not ready
                int numtries = 0;
                int maxretry = 2;
                while(numtries < maxretry)
                {
                    try{
                        lp.connect();  // Connects to the printer
                        break;
                    }
                    catch(LinePrinterException ex){
                        numtries++;
                        Log.d("TAG", "doInBackground: "+ex.getMessage());
                        Thread.sleep(1000);
                    }
                }
                if (numtries == maxretry) lp.connect();//Final retry

                // Check the state of the printer and abort printing if there are
                // any critical errors detected.
                int[] results = lp.getStatus();
                if (results != null)
                {
                    for (int err = 0; err < results.length; err++)
                    {
                        if (results[err] == 223)
                        {
                            // Paper out.
                            throw new BadPrinterStateException("Paper out");
                        }
                        else if (results[err] == 227)
                        {
                            // Lid open.
                            throw new BadPrinterStateException("Printer lid open");
                        }
                    }
                }

                lp.setBold(true);
                lp.write("                Billing POS");
                lp.setBold(false);
                lp.newLine(1);
                lp.write("                    "+invoiceId);
                lp.newLine(2);

                lp.write("Cashier  : "+customersLists.get(customerPos).getEmpName());
                lp.newLine(1);
                lp.write("Date     : "+getTodayDate()+" "+getTodayTime());
                lp.newLine(1);
                lp.write("Customer : "+ customersLists.get(customerPos).getOutletName());
                lp.newLine(1);
                lp.write("Phone    : "+ customersLists.get(customerPos).getMobile());
                lp.newLine(1);
                lp.write("Addresss : "+customersLists.get(customerPos).getStreatName());
                lp.newLine(1);
                lp.write("Wholesaler : "+customersLists.get(customerPos).getWholesalerName());
                lp.newLine(1);
                lp.write("VAT      : "+customersLists.get(customerPos).getVATNumber());
                lp.newLine(1);
                lp.write("---------------------------------------------------------");
                lp.newLine(1);

                lp.write("Item                    "+" UOM "+"Qty "+"       Rate"+"      Total");
                lp.newLine(1);
                lp.write("---------------------------------------------------------");
                lp.newLine(1);

                for (int i = 0; i < orderList.size(); i++) {
                    String text11 = orderList.get(i).getItemName();
                    StringBuilder text123 = new StringBuilder(text11.length());
                    text123.append(orderList.get(i).getItemName());
                    int lenght = text11.length();
                    if(text11.length() < 25){
                        for (int j = 0; j < (26 - lenght); j++){
                            text123.append(" ");
                        }
                    }
                    else {
                        text123.substring(0,25);
                    }
                    Log.d(TAG, "createPDF: " + text123.length());

                    if (orderList.get(i).getCarton() > 0) {

                        String text001 = Constants.priceFormat.format(orderList.get(i).getCartonPrice());
                        int lenght1 = text001.length();
                        if(text001.length() < 11){
                            for (int j = 0; j < (12 - lenght1); j++){
                                text001 =  " "+text001;
                            }
                        }

                        String text002 = Constants.priceFormat.format(orderList.get(i).getCarton() * orderList.get(i).getCartonPrice());
                        int lenght2 = text002.length();
                        if(text002.length() < 11){
                            for (int j = 0; j < (12 - lenght2); j++){
                                text002 =  " "+text002;
                            }
                        }

                        text11 = text123.toString() + "Ct  " + orderList.get(i).getCarton() + " " + text001 +" "+text002;
                        lp.write(text11);
                        lp.newLine(1);
                    }

                    if (orderList.get(i).getPack() > 0) {
                        String text001 = Constants.priceFormat.format(orderList.get(i).getPackPrice());
                        int lenght1 = text001.length();
                        if(text001.length() < 11){
                            for (int j = 0; j < (12 - lenght1); j++){
                                text001 =  " "+text001;
                            }
                        }

                        String text002 = Constants.priceFormat.format(orderList.get(i).getPack() * orderList.get(i).getPackPrice());
                        int lenght2 = text002.length();
                        if(text002.length() < 11){
                            for (int j = 0; j < (12 - lenght2); j++){
                                text002 =  " "+text002;
                            }
                        }

                        text11 = text123.toString() + "Pk  " + orderList.get(i).getPack() + " " + text001 + " "+text002;
                        lp.write(text11);
                        lp.newLine(1);
                    }

                    if (orderList.get(i).getFreeCarton() > 0) {

                        String text001 = "0.00";
                        int lenght2 = text001.length();
                        for (int j = 0; j < (12 - lenght2); j++){
                            text001 =  " "+text001;
                        }

                        String text002 = "0.00";
                        int lenght3 = text002.length();
                        for (int j = 0; j < (12 - lenght3); j++){
                            text002 =  " "+text002;
                        }

                        text11 = text123.toString() +"Ct  " + orderList.get(i).getFreeCarton() + " " + text001+ " "+text002;
                        lp.write(text11);
                        lp.newLine(1);
                    }

                    if (orderList.get(i).getFreePack() > 0) {
                        String text001 = "0.00";
                        int lenght3 = text001.length();
                        for (int j = 0; j < (12 - lenght3); j++){
                            text001 =  " "+text001;
                        }

                        String text002 = "0.00";
                        int lenght2 = text002.length();
                        for (int j = 0; j < (12 - lenght2); j++) {
                            text002 = " " + text002;
                        }

                        text11 = text123.toString() + "Pk  " + orderList.get(i).getFreePack() + " " + text001 + " " + text002;
                        lp.write(text11);
                        lp.newLine(1);
                    }
                }

                lp.write("---------------------------------------------------------");
                lp.newLine(1);

                String text009 = Constants.priceFormat.format(myDbHelper.getTotalOrderPrice());
                int lenght9 = text009.length();
                if(text009.length() < 17){
                    for (int j = 0; j <= (17 - lenght9); j++){
                        text009 =  " "+text009;
                    }
                }
                lp.write("                         Sub Total: "+text009);
                lp.newLine(1);

                String text0010 = Constants.priceFormat.format(vat);
                int lenght10 = text0010.length();
                if(text0010.length() < 17){
                    for (int j = 0; j <= (17 - lenght10); j++){
                        text0010 =  " "+text0010;
                    }
                }
                lp.write("                          VAT(5%): "+text0010);
                lp.newLine(1);

                String text0011 = Constants.priceFormat.format(subTotal);
                int lenght11 = text0011.length();
                if(text0011.length() < 17){
                    for (int j = 0; j <= (17 - lenght11); j++){
                        text0011 =  " "+text0011;
                    }
                }
                lp.write("                             Total: "+text0011);
                lp.newLine(1);

                lp.write("---------------------------------------------------------");
                lp.newLine(1);

                String text004 = Constants.priceFormat.format(outStandingAmt);
                int lenght = text004.length();
                if(text004.length() < 17){
                    for (int j = 0; j <= (17 - lenght); j++){
                        text004 =  " "+text004;
                    }
                }
                lp.write("                  Previous Balance: "+text004);
                lp.newLine(1);

                String text0012 = Constants.priceFormat.format(subTotal);
                int lenght12 = text0012.length();
                if(text0012.length() < 17){
                    for (int j = 0; j <= (17 - lenght12); j++){
                        text0012 =  " "+text0012;
                    }
                }
                lp.write("                    Invoice Amount: "+text0011);
                lp.newLine(1);

                String text006 = Constants.priceFormat.format(amountPaid);
                int lenght1 = text006.length();
                if(text006.length() < 17){
                    for (int j = 0; j <= (17 - lenght1); j++){
                        text006 =  " "+text006;
                    }
                }
                lp.write("                       Amount Paid: "+text006);
                lp.newLine(1);


                String text007 = Constants.priceFormat.format(outStandingBaltoInsert);
                int lenght2 = text007.length();
                if(text007.length() < 17){
                    for (int j = 0; j <= (17 - lenght2); j++){
                        text007 =  " "+text007;
                    }
                }
                lp.write("               Outstanding Balance: "+text007);
                lp.newLine(2);
                lp.write("---------------------------------------------------------");
                lp.newLine(2);

                lp.write("      SIGN................................");
                lp.newLine(1);
                lp.write("      SALES MAN...........................");
                lp.newLine(1);
                lp.write("      RECEIVER............................");
                lp.newLine(2);
                lp.write("            'WE ARE PROUD TO SERVE YOU'");
                lp.newLine(1);
                lp.newLine(4);


                sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
            }
            catch (BadPrinterStateException ex)
            {
                // Stop listening for printer events.
                lp.removePrintProgressListener(progressListener);
                sResult = "Printer error detected: " + ex.getMessage() + ". Please correct the error and try again.";
            }
            catch (LinePrinterException ex)
            {
                sResult = "LinePrinterException: " + ex.getMessage();
            }
            catch (Exception ex)
            {
                if (ex.getMessage() != null)
                    sResult = "Unexpected exception: " + ex.getMessage();
                else
                    sResult = "Unexpected exception.";
            }
            finally
            {
                if (lp != null)
                {
                    try
                    {
                        lp.disconnect();  // Disconnects from the printer
                        lp.close();  // Releases resources
                    }
                    catch (Exception ex) {}
                }
            }

            // The result string will be passed to the onPostExecute method
            // for display in the the Progress and Status text box.
            return sResult;
        }

        /**
         * Runs on the UI thread after doInBackground method. The specified
         * result parameter is the value returned by doInBackground.
         */
        @Override
        protected void onPostExecute(String result)
        {
            // Displays the result (number of bytes sent to the printer or
            // exception message) in the Progress and Status text box.
            if (result != null)
            {
                if(result.contains("Number of bytes sent to printer:")) {
                    myDbHelper.deleteOrderTable();
                    Intent intent = new Intent(CheckOutActivity.this, DashboardActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                else {
                    Toast.makeText(CheckOutActivity.this, result, Toast.LENGTH_SHORT).show();
                }
            }

            if(dialog != null) {
                dialog.dismiss();
            }
        }
    } //endofclass PrintTask

    private void offlineDataSave(){
        String inputStr = prepareJson();
        Log.i("TAG", "offlineDataSave: " );

        HashMap<String, String> values = new HashMap<>();
        values.put("Data", inputStr);
        values.put("Type", "checkout");
        values.put("Status", "1");

        myDbHelper.insertOfflineData(values);
        insertOrder();
    }
}

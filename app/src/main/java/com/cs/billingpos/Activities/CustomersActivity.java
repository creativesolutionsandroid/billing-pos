package com.cs.billingpos.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.cs.billingpos.Adapters.CustomersAdapter;
import com.cs.billingpos.Models.Customers;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.R;

import java.util.ArrayList;

public class CustomersActivity extends Activity {

    private ArrayList<RoutePlanDetailsResponse.CustomersList> customersLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.MarketList> marketLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.RegionList> regionLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.AreaList> areaLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.CityList> cityLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.ZoneList> zoneLists = new ArrayList<>();
    ArrayList<Customers> zoneList = new ArrayList<>();
    CustomersAdapter mAdapter;
    ListView customersList;
    ImageView add;
    TextView addCustomer;
    Boolean isCustVisible = false;
    ImageView backbtn;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers);

        customersLists = (ArrayList<RoutePlanDetailsResponse.CustomersList>) getIntent().getSerializableExtra("customer");
        marketLists = (ArrayList<RoutePlanDetailsResponse.MarketList>) getIntent().getSerializableExtra("market");
        regionLists = (ArrayList<RoutePlanDetailsResponse.RegionList>) getIntent().getSerializableExtra("region");
        areaLists = (ArrayList<RoutePlanDetailsResponse.AreaList>) getIntent().getSerializableExtra("area");
        cityLists = (ArrayList<RoutePlanDetailsResponse.CityList>) getIntent().getSerializableExtra("city");
        zoneLists = (ArrayList<RoutePlanDetailsResponse.ZoneList>) getIntent().getSerializableExtra("zone");

        add = (ImageView) findViewById(R.id.add);
        addCustomer = (TextView) findViewById(R.id.add_customer);
        customersList = (ListView)  findViewById(R.id.customers_list);
        backbtn = (ImageView) findViewById(R.id.back_btn);
        createZones();

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isCustVisible) {
                    isCustVisible = false;
                    addCustomer.setVisibility(View.GONE);
                }
                else {
                    isCustVisible = true;
                    addCustomer.setVisibility(View.VISIBLE);
                }
            }
        });

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CustomersActivity.this, AddCustomerActivity.class);
                intent.putExtra("market", marketLists);
                intent.putExtra("region", regionLists);
                intent.putExtra("area", areaLists);
                intent.putExtra("city", cityLists);
                intent.putExtra("zone", zoneLists);
                startActivity(intent);
                finish();
            }
        });
    }

    private void createZones(){
        ArrayList<String> dummyList = new ArrayList<>();
        for (int i = 0; i < customersLists.size(); i++){
            Customers customers = new Customers();
            if(!dummyList.contains(customersLists.get(i).getCity())) {
                dummyList.add(customersLists.get(i).getCity());
                customers.setZone(customersLists.get(i).getCity());
                customers.setSeen(false);
                ArrayList<Customers.customerList> cList = new ArrayList<>();
                for (int j = 0; j < customersLists.size(); j++) {
                    if(customersLists.get(i).getCity().equalsIgnoreCase(customersLists.get(j).getCity())){
                        Customers.customerList c = new Customers.customerList();
                        c.setCustomer(customersLists.get(j).getOutletName());
                        c.setArea(customersLists.get(j).getArea());
                        c.setMobile(customersLists.get(j).getMobileNo());
                        c.setEmail(customersLists.get(j).getEmail());
                        c.setOutstanding(customersLists.get(j).getOutStandingBalance());
                        c.setTrackingNo(customersLists.get(j).getTrackingNo());
                        c.setSeen(false);
                        cList.add(c);
                    }
                }
                Log.d("TAG", "createZones: "+cList.size());
                customers.setList(cList);
                zoneList.add(customers);
            }
        }

        mAdapter = new CustomersAdapter(CustomersActivity.this, zoneList);
        customersList.setAdapter(mAdapter);
    }
}

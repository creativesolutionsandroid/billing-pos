package com.cs.billingpos.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.billingpos.Models.InsertProductDetails;
import com.cs.billingpos.Models.MenuItemsWithoutSub;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.R;
import com.cs.billingpos.Rest.APIInterface;
import com.cs.billingpos.Rest.ApiClient;
import com.cs.billingpos.Utils.Constants;
import com.cs.billingpos.Utils.NetworkUtil;
import com.google.android.gms.vision.barcode.Barcode;
import com.ivan200.photobarcodelib.PhotoBarcodeScanner;
import com.ivan200.photobarcodelib.PhotoBarcodeScannerBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Puli on 22-04-2019.
 */

public class AddProductActivity extends Activity {

    public Context context;
    public LayoutInflater inflater;
    Button save_btn;
    private String TAG = "TAG";
    Spinner categoryspinnerlist;
    EditText name,brand,company,measureunit, pack_per_carton, cat;
    ImageView packimage,backbtn;
    String strName, strbrand, strcompany, strmeasureunit, strpackpercarton;
    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };
    boolean isCamera = false;
    private LinearLayout categoryList;
    String str;
    RelativeLayout mimage_layout;
    private int categorySelected = 0;
    private ArrayList<String> categoryNames = new ArrayList<>();
    private ArrayList<Integer> categoryId = new ArrayList<>();
    private ArrayList<MenuItemsWithoutSub> menuItemsList = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.SKUlist> skUlists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList> nonTobaccoSKULists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.CategoryList>  categoryLists = new ArrayList<>();

    private ArrayAdapter<String> categoryAdapter;
    SharedPreferences userPrefs;
    String userId ;
    Bitmap thumbnail;
    String pictureName = "";
    String imageResponse;
    private DefaultHttpClient mHttpClient11;
    boolean isImageUploaded = false;
    RelativeLayout packCodeLayout, cartonCodeLayout;
    TextView packCodeEt, cartonCodeEt;
    String packCode = "", cartonCode = "";
    PhotoBarcodeScanner photoBarcodeScanner, photoBarcodeScanner1;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_product_screen);

        packCodeEt = (TextView)findViewById(R.id.pack_code);
        cartonCodeEt = (TextView)findViewById(R.id.carton_code);
        name = (EditText)findViewById(R.id.ap_name);
        brand = (EditText)findViewById(R.id.ap_brand);
        company = (EditText)findViewById(R.id.ap_company);
        measureunit = (EditText)findViewById(R.id.ap_units);
        pack_per_carton = (EditText)findViewById(R.id.pack_per_carton);
        packimage = (ImageView) findViewById(R.id.packimage);
        backbtn =(ImageView)findViewById(R.id.back_btn);
        categoryList = (LinearLayout) findViewById(R.id.categories);
        save_btn =(Button) findViewById(R.id.save_btn);
        categoryspinnerlist = findViewById(R.id.catlist);
        cat = findViewById(R.id.ap_cat);
        mimage_layout = (RelativeLayout) findViewById(R.id.image_layout);
        packCodeLayout = (RelativeLayout) findViewById(R.id.pack_barcode_layout);
        cartonCodeLayout = (RelativeLayout) findViewById(R.id.carton_barcode_layout);

        categoryLists = (ArrayList<RoutePlanDetailsResponse.CategoryList>) getIntent().getSerializableExtra("catlist");
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("EmpId","0");

        try {
            for (RoutePlanDetailsResponse.CategoryList brand : categoryLists){
                categoryNames.add(brand.getSKUCategoryName());
                categoryId.add(brand.getSKUCategoryId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        cat.setEnabled(false);

        categoryAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, categoryNames) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.LEFT);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        categoryspinnerlist.setAdapter(categoryAdapter);

        categoryspinnerlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                for (int j = 0; j < categoryLists.size(); j++) {
                    if (categoryNames.get(i).equals(categoryLists.get(j).getSKUCategoryName())){

                        categorySelected = categoryLists.get(j).getSKUCategoryId();

                    }
                }
                cat.setText(categoryNames.get(i));

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        packimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "onClick: " + categoryNames);
                openGallery();
            }
        });
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        packCodeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoBarcodeScanner = new PhotoBarcodeScannerBuilder()
                        .withActivity(AddProductActivity.this)
                        .withResultListener((Barcode barcode) -> {
                            packCode = barcode.rawValue;
                            packCodeEt.setText(barcode.rawValue);
                        })
                        .build();
                photoBarcodeScanner.start();
            }
        });


        cartonCodeEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoBarcodeScanner1 = new PhotoBarcodeScannerBuilder()
                        .withActivity(AddProductActivity.this)
                        .withResultListener((Barcode barcode) -> {
                            cartonCode = barcode.rawValue;
                            cartonCodeEt.setText(barcode.rawValue);
                        })
                        .build();
                photoBarcodeScanner1.start();
            }
        });

        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validations()){
                    String networkStatus = NetworkUtil.getConnectivityStatusString(AddProductActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (isImageUploaded) {
                            new uploadImagesApi().execute();
                        }
                        else {
                            new InsertProductionDetail().execute();
                        }

                    }
                    else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

    }

    private class InsertProductionDetail extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            dialog = new ACProgressFlower.Builder(AddProductActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(AddProductActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<InsertProductDetails> call = apiService.GetInsertproduction(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<InsertProductDetails>() {
                @Override
                public void onResponse(Call<InsertProductDetails> call, Response<InsertProductDetails> response) {
                    if (response.isSuccessful()) {
                        Log.d(TAG, "onResponse: "+response);
                        InsertProductDetails verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getSuccess().getMessage().equals("Values Inserted Successfully")) {

                                finish();
//                                data = verifyMobileResponse.getData().get(0).getWr();
//                                if(data.size() == 0){
//                                    Constants.showOneButtonAlertDialog("No history found", getResources().getString(R.string.app_name),
//                                            getResources().getString(R.string.ok), getActivity());
//                                }
//                                mAdapter = new FeedbackAdapter(getActivity(), data, "En");
//                                mListView.setAdapter(mAdapter);
//                                setData();
//
//                                try {
//                                    name.setText(verifyMobileResponse.getData().get(0).getNameEn());
//                                    Glide.with(FeedbackFragment.this)
//                                            .load(Constants.WORKSHOP_IMAGES+verifyMobileResponse.getData().get(0).getLogo())
//                                            .into(profilePic);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
                            } else {
                                //                          status false case
                                String failureResponse = verifyMobileResponse.getSuccess().getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), AddProductActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(AddProductActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(AddProductActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<InsertProductDetails> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(AddProductActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddProductActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson() {
        JSONObject parentObj = new JSONObject();
        try {
            JSONObject addproduct = new JSONObject();

            addproduct.put("CategoryId", categorySelected);
            addproduct.put("ProductName", name.getText().toString());
            addproduct.put("BrandName", brand.getText().toString());
            addproduct.put("CompanyName", company.getText().toString());
            addproduct.put("MeasureUnit", measureunit.getText().toString());
            addproduct.put("PackBarCode", packCode);
            addproduct.put("PackPerCarton", pack_per_carton.getText().toString());
            addproduct.put("CartonBarCode", cartonCode);
            addproduct.put("PackImage", pictureName);
            addproduct.put("SalesmanId", userId);

            parentObj.put("AddProductDetails", addproduct);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }

    private boolean validations() {
        strName = name.getText().toString().trim();
        strbrand = brand.getText().toString().trim();
        strcompany = company.getText().toString().trim();
        strmeasureunit = measureunit.getText().toString().trim();
        strpackpercarton = pack_per_carton.getText().toString().trim();

        if (strName.length() == 0) {
            name.setError(getResources().getString(R.string.produt_name_invalid_name));
            return false;
        }
        else if (strbrand.length() == 0) {

            brand.setError(getResources().getString(R.string.produt_brand_enter_brand));
            return false;
        }

        else if (strcompany.length() == 0) {

            company.setError(getResources().getString(R.string.produt_brand_enter_company));
            return false;
        }
        else if (strmeasureunit.length() == 0) {

            measureunit.setError(getResources().getString(R.string.produt_brand_enter_units));
            return false;
        }

        else if (strpackpercarton.length() == 0) {

            pack_per_carton.setError(getResources().getString(R.string.produt_brand_enter_pack_per_carton));
            return false;
        }
        else {

            return true;
        }
    }


    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(AddProductActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case STORAGE_REQUEST:

                if (canAccessStorage()) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                    intent.setAction(Intent.ACTION_GET_CONTENT);

                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
                else {
                    Toast.makeText(AddProductActivity.this, "Storage permission denied, Unable to add item image", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
    public void openGallery() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessStorage()) {
                requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                isCamera=false;
            } else {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
            }
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
            convertPixel();
        }
    }

    private void convertPixel() {
        thumbnail = Bitmap.createScaledBitmap(thumbnail, 900, 800,
                false);
        thumbnail = codec(thumbnail, Bitmap.CompressFormat.JPEG, 100);
        Log.d("TAG", "convertPixel: "+thumbnail.getByteCount());
        packimage.setImageBitmap(thumbnail);
        isImageUploaded = true;
    }

    private Bitmap codec(Bitmap src, Bitmap.CompressFormat format, int quality) {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);

    }

    private class uploadImagesApi extends AsyncTask<String, String, String> {
        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ACProgressFlower.Builder(AddProductActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... str) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(AddProductActivity.this);

            HttpResponse httpResponse = null;
            int count = 0, usedCount = 0;

            MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            mHttpClient11 = new DefaultHttpClient(params);
            HttpPost httppost = new HttpPost(Constants.IMAGE_UPLOAD_URL);

            try{
                String imagePath1 = "";
                File file=null;
                if (thumbnail != null) {
                    imagePath1 = StoreByteImage(thumbnail, "back");
                }
                if (imagePath1 != null) {
                    file = new File(imagePath1);
                }
                if (null != file && file.exists()) {
                    multipartEntity.addPart("image", new FileBody(file));
                }
                httppost.setEntity(multipartEntity);
                httpResponse=  mHttpClient11.execute(httppost);
            }catch(Exception e){
                e.printStackTrace();
            }

            InputStream entity = null;
            try {
                entity = httpResponse.getEntity().getContent();
                if(entity != null) {
                    imageResponse = convertInputStreamToString(entity);
                    Log.i("TAG", "user response:" + imageResponse);
                }
            }catch(NullPointerException e)
            {
                e.printStackTrace();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            // TODO Auto-generated method stub
            super.onPostExecute(s);
            if(imageResponse!=null && imageResponse.length()>0) {
                try {
//                    JSONArray resultArray = new JSONArray(imageResponse);
                    new InsertProductionDetail().execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else{
                    Toast.makeText(AddProductActivity.this, "upload failed.please try again", Toast.LENGTH_SHORT).show();
            }
            if(dialog!=null){
                dialog.dismiss();
            }
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    public String StoreByteImage(Bitmap bitmap, String type) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
        pictureName = "PIC_" + userId + "_" + timeStamp + ".jpg";

        File sdImageMainDirectory = new File("/sdcard/"+ pictureName);

        FileOutputStream fileOutputStream = null;

        String nameFile = null;

        try {

            BitmapFactory.Options options=new BitmapFactory.Options();

            fileOutputStream = new FileOutputStream(sdImageMainDirectory);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            bitmap.compress(Bitmap.CompressFormat.JPEG,100, bos);

            bos.flush();

            bos.close();

        } catch (FileNotFoundException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();

        } catch (IOException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();
        }
        return sdImageMainDirectory.getAbsolutePath();
    }
}

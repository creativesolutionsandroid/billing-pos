package com.cs.billingpos.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.cs.billingpos.Adapters.InvocieEditAdapter;
import com.cs.billingpos.Models.CartData;
import com.cs.billingpos.Models.InvoiceDetails;
import com.cs.billingpos.Models.OfflineData;
import com.cs.billingpos.Models.Order;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.MyDatabase;
import com.cs.billingpos.R;
import com.cs.billingpos.Utils.Constants;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

public class InvoiceEditActivity extends Activity {

    TextView salesAmount;
    ImageView backBtn;
    ListView listView;
    InvocieEditAdapter mAdapter;
    private ArrayList<RoutePlanDetailsResponse.SKUlist> skuList = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList> nonTobaccoSKUList = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.Salesinvoices> salesinvoices = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.Salesinvoices> salesinvoicesEdited = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.RoutePlanDetail> customersList = new ArrayList<>();
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN
    };
    private static final int LOCATION_REQUEST = 3;
    private MyDatabase myDbHelper;
    private ArrayList<OfflineData> offlineList = new ArrayList<>();
    private boolean isOffline = false;
    private long trackingNo;
    private float outstanding;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_invoice);

        myDbHelper = new MyDatabase(InvoiceEditActivity.this);
        offlineList = myDbHelper.getOfflineDatalist();

        customersList = (ArrayList<RoutePlanDetailsResponse.RoutePlanDetail>) getIntent().getSerializableExtra("customer");
        skuList = (ArrayList<RoutePlanDetailsResponse.SKUlist>) getIntent().getSerializableExtra("sku");
        nonTobaccoSKUList = (ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList>) getIntent().getSerializableExtra("non_tobacco");
        salesinvoices = (ArrayList<RoutePlanDetailsResponse.Salesinvoices>) getIntent().getSerializableExtra("sales");

        backBtn = (ImageView) findViewById(R.id.back_btn);
        salesAmount = (TextView) findViewById(R.id.sales_amount);
        listView = (ListView) findViewById(R.id.customers_list);

        if(offlineList.size() > 0) {
            for (int i = 0; i < offlineList.size(); i++) {
                if(offlineList.get(i).getType().equals("checkout")) {
                    Gson json = new Gson();
                    CartData cartData = json.fromJson(offlineList.get(i).getData(), CartData.class);

                    RoutePlanDetailsResponse.Salesinvoices invoiceDetails = new RoutePlanDetailsResponse.Salesinvoices();
                    invoiceDetails.setId(cartData.getSalesmaster().getTrackingno());
                    invoiceDetails.setCustomername(cartData.getSalesmaster().getCustomerName());
                    invoiceDetails.setInvoiceno(cartData.getSalesmaster().getInvoiceno());
                    invoiceDetails.setTotalsales(cartData.getSalesmaster().getGrandtotalamount());
                    salesinvoicesEdited.add(invoiceDetails);

                    if(!isOffline) {
                        for (int j = 0; j < cartData.getSalesdetail().size(); j++) {
                            HashMap<String, String> values = new HashMap<>();

                            double cartonPrice = cartData.getSalesdetail().get(j).getCarton() * cartData.getSalesdetail().get(j).getCartonprice();
                            double packPrice = cartData.getSalesdetail().get(j).getPack() * cartData.getSalesdetail().get(j).getPackprice();

                            values.put("TypeId", "" + cartData.getSalesdetail().get(j).getTypeid());
                            values.put("SKUId", "" + cartData.getSalesdetail().get(j).getSkuid());
                            values.put("Carton", "" + cartData.getSalesdetail().get(j).getCarton());
                            values.put("Pack", "" + cartData.getSalesdetail().get(j).getPack());
                            values.put("FreeCarton", "" + cartData.getSalesdetail().get(j).getFreecarton());
                            values.put("FreePack", "" + cartData.getSalesdetail().get(j).getFreepack());
                            values.put("TotalCarton", "" + (cartData.getSalesdetail().get(j).getCarton() + cartData.getSalesdetail().get(j).getFreecarton()));
                            values.put("TotalPack", "" + (cartData.getSalesdetail().get(j).getPack() + cartData.getSalesdetail().get(j).getFreepack()));

                            values.put("CartonPrice", "" + cartData.getSalesdetail().get(j).getCartonprice());
                            values.put("PackPrice", "" + cartData.getSalesdetail().get(j).getPackprice());
                            values.put("TotalCartonPrice", "" + cartonPrice);
                            values.put("TotalPackPrice", "" + packPrice);
                            values.put("TotalPrice", "" + (cartonPrice + packPrice));

                            if (cartData.getSalesdetail().get(j).getTypeid() == 1) {
                                for (int k = 0; k < skuList.size(); k++) {
                                    if (cartData.getSalesdetail().get(j).getSkuid() == skuList.get(k).getSKUId()) {
                                        values.put("itemName", skuList.get(k).getSKUName());
                                        values.put("packPerCarton", "" + skuList.get(k).getPacksPerCarton());
                                        values.put("subCatId", "" + skuList.get(k).getCompanyName());
                                        break;
                                    }
                                }
                            } else if (cartData.getSalesdetail().get(j).getTypeid() == 2) {
                                for (int k = 0; k < nonTobaccoSKUList.size(); k++) {
                                    if (cartData.getSalesdetail().get(j).getSkuid() == nonTobaccoSKUList.get(k).getSKUId()) {
                                        values.put("itemName", nonTobaccoSKUList.get(k).getSKUName());
                                        values.put("packPerCarton", "" + nonTobaccoSKUList.get(k).getPackPerCarton());
                                        values.put("subCatId", "" + nonTobaccoSKUList.get(k).getCompanyName());
                                        break;
                                    }
                                }
                            }
                            values.put("invoiceId", "0" );
                            values.put("invoiceNumber", "" + cartData.getSalesdetail().get(j).getInvoiceno());
                            values.put("outstandingId", "0");
                            values.put("outletName", cartData.getSalesmaster().getCustomerName());
                            myDbHelper.insertOrder(values);

                            isOffline = true;
                            trackingNo = cartData.getSalesmaster().getTrackingno();
                            outstanding = cartData.getOutstandingbalance().getPreviousoutstanding();
                        }
                    }
                }
            }
        }
        salesinvoicesEdited.addAll(salesinvoices);


        mAdapter = new InvocieEditAdapter(InvoiceEditActivity.this, salesinvoicesEdited, skuList, nonTobaccoSKUList, customersList, isOffline, trackingNo, outstanding);
        listView.setAdapter(mAdapter);

        salesAmount.setText("("+ Constants.priceFormat.format(getIntent().getDoubleExtra("amount", 0))+")");

        if(!canAccessStorgae() && !canReadStorgae()) {
            requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessStorgae() {
        return (hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canReadStorgae() {
        return (hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(InvoiceEditActivity.this, perm));
    }
}

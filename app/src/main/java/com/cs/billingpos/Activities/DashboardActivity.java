package com.cs.billingpos.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.billingpos.Models.InsertPromoterResponse;
import com.cs.billingpos.Models.InsertStockDetails;
import com.cs.billingpos.Models.OfflineData;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.MyDatabase;
import com.cs.billingpos.R;
import com.cs.billingpos.Rest.APIInterface;
import com.cs.billingpos.Rest.ApiClient;
import com.cs.billingpos.Utils.Constants;
import com.cs.billingpos.Utils.NetworkUtil;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends AppCompatActivity {

    private String empId;
    private String TAG = "TAG";
    private ArrayList<RoutePlanDetailsResponse.RoutePlanDetail> routePlanDetails = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.SKUlist> skuList = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList> nonTobaccoSKUList = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.CustomersList> customersList = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.MarketList> marketLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.RegionList> regionLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.AreaList> areaLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.CityList> cityLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.ZoneList> zoneLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.BrandList> BrandLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.Salesinvoices> salesinvoices = new ArrayList<>();
    private double outstandingBalance;
    private double todaySales;
    private RelativeLayout billingLayout, customerLayout, productLayout, reportLayout;
    private ImageView settings;
    private SharedPreferences userPrefs;
    private SharedPreferences.Editor userPrefsEditor;
    private ImageView logout;
    AlertDialog customDialog;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 3;
    private ArrayList<RoutePlanDetailsResponse.CategoryList>  categoryLists = new ArrayList<>();

    RelativeLayout dailySalesLayout;
    private TextView tvTodaySales, tvOutstandingBal, customerName, customerCount;
    ImageView sync;
    MyDatabase myDbHelper;
    ACProgressFlower progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);
        myDbHelper = new MyDatabase(DashboardActivity.this);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        empId = userPrefs.getString("EmpId", "2");
        Log.d(TAG, "onCreate: "+empId);

        sync = (ImageView) findViewById(R.id.sync);
        settings = (ImageView) findViewById(R.id.settings);
        tvTodaySales = (TextView) findViewById(R.id.dailysales);
        tvOutstandingBal = (TextView) findViewById(R.id.outstandingBalance);
        customerName = (TextView) findViewById(R.id.customer_name);
        customerCount = (TextView) findViewById(R.id.customer_count);
        logout = (ImageView) findViewById(R.id.logout);

        dailySalesLayout = (RelativeLayout) findViewById(R.id.daily_sales_layout);
        billingLayout = (RelativeLayout) findViewById(R.id.billing_layout);
        customerLayout = (RelativeLayout) findViewById(R.id.customers_layout);
        productLayout = (RelativeLayout) findViewById(R.id.products_layout);
        reportLayout = (RelativeLayout) findViewById(R.id.reports_layout);

//        String networkStatus = NetworkUtil.getConnectivityStatusString(DashboardActivity.this);
//        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//            dashboardApi();
//        }
//        else {
//            if (userPrefs.getString("json", "").equals("")) {
//                dashboardApi();
//            } else {
//                Gson json = new Gson();
//                RoutePlanDetailsResponse routeResponse = json.fromJson(userPrefs.getString("json", ""), RoutePlanDetailsResponse.class);
//                routePlanDetails.addAll(routeResponse.getRoutePlanDetails());
//                customersList.addAll(routeResponse.getCustomersList());
//                categoryLists.addAll(routeResponse.getCategoryList());
//                skuList.addAll(routeResponse.getSKUlist());
//                nonTobaccoSKUList.addAll(routeResponse.getNonTobaccoSKUList());
//                outstandingBalance = routeResponse.getTotaloutstandingBalance();
//                todaySales = routeResponse.getTodaysales();
//                initView();
//            }
//        }

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            }
        }

        sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String networkStatus = NetworkUtil.getConnectivityStatusString(DashboardActivity.this);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    dataSync();
                }
                else {
                    Toast.makeText(DashboardActivity.this, R.string.connection_error, Toast.LENGTH_SHORT).show();
                }
//                Toast.makeText(DashboardActivity.this, "No data found", Toast.LENGTH_SHORT).show();
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showtwoButtonsAlertDialog();
            }
        });

        billingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(routePlanDetails.size()> 0) {
                    Intent intent = new Intent(DashboardActivity.this, BillingActivity.class);
                    intent.putExtra("customer", routePlanDetails);
                    intent.putExtra("sku", skuList);
                    intent.putExtra("non_tobacco", nonTobaccoSKUList);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(DashboardActivity.this, "Route plan not assigned", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dailySalesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(salesinvoices.size()> 0) {
                    Intent intent = new Intent(DashboardActivity.this, InvoiceEditActivity.class);
                    intent.putExtra("customer", routePlanDetails);
                    intent.putExtra("sku", skuList);
                    intent.putExtra("non_tobacco", nonTobaccoSKUList);
                    intent.putExtra("sales", salesinvoices);
                    intent.putExtra("amount", todaySales);
                    startActivity(intent);
                }
                else {
                }
            }
        });

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardActivity.this, ChangePasswordActivity.class));
            }
        });

        customerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, CustomersActivity.class);
                intent.putExtra("customer", customersList);
                intent.putExtra("market", marketLists);
                intent.putExtra("region", regionLists);
                intent.putExtra("area", areaLists);
                intent.putExtra("city", cityLists);
                intent.putExtra("zone", zoneLists);
                startActivity(intent);
            }
        });

        productLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, ProductActivity.class);
                intent.putExtra("catlist", categoryLists);
                intent.putExtra("sku", skuList);
                intent.putExtra("non_tobacco", nonTobaccoSKUList);
                startActivity(intent);
            }
        });

        reportLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, ReportsActivity.class);
                intent.putExtra("brands", BrandLists);
                intent.putExtra("sku", skuList);
                startActivity(intent);
            }
        });
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(DashboardActivity.this, perm));
    }

    private void dashboardApi(){
        final ACProgressFlower dialog = new ACProgressFlower.Builder(DashboardActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();;
        dialog.show();

        final String networkStatus = NetworkUtil.getConnectivityStatusString(DashboardActivity.this);

        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<RoutePlanDetailsResponse> call = apiService.dashboard(getTodayDate(), empId);
        call.enqueue(new Callback<RoutePlanDetailsResponse>() {

            public void onResponse(Call<RoutePlanDetailsResponse> call, Response<RoutePlanDetailsResponse> response) {
                if(response.isSuccessful()){
                    RoutePlanDetailsResponse routeResponse = response.body();
                    try {
                        //status true case
                        routePlanDetails.clear();
                        customersList.clear();
                        categoryLists.clear();
                        skuList.clear();
                        nonTobaccoSKUList.clear();
                        marketLists.clear();
                        areaLists.clear();
                        zoneLists.clear();
                        cityLists.clear();
                        regionLists.clear();
                        BrandLists.clear();
                        salesinvoices.clear();
                        salesinvoices.addAll(routeResponse.getSalesinvoices());
                        BrandLists.addAll(routeResponse.getBrandList());
                        routePlanDetails.addAll(routeResponse.getRoutePlanDetails());
                        customersList.addAll(routeResponse.getCustomersList());
                        categoryLists.addAll(routeResponse.getCategoryList());
                        skuList.addAll(routeResponse.getSKUlist());
                        nonTobaccoSKUList.addAll(routeResponse.getNonTobaccoSKUList());
                        marketLists.addAll(routeResponse.getMarketList());
                        areaLists.addAll(routeResponse.getAreaList());
                        zoneLists.addAll(routeResponse.getZoneList());
                        cityLists.addAll(routeResponse.getCityList());
                        regionLists.addAll(routeResponse.getRegionList());
                        outstandingBalance = routeResponse.getTotaloutstandingBalance();
                        todaySales = routeResponse.getTodaysales();
                        Gson gson = new Gson();
                        String json = gson.toJson(routeResponse);
                        userPrefsEditor.putString("json", json);
                        userPrefsEditor.commit();
                        initView();
                    } catch (Exception e) {
                        Gson json = new Gson();
                        RoutePlanDetailsResponse routeResponse12 = json.fromJson(userPrefs.getString("json", ""), RoutePlanDetailsResponse.class);
                        if(!routeResponse12.equals("")) {
                            routePlanDetails.clear();
                            customersList.clear();
                            categoryLists.clear();
                            skuList.clear();
                            nonTobaccoSKUList.clear();
                            marketLists.clear();
                            areaLists.clear();
                            zoneLists.clear();
                            cityLists.clear();
                            regionLists.clear();
                            BrandLists.clear();
                            salesinvoices.clear();
                            salesinvoices.addAll(routeResponse.getSalesinvoices());
                            BrandLists.addAll(routeResponse.getBrandList());
                            routePlanDetails.addAll(routeResponse12.getRoutePlanDetails());
                            customersList.addAll(routeResponse12.getCustomersList());
                            categoryLists.addAll(routeResponse12.getCategoryList());
                            skuList.addAll(routeResponse12.getSKUlist());
                            nonTobaccoSKUList.addAll(routeResponse12.getNonTobaccoSKUList());
                            marketLists.addAll(routeResponse12.getMarketList());
                            areaLists.addAll(routeResponse12.getAreaList());
                            zoneLists.addAll(routeResponse12.getZoneList());
                            cityLists.addAll(routeResponse12.getCityList());
                            regionLists.addAll(routeResponse12.getRegionList());
                            outstandingBalance = routeResponse12.getTotaloutstandingBalance();
                            todaySales = routeResponse12.getTodaysales();
                            initView();
                        }
                        else {
                            dashboardApi();
                        }
//                        Toast.makeText(DashboardActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if(dialog != null)
                        dialog.dismiss();
                }
                else{
                    try {
                        Log.d(TAG, "onResponse: "+response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(dialog != null)
                        dialog.dismiss();
                    Gson json = new Gson();
                    RoutePlanDetailsResponse routeResponse12 = json.fromJson(userPrefs.getString("json", ""), RoutePlanDetailsResponse.class);
                    if(!routeResponse12.equals("")) {
                        routePlanDetails.clear();
                        customersList.clear();
                        categoryLists.clear();
                        skuList.clear();
                        nonTobaccoSKUList.clear();
                        marketLists.clear();
                        areaLists.clear();
                        zoneLists.clear();
                        cityLists.clear();
                        regionLists.clear();
                        BrandLists.clear();
                        salesinvoices.clear();
                        salesinvoices.addAll(routeResponse12.getSalesinvoices());
                        BrandLists.addAll(routeResponse12.getBrandList());
                        routePlanDetails.addAll(routeResponse12.getRoutePlanDetails());
                        customersList.addAll(routeResponse12.getCustomersList());
                        categoryLists.addAll(routeResponse12.getCategoryList());
                        skuList.addAll(routeResponse12.getSKUlist());
                        nonTobaccoSKUList.addAll(routeResponse12.getNonTobaccoSKUList());
                        marketLists.addAll(routeResponse12.getMarketList());
                        areaLists.addAll(routeResponse12.getAreaList());
                        zoneLists.addAll(routeResponse12.getZoneList());
                        cityLists.addAll(routeResponse12.getCityList());
                        regionLists.addAll(routeResponse12.getRegionList());
                        outstandingBalance = routeResponse12.getTotaloutstandingBalance();
                        todaySales = routeResponse12.getTodaysales();
                        initView();
                    }
                    else {
                        dashboardApi();
                    }
//                    Toast.makeText(DashboardActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
            }

            public void onFailure(Call<RoutePlanDetailsResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                    Toast.makeText(DashboardActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }
                else {
//                    Toast.makeText(DashboardActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                Gson json = new Gson();
                RoutePlanDetailsResponse routeResponse12 = json.fromJson(userPrefs.getString("json", ""), RoutePlanDetailsResponse.class);
                if(routeResponse12 != null && !routeResponse12.equals("")) {
                    routePlanDetails.clear();
                    customersList.clear();
                    categoryLists.clear();
                    skuList.clear();
                    nonTobaccoSKUList.clear();
                    marketLists.clear();
                    areaLists.clear();
                    zoneLists.clear();
                    cityLists.clear();
                    regionLists.clear();
                    BrandLists.clear();
                    BrandLists.addAll(routeResponse12.getBrandList());
                    salesinvoices.clear();
                    salesinvoices.addAll(routeResponse12.getSalesinvoices());
                    routePlanDetails.addAll(routeResponse12.getRoutePlanDetails());
                    customersList.addAll(routeResponse12.getCustomersList());
                    categoryLists.addAll(routeResponse12.getCategoryList());
                    skuList.addAll(routeResponse12.getSKUlist());
                    nonTobaccoSKUList.addAll(routeResponse12.getNonTobaccoSKUList());
                    marketLists.addAll(routeResponse12.getMarketList());
                    areaLists.addAll(routeResponse12.getAreaList());
                    zoneLists.addAll(routeResponse12.getZoneList());
                    cityLists.addAll(routeResponse12.getCityList());
                    regionLists.addAll(routeResponse12.getRegionList());
                    outstandingBalance = routeResponse12.getTotaloutstandingBalance();
                    todaySales = routeResponse12.getTodaysales();
                    initView();
                }
                else {
                    dashboardApi();
                }
                if(dialog != null)
                    dialog.dismiss();
            }
        });
    }

    private String getTodayDate(){
        String dateStr = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        Calendar calendar = Calendar.getInstance();
        dateStr = sdf.format(calendar.getTime());

        return dateStr;
    }

    private void initView() {
        tvTodaySales.setText(Constants.priceFormat.format(todaySales));
        tvOutstandingBal.setText(Constants.priceFormat.format(outstandingBalance));
        if(customersList.size() > 0) {
            customerName.setText(customersList.get(0).getSalesmanName());
            customerCount.setText(""+customersList.size());
            customerCount.setVisibility(View.VISIBLE);
        }
    }

    public void showtwoButtonsAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(DashboardActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        final TextView title = (TextView) dialogView.findViewById(R.id.title);
        final TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        final TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        final TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

        title.setText("Billing Pos");
        yes.setText("Yes");
        no.setText("No");
        desc.setText("Do you want to logout?");

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userPrefsEditor.clear();
                userPrefsEditor.commit();
                Intent i = new Intent(DashboardActivity.this, SignInActivity.class);
                startActivity(i);
                finish();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ArrayList<OfflineData> offlineData = new ArrayList<>();
        offlineData = myDbHelper.getOfflineDatalist();

        if(offlineData.size() != 0) {
            String networkStatus = NetworkUtil.getConnectivityStatusString(DashboardActivity.this);
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                dashboardApi();
            } else {
                if (userPrefs.getString("json", "").equals("")) {
                    dashboardApi();
                } else {
                    Gson json = new Gson();
                    RoutePlanDetailsResponse routeResponse = json.fromJson(userPrefs.getString("json", ""), RoutePlanDetailsResponse.class);
                    routePlanDetails.clear();
                    customersList.clear();
                    categoryLists.clear();
                    skuList.clear();
                    nonTobaccoSKUList.clear();
                    marketLists.clear();
                    areaLists.clear();
                    zoneLists.clear();
                    cityLists.clear();
                    regionLists.clear();
                    BrandLists.clear();
                    BrandLists.addAll(routeResponse.getBrandList());
                    salesinvoices.clear();
                    salesinvoices.addAll(routeResponse.getSalesinvoices());
                    routePlanDetails.addAll(routeResponse.getRoutePlanDetails());
                    customersList.addAll(routeResponse.getCustomersList());
                    categoryLists.addAll(routeResponse.getCategoryList());
                    skuList.addAll(routeResponse.getSKUlist());
                    nonTobaccoSKUList.addAll(routeResponse.getNonTobaccoSKUList());
                    marketLists.addAll(routeResponse.getMarketList());
                    areaLists.addAll(routeResponse.getAreaList());
                    zoneLists.addAll(routeResponse.getZoneList());
                    cityLists.addAll(routeResponse.getCityList());
                    regionLists.addAll(routeResponse.getRegionList());
                    outstandingBalance = routeResponse.getTotaloutstandingBalance();
                    todaySales = routeResponse.getTodaysales();
                    initView();
                }
            }
        }
        else {
            String networkStatus = NetworkUtil.getConnectivityStatusString(DashboardActivity.this);
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                dataSync();
            }
        }
    }

    private void dataSync(){
        ArrayList<OfflineData> offlineData = new ArrayList<>();
        offlineData = myDbHelper.getOfflineDatalist();
        if(offlineData.size() > 0){
            progressDialog = new ACProgressFlower.Builder(DashboardActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            progressDialog.show();
            if(offlineData.get(0).getType().equals("checkout") || offlineData.get(0).getType().equals("collection")) {
                new insertInvoice().execute(offlineData.get(0).getData());
            }
            else if(offlineData.get(0).getType().equals("stock")) {
                new InsertStockDetail().execute(offlineData.get(0).getData());
            }
            else if(offlineData.get(0).getType().equals("balance")) {
                new InsertStockBalance().execute(offlineData.get(0).getData());
            }
        }
        else {
            dashboardApi();
        }
    }

    private class InsertStockBalance extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ACProgressFlower.Builder(DashboardActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(DashboardActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            inputStr = params[0];
            Call<InsertStockDetails> call = apiService.InsertstockBalance(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<InsertStockDetails>() {
                @Override
                public void onResponse(Call<InsertStockDetails> call, Response<InsertStockDetails> response) {
                    if (response.isSuccessful()) {
                        Log.d("TAG", "onResponse: " + response);
                        InsertStockDetails verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getSuccess().getMessage().equals("Values Inserted Successfully")) {
                                ArrayList<OfflineData> offlineData = new ArrayList<>();
                                offlineData = myDbHelper.getOfflineDatalist();
                                myDbHelper.deleteDataFromOfflineTable(""+offlineData.get(0).getId());
                                offlineData = myDbHelper.getOfflineDatalist();
                                if(offlineData.size() > 0){
                                    if(offlineData.get(0).getType().equals("checkout") || offlineData.get(0).getType().equals("collection")) {
                                        new insertInvoice().execute(offlineData.get(0).getData());
                                    }
                                    else if(offlineData.get(0).getType().equals("stock")) {
                                        new InsertStockDetail().execute(offlineData.get(0).getData());
                                    }
                                    else if(offlineData.get(0).getType().equals("balance")) {
                                        new InsertStockBalance().execute(offlineData.get(0).getData());
                                    }
                                }
                                else {
                                    if(progressDialog !=null) {
                                        progressDialog.dismiss();
                                    }
                                    dashboardApi();
                                }
                            } else {
                                //                          status false case
                                String failureResponse = verifyMobileResponse.getSuccess().getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), DashboardActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(DashboardActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(DashboardActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<InsertStockDetails> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(DashboardActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(DashboardActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }


    private class InsertStockDetail extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ACProgressFlower.Builder(DashboardActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(DashboardActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            inputStr = params[0];

            Call<InsertStockDetails> call = apiService.GetInsertstock(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<InsertStockDetails>() {
                @Override
                public void onResponse(Call<InsertStockDetails> call, Response<InsertStockDetails> response) {
                    if (response.isSuccessful()) {
                        Log.d("TAG", "onResponse: " + response);
                        InsertStockDetails verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getSuccess().getMessage().equals("Values Inserted Successfully")) {
                                ArrayList<OfflineData> offlineData = new ArrayList<>();
                                offlineData = myDbHelper.getOfflineDatalist();
                                myDbHelper.deleteDataFromOfflineTable(""+offlineData.get(0).getId());
                                offlineData = myDbHelper.getOfflineDatalist();
                                if(offlineData.size() > 0){
                                    if(offlineData.get(0).getType().equals("checkout") || offlineData.get(0).getType().equals("collection")) {
                                        new insertInvoice().execute(offlineData.get(0).getData());
                                    }
                                    else if(offlineData.get(0).getType().equals("stock")) {
                                        new InsertStockDetail().execute(offlineData.get(0).getData());
                                    }
                                    else if(offlineData.get(0).getType().equals("balance")) {
                                        new InsertStockBalance().execute(offlineData.get(0).getData());
                                    }
                                }
                                else {
                                    if(progressDialog !=null) {
                                        progressDialog.dismiss();
                                    }
                                    dashboardApi();
                                }
                            } else {
                                //                          status false case
                                String failureResponse = verifyMobileResponse.getSuccess().getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), DashboardActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(DashboardActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(DashboardActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<InsertStockDetails> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(DashboardActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(DashboardActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private class insertInvoice extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(DashboardActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            inputStr = params[0];
            Call<InsertPromoterResponse> call = apiService.insertCollection(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<InsertPromoterResponse>() {
                @Override
                public void onResponse(Call<InsertPromoterResponse> call, Response<InsertPromoterResponse> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if (response.isSuccessful()) {
                        InsertPromoterResponse PlacebidResponce = response.body();
                        ArrayList<OfflineData> offlineData = new ArrayList<>();
                        offlineData = myDbHelper.getOfflineDatalist();
                        myDbHelper.deleteDataFromOfflineTable(""+offlineData.get(0).getId());
                        offlineData = myDbHelper.getOfflineDatalist();
                        if(offlineData.size() > 0){
                            if(offlineData.get(0).getType().equals("checkout") || offlineData.get(0).getType().equals("collection")) {
                                new insertInvoice().execute(offlineData.get(0).getData());
                            }
                        }
                        else {
                            if(progressDialog !=null) {
                                progressDialog.dismiss();
                            }
                            dashboardApi();
                        }
                    } else {
                        Toast.makeText(DashboardActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<InsertPromoterResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(DashboardActivity.this, R.string.connection_error, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(DashboardActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });
            return null;
        }
    }
}

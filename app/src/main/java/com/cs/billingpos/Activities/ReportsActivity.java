package com.cs.billingpos.Activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.billingpos.Adapters.DailySalesReportAdapter;
import com.cs.billingpos.Adapters.ReportAdapter;
import com.cs.billingpos.Models.BrandItems;
import com.cs.billingpos.Models.DailySalesReport;
import com.cs.billingpos.Models.DailyStockResponse;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.R;
import com.cs.billingpos.Rest.APIInterface;
import com.cs.billingpos.Rest.ApiClient;
import com.cs.billingpos.Utils.Constants;
import com.cs.billingpos.Utils.NetworkUtil;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportsActivity extends Activity {

    private ListView reportListView;
    ImageView back_btn;
    private ReportAdapter ReportAdapter;
    private List<DailyStockResponse.SalesReportsDetail> dailyStockLists = new ArrayList<>();
    private List<DailySalesReport.SalesReportsDetails> salesLists = new ArrayList<>();
    private LinearLayout categoryList;
    private ArrayList<String> BrandNames = new ArrayList<>();
    private ArrayAdapter<BrandItems> BrandAdapter;
    private ArrayAdapter<BrandItems.Items> BrandItemsAdapter;
    private int categorySelected = 0, itemSelected = 0;
    LinearLayout products;
    TextView addStock, addProduct;
    private String empId, skuid = "null", brandid = "null";
    Button daily_stock, daily_sales;
    RelativeLayout mdaily_stock_layout, mdaily_sales_layout;
    LinearLayout mstart_date_layout, mend_date_layout;
    TextView tvCheckInDate, tvCheckInMonth, tvCheckInWeek;
    TextView tvCheckOutDate, tvCheckOutMonth, tvCheckOutWeek;
    private int mCheckInYear = -1, mCheckInMonth, mCheckInDay, mCheckOutYear, mCheckOutMonth, mCheckOutDay;
    LinearLayout mstart_date_layoutStock, mend_date_layoutStock;
    TextView tvCheckInDateStock, tvCheckInMonthStock, tvCheckInWeekStock;
    TextView tvCheckOutDateStock, tvCheckOutMonthStock, tvCheckOutWeekStock;
    private int mCheckInYearStock = -1, mCheckInMonthStock, mCheckInDayStock, mCheckOutYearStock, mCheckOutMonthStock, mCheckOutDayStock;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final String[] WEEKS = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    Date mCheckInDate = null, mCheckOutDate = null;
    String mCheckInDateStr = null, mCheckOutDateStr = null;
    Date mCheckInDateStock = null, mCheckOutDateStock = null;
    String mCheckInDateStrStock = null, mCheckOutDateStrStock = null;
    boolean isCheckOutShown = false, isCheckOutShownStock = false, isCheckInSelected = false, isCheckInSelectedStock = false, isCalender_start = false, isCalender_end = false;
    LinearLayout mbottom_list;
    ListView daily_sales_list;
    Date startdate, enddate, startdateStock, enddateStock;
    String startDate, endDate, startDateStock, endDateStock;
    ImageView start_calender, end_calender, start_calenderStock, end_calenderStock;
    Button submitBtn, submitBtnStock;
    LinearLayout calendarLayoutStock;

    private SharedPreferences userPrefs;
    private SharedPreferences.Editor userPrefsEditor;
    private ReportAdapter mAdapter;
    private DailySalesReportAdapter mAdapterSales;
    Spinner mbrandlist, mItemsSpinner;
    EditText brand_edit, item_edit;
    boolean isStartDateSet = false, isEndDateSet = false, isBrandSelected = false, isItemSet = false, isItemSelected = false;
    boolean isStartDateSetStock = false;

    private ArrayList<RoutePlanDetailsResponse.BrandList> BrandLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.SKUlist> skUlists = new ArrayList<>();
    private ArrayList<BrandItems> menuItemsList = new ArrayList<>();
    ArrayList<BrandItems.Items> brandItemsList = new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.repots_daily);

        BrandLists = (ArrayList<RoutePlanDetailsResponse.BrandList>) getIntent().getSerializableExtra("brands");
        skUlists = (ArrayList<RoutePlanDetailsResponse.SKUlist>) getIntent().getSerializableExtra("sku");

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        empId = userPrefs.getString("EmpId", "2");

        back_btn = (ImageView) findViewById(R.id.back_btn);
        reportListView = (ListView) findViewById(R.id.product_List);
        categoryList = (LinearLayout) findViewById(R.id.categories);
        calendarLayoutStock = (LinearLayout) findViewById(R.id.calander_layout_stock);
        daily_stock = findViewById(R.id.daily_stock);
        daily_sales = findViewById(R.id.daily_sales);
        mdaily_stock_layout = findViewById(R.id.daily_stock_layout);
        mdaily_sales_layout = findViewById(R.id.daily_sale_layout);
        mstart_date_layout = findViewById(R.id.start_date_layout);
        mend_date_layout = findViewById(R.id.end_date_layout);
        tvCheckInDate = findViewById(R.id.start_date);
        tvCheckInWeek = findViewById(R.id.start_week);
        tvCheckInMonth = findViewById(R.id.start_month);
        tvCheckOutDate = findViewById(R.id.end_date);
        tvCheckOutWeek = findViewById(R.id.end_week);
        tvCheckOutMonth = findViewById(R.id.end_month);
        mbottom_list = findViewById(R.id.bottom_list_layout);
        daily_sales_list = findViewById(R.id.daily_sales_list);
        mbrandlist = findViewById(R.id.brandlist);
        mItemsSpinner = findViewById(R.id.skulist);
        brand_edit = findViewById(R.id.ap_brand);
        item_edit = findViewById(R.id.ap_sku);
        start_calender = findViewById(R.id.start_calender);
        end_calender = findViewById(R.id.end_calender);
        submitBtn = findViewById(R.id.submit);

        mstart_date_layoutStock = findViewById(R.id.start_date_layout_stock);
        mend_date_layoutStock = findViewById(R.id.end_date_layout_stock);
        tvCheckInDateStock = findViewById(R.id.start_date_stock);
        tvCheckInWeekStock = findViewById(R.id.start_week_stock);
        tvCheckInMonthStock = findViewById(R.id.start_month_stock);
        tvCheckOutDateStock = findViewById(R.id.end_date_stock);
        tvCheckOutWeekStock = findViewById(R.id.end_week_stock);
        tvCheckOutMonthStock = findViewById(R.id.end_month_stock);

        start_calenderStock = findViewById(R.id.start_calender_stock);
        end_calenderStock = findViewById(R.id.end_calender_stock);
        submitBtnStock = findViewById(R.id.submit_stock);

        BrandNames.clear();

//        dashboardApi();
        prepareMenuItems();
        setDates();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        daily_stock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                daily_stock.setBackground(getResources().getDrawable(R.drawable.seletedshape));
                daily_sales.setBackground(getResources().getDrawable(R.drawable.unseletedshape));

                mdaily_stock_layout.setVisibility(View.VISIBLE);
                mdaily_sales_layout.setVisibility(View.GONE);
                calendarLayoutStock.setVisibility(View.VISIBLE);
//                submitBtnStock.setVisibility(View.VISIBLE);

                daily_sales.setTextColor(getResources().getColor(R.color.billing_grey_text_color));
                daily_stock.setTextColor(getResources().getColor(R.color.white));
            }
        });

        daily_sales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                daily_stock.setBackground(getResources().getDrawable(R.drawable.unseletedshape));
                daily_sales.setBackground(getResources().getDrawable(R.drawable.seletedshape));

                mdaily_stock_layout.setVisibility(View.GONE);
                mdaily_sales_layout.setVisibility(View.VISIBLE);
                submitBtnStock.setVisibility(View.GONE);
                calendarLayoutStock.setVisibility(View.GONE);

                daily_stock.setTextColor(getResources().getColor(R.color.billing_grey_text_color));
                daily_sales.setTextColor(getResources().getColor(R.color.white));

//                checkInDatePicker();

//                for (RoutePlanDetailsResponse.BrandList brand : BrandLists) {
//                    BrandNames.add(brand.getBrandName());
//                    Log.i("TAG", "onResponse: " + brand.getBrandName());
//                }

                setSpinner();

            }
        });

        mstart_date_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInDatePicker();

//                start_calender.setBackground(getResources().getDrawable(R.drawable.circle_calender_selected));
//                end_calender.setBackground(getResources().getDrawable(R.drawable.circle_calender_selected));
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DailySalesApi();
            }
        });

        mend_date_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isCheckOutShown = false;
                if (isStartDateSet) {
                    checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth + 1));
//                    start_calender.setBackground(getResources().getDrawable(R.drawable.circle_calender_selected));
//                    end_calender.setBackground(getResources().getDrawable(R.drawable.circle_calender_selected));
                } else {
                    Toast.makeText(ReportsActivity.this, "Please select start date", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mstart_date_layoutStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInDatePickerStock();
            }
        });

        submitBtnStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DailyStockApi();
            }
        });

        mend_date_layoutStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isCheckOutShownStock = false;
                if (isStartDateSetStock) {
                    checkOutDatePickerStock(mCheckInYearStock, mCheckInDayStock, (mCheckInMonthStock + 1));
//                    start_calenderStock.setBackground(getResources().getDrawable(R.drawable.circle_calender_selected));
//                    end_calenderStock.setBackground(getResources().getDrawable(R.drawable.circle_calender_selected));
                } else {
                    Toast.makeText(ReportsActivity.this, "Please select start date", Toast.LENGTH_SHORT).show();
                }
            }
        });
        mbottom_list.setVisibility(View.GONE);
    }

    public void checkInDatePicker() {
        final Calendar c = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(ReportsActivity.this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        if (view.isShown()) {
                            isStartDateSet = true;
                            mCheckInYear = year;
                            mCheckInDay = dayOfMonth;
                            mCheckInMonth = monthOfYear;

                            if (mCheckInDay < 10) {
                                tvCheckInDate.setText("0" + mCheckInDay + " ");
                            } else {
                                tvCheckInDate.setText("" + mCheckInDay + " ");
                            }
                            tvCheckInMonth.setText(MONTHS[mCheckInMonth]);
                            isCheckOutShown = false;
                            isCheckInSelected = true;
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                            String sd = mCheckInYear + "-" + (mCheckInMonth + 1) + "-" + mCheckInDay;

                            try {
                                startdate = sdf.parse(sd);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            startDate = sdf.format(startdate);

                            checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth + 1));
                        }
                    }
                }, mCheckInYear, mCheckInMonth, mCheckInDay);

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        datePickerDialog.setCancelable(false);

        datePickerDialog.setMessage("Select Start Date");

        datePickerDialog.show();
    }

    public void checkOutDatePicker(final int year, final int date, final int month) {
        final Calendar c = Calendar.getInstance();
        long selectedMillis = c.getTimeInMillis();
//        final Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, 1);
        String givenDateString = year + "-" + (month) + "-" + date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            mCheckInDate = sdf.parse(givenDateString);
            mCheckInDateStr = sdf.format(mCheckInDate);
            selectedMillis = mCheckInDate.getTime();
            c.setTime(mCheckInDate); // yourdate is an object of type Date
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            tvCheckInWeek.setText(WEEKS[(dayOfWeek - 1)] + ", ");
        } catch (Exception e) {
            e.printStackTrace();
        }

//        c.add(Calendar.DATE, 1);
        selectedMillis = c.getTimeInMillis();
        mCheckOutYear = c.get(Calendar.YEAR);
        mCheckOutMonth = c.get(Calendar.MONTH);
        mCheckOutDay = c.get(Calendar.DAY_OF_MONTH);

        final DatePickerDialog datePickerDialog = new DatePickerDialog(ReportsActivity.this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        isEndDateSet = true;

                        mCheckOutYear = year;
                        mCheckOutDay = dayOfMonth;
                        mCheckOutMonth = monthOfYear;

                        if (mCheckOutDay < 10) {
                            tvCheckOutDate.setText("0" + mCheckOutDay + " ");
                        } else {
                            tvCheckOutDate.setText("" + mCheckOutDay + " ");
                        }

                        tvCheckOutMonth.setText(MONTHS[mCheckOutMonth]);
                        isCheckInSelected = false;


                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                        String ed = mCheckOutYear + "-" + (mCheckOutMonth + 1) + "-" + mCheckOutDay;

                        try {
                            enddate = sdf.parse(ed);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        endDate = sdf.format(enddate);
//                        if(isStartDateSet && isEndDateSet && isBrandSelected && isItemSet) {
//                            DailySalesApi();
//                        }
                        submitBtn.setVisibility(View.VISIBLE);
                        daily_sales_list.setVisibility(View.GONE);
                        String givenDateString = mCheckOutYear + "-" + (mCheckOutMonth + 1) + "-" + mCheckOutDay;

                        try {
                            mCheckOutDate = sdf.parse(givenDateString);
                            mCheckOutDateStr = sdf.format(mCheckOutDate);
                            c.setTime(mCheckOutDate); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckOutWeek.setText(WEEKS[(dayOfWeek - 1)] + ", ");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }, mCheckOutYear, mCheckOutMonth, mCheckOutDay);
        datePickerDialog.getDatePicker().setMinDate(selectedMillis);
//        long max = TimeUnit.DAYS.toMillis(90);
//        datePickerDialog.getDatePicker().setMaxDate(selectedMillis + max);
        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    // Do Stuff
                    if (isCheckInSelected) {
                        String givenDateString = year + "-" + (month) + "-" + date;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            mCheckInDate = sdf.parse(givenDateString);
                            mCheckInDateStr = sdf.format(mCheckInDate);
//                            selectedMillis = mCheckInDate.getTime();
                            c.setTime(mCheckInDate); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckInWeek.setText(WEEKS[(dayOfWeek - 1)] + ", ");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        c.add(Calendar.DATE, 1);
//                        selectedMillis = c.getTimeInMillis();
                        mCheckOutYear = c.get(Calendar.YEAR);
                        mCheckOutMonth = c.get(Calendar.MONTH);
                        mCheckOutDay = c.get(Calendar.DAY_OF_MONTH);

                        if (mCheckOutDay < 10) {
                            tvCheckOutDate.setText("0" + mCheckOutDay + " ");
                        } else {
                            tvCheckOutDate.setText("" + mCheckOutDay + " ");
                        }

                        tvCheckOutMonth.setText(MONTHS[mCheckOutMonth]);
                        isCheckInSelected = false;


                        String ed = mCheckOutYear + "-" + (mCheckOutMonth + 1) + "-" + mCheckOutDay;

                        try {
                            enddate = sdf.parse(ed);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        endDate = sdf.format(enddate);

                        String givenDateString1 = mCheckOutYear + "-" + (mCheckOutMonth + 1) + "-" + mCheckOutDay;
                        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            mCheckOutDate = sdf1.parse(givenDateString1);
                            mCheckOutDateStr = sdf1.format(mCheckOutDate);
                            c.setTime(mCheckOutDate); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckOutWeek.setText(WEEKS[(dayOfWeek - 1)] + ", ");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    datePickerDialog.dismiss();
//                    DailySalesApi();
                }
            }
        });
        datePickerDialog.setInverseBackgroundForced(true);
        datePickerDialog.setMessage("Select End Date");
        if (!isCheckOutShown) {
            datePickerDialog.show();
            isCheckOutShown = true;
        }
    }

    public void checkInDatePickerStock() {
        final Calendar c = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(ReportsActivity.this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        if (view.isShown()) {
                            isStartDateSetStock = true;
                            mCheckInYearStock = year;
                            mCheckInDayStock = dayOfMonth;
                            mCheckInMonthStock = monthOfYear;

                            if (mCheckInDayStock < 10) {
                                tvCheckInDateStock.setText("0" + mCheckInDayStock + " ");
                            } else {
                                tvCheckInDateStock.setText("" + mCheckInDayStock + " ");
                            }
                            tvCheckInMonthStock.setText(MONTHS[mCheckInMonthStock]);
                            isCheckOutShownStock = false;
                            isCheckInSelectedStock = true;
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                            String sd = mCheckInYearStock + "-" + (mCheckInMonthStock + 1) + "-" + mCheckInDayStock;

                            try {
                                startdateStock = sdf.parse(sd);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            startDateStock = sdf.format(startdateStock);

                            checkOutDatePickerStock(mCheckInYearStock, mCheckInDayStock, (mCheckInMonthStock + 1));
                        }
                    }
                }, mCheckInYearStock, mCheckInMonthStock, mCheckInDayStock);

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        datePickerDialog.setCancelable(false);

        datePickerDialog.setMessage("Select Start Date");

        datePickerDialog.show();
    }

    public void checkOutDatePickerStock(final int year, final int date, final int month) {
        final Calendar c = Calendar.getInstance();
        long selectedMillis = c.getTimeInMillis();
//        final Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, 1);
        String givenDateString = year + "-" + (month) + "-" + date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            mCheckInDateStock = sdf.parse(givenDateString);
            mCheckInDateStrStock = sdf.format(mCheckInDateStock);
            selectedMillis = mCheckInDateStock.getTime();
            c.setTime(mCheckInDateStock); // yourdate is an object of type Date
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            tvCheckInWeekStock.setText(WEEKS[(dayOfWeek - 1)] + ", ");
        } catch (Exception e) {
            e.printStackTrace();
        }

//        c.add(Calendar.DATE, 1);
        selectedMillis = c.getTimeInMillis();
        mCheckOutYearStock = c.get(Calendar.YEAR);
        mCheckOutMonthStock = c.get(Calendar.MONTH);
        mCheckOutDayStock = c.get(Calendar.DAY_OF_MONTH);

        final DatePickerDialog datePickerDialog = new DatePickerDialog(ReportsActivity.this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        isEndDateSet = true;

                        mCheckOutYearStock = year;
                        mCheckOutDayStock = dayOfMonth;
                        mCheckOutMonthStock = monthOfYear;

                        if (mCheckOutDayStock < 10) {
                            tvCheckOutDateStock.setText("0" + mCheckOutDayStock + " ");
                        } else {
                            tvCheckOutDateStock.setText("" + mCheckOutDay + " ");
                        }

                        tvCheckOutMonthStock.setText(MONTHS[mCheckOutMonthStock]);
                        isCheckInSelectedStock = false;

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                        String ed = mCheckOutYearStock + "-" + (mCheckOutMonthStock + 1) + "-" + mCheckOutDayStock;

                        try {
                            enddateStock = sdf.parse(ed);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        endDateStock = sdf.format(enddateStock);
//                        if(isStartDateSet && isEndDateSet && isBrandSelected && isItemSet) {
//                            DailySalesApi();
//                        }
                        submitBtnStock.setVisibility(View.VISIBLE);
                        mdaily_stock_layout.setVisibility(View.GONE);
                        String givenDateString = mCheckOutYearStock + "-" + (mCheckOutMonthStock + 1) + "-" + mCheckOutDayStock;

                        try {
                            mCheckOutDateStock = sdf.parse(givenDateString);
                            mCheckOutDateStrStock = sdf.format(mCheckOutDateStock);
                            c.setTime(mCheckOutDateStock); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckOutWeekStock.setText(WEEKS[(dayOfWeek - 1)] + ", ");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }, mCheckOutYearStock, mCheckOutMonthStock, mCheckOutDayStock);
        datePickerDialog.getDatePicker().setMinDate(selectedMillis);
//        long max = TimeUnit.DAYS.toMillis(90);
//        datePickerDialog.getDatePicker().setMaxDate(selectedMillis + max);
        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    // Do Stuff
                    if (isCheckInSelectedStock) {
                        String givenDateString = year + "-" + (month) + "-" + date;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            mCheckInDateStock = sdf.parse(givenDateString);
                            mCheckInDateStrStock = sdf.format(mCheckInDateStock);
//                            selectedMillis = mCheckInDate.getTime();
                            c.setTime(mCheckInDateStock); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckInWeekStock.setText(WEEKS[(dayOfWeek - 1)] + ", ");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        c.add(Calendar.DATE, 1);
//                        selectedMillis = c.getTimeInMillis();
                        mCheckOutYearStock = c.get(Calendar.YEAR);
                        mCheckOutMonthStock = c.get(Calendar.MONTH);
                        mCheckOutDayStock = c.get(Calendar.DAY_OF_MONTH);

                        if (mCheckOutDayStock < 10) {
                            tvCheckOutDateStock.setText("0" + mCheckOutDay + " ");
                        } else {
                            tvCheckOutDateStock.setText("" + mCheckOutDay + " ");
                        }

                        tvCheckOutMonthStock.setText(MONTHS[mCheckOutMonthStock]);
                        isCheckInSelectedStock = false;


                        String ed = mCheckOutYearStock + "-" + (mCheckOutMonthStock + 1) + "-" + mCheckOutDayStock;

                        try {
                            enddateStock = sdf.parse(ed);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        endDateStock = sdf.format(enddateStock);

                        String givenDateString1 = mCheckOutYearStock + "-" + (mCheckOutMonthStock + 1) + "-" + mCheckOutDayStock;
                        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            mCheckOutDateStock = sdf1.parse(givenDateString1);
                            mCheckOutDateStrStock = sdf1.format(mCheckOutDateStock);
                            c.setTime(mCheckOutDate); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckOutWeekStock.setText(WEEKS[(dayOfWeek - 1)] + ", ");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    datePickerDialog.dismiss();
//                    DailySalesApi();
//                    DailyStockApi();
                }
            }
        });
        datePickerDialog.setInverseBackgroundForced(true);
        datePickerDialog.setMessage("Select End Date");
        if (!isCheckOutShownStock) {
            datePickerDialog.show();
            isCheckOutShownStock = true;
        }
    }

    private String getTodayDate() {
        String dateStr = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        Calendar calendar = Calendar.getInstance();
        dateStr = sdf.format(calendar.getTime());

        return dateStr;
    }

    private void DailySalesApi() {
        final ACProgressFlower dialog = new ACProgressFlower.Builder(ReportsActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        ;
        dialog.show();

        final String networkStatus = NetworkUtil.getConnectivityStatusString(ReportsActivity.this);

        String bId = "null", skuId = "null";
        if (isBrandSelected) {
            if (isItemSelected && itemSelected != 0) {
                skuId = "" + menuItemsList.get(categorySelected).getItems().get(itemSelected).getSKUId();
            }
            if(categorySelected != 0) {
                bId = "" + menuItemsList.get(categorySelected).getBrandId();
            }
        }
        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<DailySalesReport> call = apiService.dailysalesreport(empId, startDate, endDate, skuId,
                bId);
        Log.i("TAG", "dates: " + startDate + " " + endDate + " " + empId + " " + skuId + " " + bId);
        call.enqueue(new Callback<DailySalesReport>() {

            public void onResponse(Call<DailySalesReport> call, Response<DailySalesReport> response) {
                if (response.isSuccessful()) {
                    Log.d("TAG", "onResponse: " + response);
                    DailySalesReport routeResponse = response.body();
                    try {
                        //status true case

                        salesLists = routeResponse.getSalesReportsDetails();
                        mAdapterSales = new DailySalesReportAdapter(ReportsActivity.this, salesLists);
                        daily_sales_list.setAdapter(mAdapterSales);
                        if (salesLists.size() > 0) {
                            mbottom_list.setVisibility(View.VISIBLE);
                            submitBtn.setVisibility(View.GONE);
                            daily_sales_list.setVisibility(View.VISIBLE);
                        } else {
                            Constants.showOneButtonAlertDialog("No records found", getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), ReportsActivity.this);
                        }
                        Log.i("TAG", "onResponse: " + salesLists);

//                        for (RoutePlanDetailsResponse.CategoryList brand : BrandLists){
//                            BrandNames.add(brand.getSKUCategoryName());
//                        }

//                        categoryAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner_guests, categoryNames) {
//                            public View getView(int position, View convertView, ViewGroup parent) {
//                                View v = super.getView(position, convertView, parent);
//
//                                ((TextView) v).setTextSize(1);
//                                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));
//
//                                return v;
//                            }
//
//                            public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                                View v = super.getDropDownView(position, convertView, parent);
//                                v.setBackgroundResource(R.color.white);
//                                ((TextView) v).setTextSize(15);
//                                ((TextView) v).setGravity(Gravity.LEFT);
//                                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
//
//                                return v;
//                            }
//                        };

//                        categoryspinnerlist.setAdapter(categoryAdapter);

                    } catch (Exception e) {
                        Toast.makeText(ReportsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null)
                        dialog.dismiss();
                } else {
                    try {
                        Log.d("TAG", "onResponse: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (dialog != null)
                        dialog.dismiss();

                    Toast.makeText(ReportsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
            }

            public void onFailure(Call<DailySalesReport> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    Toast.makeText(ReportsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ReportsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }

    private void setSpinner() {
        BrandAdapter = new ArrayAdapter<BrandItems>(getApplicationContext(), R.layout.list_spinner_guests, menuItemsList) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setText(menuItemsList.get(position).getBrandName());
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.LEFT);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        brandItemsList.clear();
        if(categorySelected != 0) {
            BrandItems.Items items = new BrandItems.Items();
            items.setSKUName("-- Select SKU --");
            items.setSKUId(0);
            items.setCartonBalance(0);
            items.setPackBalance(0);
            items.setCartonPrice(0);
            items.setPackPrice(0);
            items.setPackImage("");
            brandItemsList.add(items);
        }
        brandItemsList.addAll(menuItemsList.get(categorySelected).getItems());
        BrandItemsAdapter = new ArrayAdapter<BrandItems.Items>(getApplicationContext(), R.layout.list_spinner_guests, brandItemsList) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setText(brandItemsList.get(position).getSKUName());
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.LEFT);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        mbrandlist.setAdapter(BrandAdapter);
        mItemsSpinner.setAdapter(BrandItemsAdapter);

        mbrandlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                categorySelected = i;
                brand_edit.setText(menuItemsList.get(i).getBrandName());
                brandItemsList.clear();
                if(categorySelected != 0) {
                    BrandItems.Items items = new BrandItems.Items();
                    items.setSKUName("-- Select SKU --");
                    items.setSKUId(0);
                    items.setCartonBalance(0);
                    items.setPackBalance(0);
                    items.setCartonPrice(0);
                    items.setPackPrice(0);
                    items.setPackImage("");
                    brandItemsList.add(items);
                }
                brandItemsList.addAll(menuItemsList.get(categorySelected).getItems());
//                            mItemsSpinner.setAdapter(BrandItemsAdapter);
                BrandItemsAdapter.notifyDataSetChanged();
                isBrandSelected = true;
//                if(isStartDateSet && isEndDateSet && isBrandSelected && isItemSet) {
//                    DailySalesApi();
//                }
                submitBtn.setVisibility(View.VISIBLE);
                daily_sales_list.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        mItemsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                itemSelected = i;
                item_edit.setText(menuItemsList.get(categorySelected).getItems().get(i).getSKUName());
                isItemSet = true;
                isItemSelected = true;
//                if(isStartDateSet && isEndDateSet && isBrandSelected && isItemSet) {
//                    DailySalesApi();
//                }
                submitBtn.setVisibility(View.VISIBLE);
                daily_sales_list.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


//        mItemsSpinner.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                mbrandlist.setAdapter(BrandAdapter);
//                mItemsSpinner.setAdapter(BrandItemsAdapter);
//                return false;
//            }
//        });


    }

    private void prepareMenuItems() {
        BrandNames.clear();
        menuItemsList.clear();
        BrandItems brandLists = new BrandItems();
        brandLists.setBrandName("-- Select Brand --");
        brandLists.setBrandId(0);
        ArrayList<BrandItems.Items> list = new ArrayList<>();
        BrandItems.Items items11 = new BrandItems.Items();
        items11.setSKUName("-- Select SKU --");
        items11.setSKUId(0);
        items11.setCartonBalance(0);
        items11.setPackBalance(0);
        items11.setCartonPrice(0);
        items11.setPackPrice(0);
        items11.setPackImage("");
        list.add(items11);
        brandLists.setItems(list);
        menuItemsList.add(brandLists);
        for (int i = 0; i < BrandLists.size(); i++) {
            boolean isCategoryAdded = false;
            for (int c = 0; c < BrandNames.size(); c++) {
                if (BrandNames.get(c).equals(BrandLists.get(i).getBrandName())) {
                    isCategoryAdded = true;
                }
            }
            if (!isCategoryAdded) {
                BrandNames.add(BrandLists.get(i).getBrandName());

                BrandItems menuItems = new BrandItems();
                menuItems.setBrandId(BrandLists.get(i).getBrandId());
                menuItems.setBrandName(BrandLists.get(i).getBrandName());
                ArrayList<BrandItems.Items> itemsList = new ArrayList<>();
                for (int k = 0; k < skUlists.size(); k++) {
                    if (BrandLists.get(i).getBrandId() == (skUlists.get(k).getBrandId())) {
                        BrandItems.Items items = new BrandItems.Items();
                        items.setSKUId(skUlists.get(k).getSKUId());
                        items.setSKUName(skUlists.get(k).getSKUName());
                        items.setPackImage(skUlists.get(k).getPackImage());
                        items.setPackBalance(skUlists.get(k).getPackBalance());
                        items.setCartonBalance(skUlists.get(k).getCartonBalance());
                        items.setPackPrice(skUlists.get(k).getPackPrice());
                        items.setCartonPrice(skUlists.get(k).getCartonPrice());
                        itemsList.add(items);
                    }
                }
                menuItems.setItems(itemsList);
                menuItemsList.add(menuItems);
            }
        }
    }

    private void DailyStockApi() {
        final ACProgressFlower dialog = new ACProgressFlower.Builder(ReportsActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        dialog.show();

        final String networkStatus = NetworkUtil.getConnectivityStatusString(ReportsActivity.this);

        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<DailyStockResponse> call = apiService.dailystockreport(empId, startDateStock, endDateStock);
        call.enqueue(new Callback<DailyStockResponse>() {

            public void onResponse(Call<DailyStockResponse> call, Response<DailyStockResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("TAG", "onResponse: " + response);
                    DailyStockResponse routeResponse = response.body();
                    try {
                        //status true case

                        dailyStockLists = routeResponse.getSalesReportsDetails();
                        mAdapter = new ReportAdapter(ReportsActivity.this, dailyStockLists);
                        reportListView.setAdapter(mAdapter);
                        if (dailyStockLists.size() == 0) {
                            submitBtnStock.setVisibility(View.VISIBLE);
                            mdaily_stock_layout.setVisibility(View.GONE);
                            Constants.showOneButtonAlertDialog("No records found", getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), ReportsActivity.this);
                        }
                        else {
                            submitBtnStock.setVisibility(View.GONE);
                            mdaily_stock_layout.setVisibility(View.VISIBLE);
                        }

                    } catch (Exception e) {
                        Toast.makeText(ReportsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null)
                        dialog.dismiss();
                } else {
                    if (dialog != null)
                        dialog.dismiss();

                    Toast.makeText(ReportsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
            }

            public void onFailure(Call<DailyStockResponse> call, Throwable t) {
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    Toast.makeText(ReportsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ReportsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }

    private void setDates(){
        final Calendar c = Calendar.getInstance();
        mCheckInYear = c.get(Calendar.YEAR);
        mCheckInMonth = c.get(Calendar.MONTH);
        mCheckInDay = c.get(Calendar.DAY_OF_MONTH);
        mCheckInYearStock = c.get(Calendar.YEAR);
        mCheckInMonthStock = c.get(Calendar.MONTH);
        mCheckInDayStock = c.get(Calendar.DAY_OF_MONTH);

        if ((c.get(Calendar.DATE)) < 10) {
            tvCheckInDate.setText("0" + c.get(Calendar.DATE) + " ");
            tvCheckInDateStock.setText("0" + c.get(Calendar.DATE) + " ");
        } else {
            tvCheckInDate.setText("" + c.get(Calendar.DATE) + " ");
            tvCheckInDateStock.setText("" + c.get(Calendar.DATE) + " ");
        }
        tvCheckInMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
        tvCheckInMonthStock.setText(MONTHS[c.get(Calendar.MONTH)]);
        tvCheckInWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK) - 1)] + ", ");
        tvCheckInWeekStock.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK) - 1)] + ", ");

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MMM-dd", Locale.US);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        String sd = c.get(Calendar.YEAR) + "-" + MONTHS[c.get(Calendar.MONTH)] + "-" + tvCheckInDate.getText().toString();

        try {
            startdate = sdf1.parse(sd);
            startdateStock = sdf1.parse(sd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        startDate = sdf.format(startdate);
        startDateStock = sdf.format(startdate);

        mCheckOutYear = c.get(Calendar.YEAR);
        mCheckOutMonth = c.get(Calendar.MONTH);
        mCheckOutDay = c.get(Calendar.DAY_OF_MONTH);
        mCheckOutYearStock = c.get(Calendar.YEAR);
        mCheckOutMonthStock = c.get(Calendar.MONTH);
        mCheckOutDayStock = c.get(Calendar.DAY_OF_MONTH);

        if ((c.get(Calendar.DATE)) < 10) {
            tvCheckOutDate.setText("0" + c.get(Calendar.DATE) + " ");
            tvCheckOutDateStock.setText("0" + c.get(Calendar.DATE) + " ");
        } else {
            tvCheckOutDate.setText("" + c.get(Calendar.DATE) + " ");
            tvCheckOutDateStock.setText("" + c.get(Calendar.DATE) + " ");
        }
        tvCheckOutMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
        tvCheckOutMonthStock.setText(MONTHS[c.get(Calendar.MONTH)]);
        tvCheckOutWeekStock.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK) - 1)] + ", ");

        String ed = c.get(Calendar.YEAR) + "-" + MONTHS[c.get(Calendar.MONTH)] + "-" + tvCheckOutDate.getText().toString();

        try {
            enddate = sdf1.parse(ed);
            enddateStock = sdf1.parse(ed);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        endDate = sdf.format(enddate);
        endDateStock = sdf.format(enddate);

//        start_calender.setBackground(getResources().getDrawable(R.drawable.circle_calender_selected));
//        end_calender.setBackground(getResources().getDrawable(R.drawable.circle_calender_selected));
//        DailyStockApi();
    }
}

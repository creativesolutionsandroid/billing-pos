package com.cs.billingpos.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.billingpos.Models.CustomerDetails;
import com.cs.billingpos.Models.InsertProductDetails;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.R;
import com.cs.billingpos.Rest.APIInterface;
import com.cs.billingpos.Rest.ApiClient;
import com.cs.billingpos.Utils.Constants;
import com.cs.billingpos.Utils.GPSTracker;
import com.cs.billingpos.Utils.NetworkUtil;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



/**
 * Created by Puli on 23-04-2019.
 */

public class AddCustomerActivity extends Activity {

    Spinner marketspiner, regionspiner, areaspiner, cityspiner, zonespiner;
    EditText street_name, market_ed, region_ed, area_ed, city_ed, zone_ed, tempory_ed, outletname_ed, district_ed, latitude_ed, longitude_ed, mobile_ed;
    String strunic, stroutletname, strdistrict, strlatitude, strlongitude, strmobile, strStreetName;
    Button savebtn, mgps;
    private String TAG = "TAG";

    private ArrayList<RoutePlanDetailsResponse.MarketList> marketLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.RegionList> regionLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.AreaList> areaLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.CityList> cityLists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.ZoneList> zoneLists = new ArrayList<>();

    ArrayAdapter<CustomerDetails> marketAdapte;
    private ArrayAdapter<CustomerDetails.RegionlistCD> regionspinner;
    private ArrayAdapter<CustomerDetails.AreaListCD> areaspinner;
    private ArrayAdapter<CustomerDetails.CityListCD> cityspinner;
    private ArrayAdapter<CustomerDetails.ZoneListCD> zonespinner;
    private ArrayList<CustomerDetails> marketspinner = new ArrayList<>();
    private ArrayList<CustomerDetails.RegionlistCD> regionsList = new ArrayList<>();
    private ArrayList<CustomerDetails.AreaListCD> areaList = new ArrayList<>();
    private ArrayList<CustomerDetails.CityListCD> cityList = new ArrayList<>();
    private ArrayList<CustomerDetails.ZoneListCD> zoneList = new ArrayList<>();
    int market = 0, region = 0, area = 0, city = 0, zone = 0;
    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 3;
    private FusedLocationProviderClient mFusedLocationClient;
    private double latitude, longitude;
    ImageView backBtn;
    private SharedPreferences userPrefs;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_customer_activity);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        marketspiner = (Spinner) findViewById(R.id.ad_marketlist);
        regionspiner = (Spinner) findViewById(R.id.ad_Regionlist);
        areaspiner = (Spinner) findViewById(R.id.arealist);
        cityspiner = (Spinner) findViewById(R.id.citylist);
        zonespiner = (Spinner) findViewById(R.id.zonelist);

        backBtn = (ImageView) findViewById(R.id.back_btn);
        market_ed = (EditText) findViewById(R.id.ad_market);
        region_ed = (EditText) findViewById(R.id.region_ad);
        area_ed = (EditText) findViewById(R.id.ad_areaet);
        city_ed = (EditText) findViewById(R.id.ad_city);
        zone_ed = (EditText) findViewById(R.id.ad_zoneet);
        tempory_ed = (EditText) findViewById(R.id.ad_uniccode);
        outletname_ed = (EditText) findViewById(R.id.outletname);
        district_ed = (EditText) findViewById(R.id.districtnme);
        latitude_ed = (EditText) findViewById(R.id.ad_latitude);
        longitude_ed = (EditText) findViewById(R.id.ad_longitude);
        mobile_ed = (EditText) findViewById(R.id.ad_mobile);
        street_name = (EditText) findViewById(R.id.streetname);
        savebtn = (Button) findViewById(R.id.save_btn);
        mgps = (Button) findViewById(R.id.gps);

        marketLists = (ArrayList<RoutePlanDetailsResponse.MarketList>) getIntent().getSerializableExtra("market");
        regionLists = (ArrayList<RoutePlanDetailsResponse.RegionList>) getIntent().getSerializableExtra("region");
        areaLists = (ArrayList<RoutePlanDetailsResponse.AreaList>) getIntent().getSerializableExtra("area");
        cityLists = (ArrayList<RoutePlanDetailsResponse.CityList>) getIntent().getSerializableExtra("city");
        zoneLists = (ArrayList<RoutePlanDetailsResponse.ZoneList>) getIntent().getSerializableExtra("zone");

        arrangeData();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(AddCustomerActivity.this);
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                gps = new GPSTracker(AddCustomerActivity.this);
                try {
                    getGPSCoordinates();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            gps = new GPSTracker(AddCustomerActivity.this);
            try {
                getGPSCoordinates();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mgps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                latitude_ed.setText("" + latitude);
                longitude_ed.setText("" + longitude);
            }
        });

        savebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(AddCustomerActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            new AddCustomer().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void getGPSCoordinates() throws IOException {

        if(gps != null){
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                try {

                    mFusedLocationClient.getLastLocation().addOnFailureListener(AddCustomerActivity.this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.i("TAG","fused lat null");
                        }
                    })
                            .addOnSuccessListener(AddCustomerActivity.this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {
                                        // Logic to handle location object
                                        latitude = location.getLatitude();
                                        longitude = location.getLongitude();
                                        Log.i("TAG","fused lat "+location.getLatitude());
                                        Log.i("TAG","fused long "+location.getLongitude());
                                    }
                                    else{
                                        Log.i("TAG","fused lat null");
                                    }
                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(AddCustomerActivity.this);
                    try {
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(AddCustomerActivity.this, "Location permission denied, Unable to save location", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(AddCustomerActivity.this, perm));
    }

    private class AddCustomer extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            dialog = new ACProgressFlower.Builder(AddCustomerActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(AddCustomerActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<InsertProductDetails> call = apiService.GetCustomerdetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<InsertProductDetails>() {
                @Override
                public void onResponse(Call<InsertProductDetails> call, Response<InsertProductDetails> response) {
                    if (response.isSuccessful()) {
                        Log.d(TAG, "onResponse: "+response);
                        InsertProductDetails verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getSuccess().getMessage().equals("Values Inserted Successfully")) {

                                finish();
//                                data = verifyMobileResponse.getData().get(0).getWr();
//                                if(data.size() == 0){
//                                    Constants.showOneButtonAlertDialog("No history found", getResources().getString(R.string.app_name),
//                                            getResources().getString(R.string.ok), getActivity());
//                                }
//                                mAdapter = new FeedbackAdapter(getActivity(), data, "En");
//                                mListView.setAdapter(mAdapter);
//                                setData();
//
//                                try {
//                                    name.setText(verifyMobileResponse.getData().get(0).getNameEn());
//                                    Glide.with(FeedbackFragment.this)
//                                            .load(Constants.WORKSHOP_IMAGES+verifyMobileResponse.getData().get(0).getLogo())
//                                            .into(profilePic);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
                            } else {
                                //                          status false case
                                String failureResponse = verifyMobileResponse.getSuccess().getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), AddCustomerActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(AddCustomerActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(AddCustomerActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<InsertProductDetails> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(AddCustomerActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddCustomerActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson() {
        JSONObject parentObj = new JSONObject();
        try {
            JSONObject addproduct = new JSONObject();

            addproduct.put("pid", 0);
            addproduct.put("TrackingNo", tempory_ed.getText().toString());
            addproduct.put("OutletName", outletname_ed.getText().toString());
            addproduct.put("DistrictName", district_ed.getText().toString());
            addproduct.put("MID", marketspinner.get(market).getMarket_id());
            addproduct.put("RID", marketspinner.get(market).getRegions().get(region).getRID());
            addproduct.put("AID", marketspinner.get(market).getRegions().get(region).getAreaListCDS().get(area).getAID());
            addproduct.put("CityID", marketspinner.get(market).getRegions().get(region).getAreaListCDS().get(area).getCityListCDS().get(city).getCID());
            addproduct.put("ZoneID", marketspinner.get(market).getRegions().get(region).getAreaListCDS().get(area).getCityListCDS().get(city).getZoneListCDS().get(zone).getZID());
            addproduct.put("Latitude", latitude);
            addproduct.put("Longitude", longitude);
            addproduct.put("MobileNo", mobile_ed.getText().toString());
            addproduct.put("MobileNo", mobile_ed.getText().toString());
            addproduct.put("StreatName", strStreetName);
            addproduct.put("CreatedBy", userPrefs.getString("EmpId", ""));

            parentObj.put("AddSCustomerDetails", addproduct);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }

    private void arrangeData(){
        marketspinner.clear();
        ArrayList<Integer> dummyMarketList = new ArrayList<>();
        for (int i = 0; i < marketLists.size(); i++) {
            if (!dummyMarketList.contains(regionLists.get(i).getMID())) {
                dummyMarketList.add(regionLists.get(i).getMID());
                CustomerDetails brand = new CustomerDetails();
                brand.setMarket_id(String.valueOf(marketLists.get(i).getMID()));
                brand.setMarket_name(marketLists.get(i).getMarket1());
                ArrayList<Integer> dummyCityList = new ArrayList<>();
                ArrayList<CustomerDetails.RegionlistCD> regionlistCDS = new ArrayList<>();

                for (int j = 0; j < regionLists.size(); j++) {
                    if (!dummyCityList.contains(regionLists.get(j).getRID())) {
                        if (regionLists.get(j).getMID() == (marketLists.get(i).getMID())) {
                            dummyCityList.add(regionLists.get(j).getRID());
                            CustomerDetails.RegionlistCD regionlistCD = new CustomerDetails.RegionlistCD();
                            regionlistCD.setRegion1(regionLists.get(j).getRegion1());
                            regionlistCD.setMID(String.valueOf(regionLists.get(j).getMID()));
                            regionlistCD.setRID(String.valueOf(regionLists.get(j).getRID()));

                            ArrayList<Integer> dummyarealist = new ArrayList<>();

                            ArrayList<CustomerDetails.AreaListCD> areaListCDS = new ArrayList<>();
                            for (int k = 0; k < areaLists.size(); k++) {
                                if (!dummyarealist.contains(areaLists.get(k).getAID())) {
                                    if (marketLists.get(i).getMID() == (regionLists.get(j).getMID())) {
                                        if (areaLists.get(k).getRID() == (regionLists.get(j).getRID())) {
                                            dummyarealist.add(areaLists.get(k).getAID());
                                            CustomerDetails.AreaListCD branch = new CustomerDetails.AreaListCD();

                                            branch.setAID(String.valueOf(areaLists.get(k).getAID()));
                                            branch.setMID(String.valueOf(areaLists.get(k).getMID()));
                                            branch.setRID(String.valueOf(areaLists.get(k).getRID()));
                                            branch.setArea1(String.valueOf(areaLists.get(k).getArea1()));

                                            ArrayList<Integer> dummyCityList1 = new ArrayList<>();
                                            ArrayList<CustomerDetails.CityListCD> cityListCDS = new ArrayList<>();

                                            for (int l = 0; l < cityLists.size(); l++) {
                                                if (!dummyCityList1.contains(cityLists.get(l).getCID()) &&
                                                        marketLists.get(i).getMID() == cityLists.get(l).getMID() &&
                                                        areaLists.get(k).getRID() == cityLists.get(l).getRID() &&
                                                        cityLists.get(l).getAID() == areaLists.get(k).getAID()) {
                                                    dummyCityList1.add(cityLists.get(l).getCID());
                                                    CustomerDetails.CityListCD cityListCD = new CustomerDetails.CityListCD();
                                                    cityListCD.setCity1(cityLists.get(l).getCity1());
                                                    cityListCD.setMID(String.valueOf(cityLists.get(l).getMID()));
                                                    cityListCD.setRID(String.valueOf(cityLists.get(l).getRID()));
                                                    cityListCD.setAID(String.valueOf(cityLists.get(l).getAID()));
                                                    cityListCD.setCID(String.valueOf(cityLists.get(l).getCID()));

                                                    ArrayList<CustomerDetails.ZoneListCD> zoneListCDS = new ArrayList<>();
                                                    for (int m = 0; m < zoneLists.size(); m++) {
                                                        if (marketLists.get(i).getMID() == zoneLists.get(m).getMID() &&
                                                                regionLists.get(j).getRID() == zoneLists.get(m).getRID() &&
                                                                areaLists.get(k).getAID() == zoneLists.get(m).getAID() &&
                                                                cityLists.get(l).getCID() == zoneLists.get(m).getCID()) {

                                                            CustomerDetails.ZoneListCD zoneListCD = new CustomerDetails.ZoneListCD();

                                                            zoneListCD.setAID(String.valueOf(zoneLists.get(m).getAID()));
                                                            zoneListCD.setMID(String.valueOf(zoneLists.get(m).getMID()));
                                                            zoneListCD.setRID(String.valueOf(zoneLists.get(m).getRID()));
                                                            zoneListCD.setCID(String.valueOf(zoneLists.get(m).getCID()));
                                                            zoneListCD.setZID(String.valueOf(zoneLists.get(m).getZID()));
                                                            zoneListCD.setZone1(String.valueOf(zoneLists.get(m).getZone1()));

                                                            zoneListCDS.add(zoneListCD);
                                                        }
                                                    }
                                                    cityListCD.setZoneListCDS(zoneListCDS);
                                                    cityListCDS.add(cityListCD);
                                                }
                                            }
                                            branch.setCityListCDS(cityListCDS);
                                            areaListCDS.add(branch);
                                        }
                                    }
                                }
                            }
                            regionlistCD.setAreaListCDS(areaListCDS);
                            regionlistCDS.add(regionlistCD);
                        }
                    }
                }
                brand.setRegions(regionlistCDS);
                marketspinner.add(brand);
            }
        }
        setAdapters(true);
    }

    public void setAdapters(boolean brandRefresh) {
        regionsList.addAll(marketspinner.get(market).getRegions());
        areaList.addAll(marketspinner.get(market).getRegions().get(region).getAreaListCDS());
        cityList.addAll(marketspinner.get(market).getRegions().get(region).getAreaListCDS().get(area).getCityListCDS());
        zoneList.addAll(marketspinner.get(market).getRegions().get(region).getAreaListCDS().get(area).getCityListCDS().get(city).getZoneListCDS());

            marketAdapte = new ArrayAdapter<CustomerDetails>(getApplicationContext(), R.layout.list_spinner_guests, marketspinner) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    ((TextView) v).setTextSize(1);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                    return v;
                }

                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    v.setBackgroundResource(R.color.white);
                    ((TextView) v).setText(marketspinner.get(position).getMarket_name());
                    ((TextView) v).setTextSize(15);
                    ((TextView) v).setGravity(Gravity.LEFT);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                    return v;
                }
            };

            regionspinner = new ArrayAdapter<CustomerDetails.RegionlistCD>(getApplicationContext(), R.layout.list_spinner_guests, regionsList) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    ((TextView) v).setTextSize(1);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                    return v;
                }

                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    v.setBackgroundResource(R.color.white);
                    ((TextView) v).setText(regionsList.get(position).getRegion1());
                    ((TextView) v).setTextSize(15);
                    ((TextView) v).setGravity(Gravity.LEFT);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                    return v;
                }
            };

            areaspinner = new ArrayAdapter<CustomerDetails.AreaListCD>(getApplicationContext(), R.layout.list_spinner_guests, areaList) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    ((TextView) v).setTextSize(1);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                    return v;
                }

                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    v.setBackgroundResource(R.color.white);
                    ((TextView) v).setText(areaList.get(position).getArea1());
                    ((TextView) v).setTextSize(15);
                    ((TextView) v).setGravity(Gravity.LEFT);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                    return v;
                }
            };

            cityspinner = new ArrayAdapter<CustomerDetails.CityListCD>(getApplicationContext(), R.layout.list_spinner_guests, cityList) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    ((TextView) v).setTextSize(1);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                    return v;
                }

                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    v.setBackgroundResource(R.color.white);
                    ((TextView) v).setText(cityList.get(position).getCity1());
                    ((TextView) v).setTextSize(15);
                    ((TextView) v).setGravity(Gravity.LEFT);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                    return v;
                }
            };

            zonespinner = new ArrayAdapter<CustomerDetails.ZoneListCD>(getApplicationContext(), R.layout.list_spinner_guests, zoneList) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    ((TextView) v).setTextSize(1);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                    return v;
                }

                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    v.setBackgroundResource(R.color.white);
                    ((TextView) v).setText(zoneList.get(position).getZone1());
                    ((TextView) v).setTextSize(15);
                    ((TextView) v).setGravity(Gravity.LEFT);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                    return v;
                }
            };

            marketspiner.setAdapter(marketAdapte);
            regionspiner.setAdapter(regionspinner);
            areaspiner.setAdapter(areaspinner);
            cityspiner.setAdapter(cityspinner);
            zonespiner.setAdapter(zonespinner);

            marketspiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    market = i;
                    market_ed.setText(marketspinner.get(i).getMarket_name());

                    regionsList.clear();
                    areaList.clear();
                    cityList.clear();
                    zoneList.clear();

                    regionsList.addAll(marketspinner.get(market).getRegions());
                    areaList.addAll(marketspinner.get(market).getRegions().get(region).getAreaListCDS());
                    cityList.addAll(marketspinner.get(market).getRegions().get(region).getAreaListCDS().get(area).getCityListCDS());
                    zoneList.addAll(marketspinner.get(market).getRegions().get(region).getAreaListCDS().get(area).getCityListCDS().get(city).getZoneListCDS());

                    regionspiner.setAdapter(regionspinner);
                    areaspiner.setAdapter(areaspinner);
                    cityspiner.setAdapter(cityspinner);
                    zonespiner.setAdapter(zonespinner);

                    regionspinner.notifyDataSetChanged();
                    areaspinner.notifyDataSetChanged();
                    cityspinner.notifyDataSetChanged();
                    zonespinner.notifyDataSetChanged();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            regionspiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    region = i;
                    region_ed.setText(marketspinner.get(market).getRegions().get(i).getRegion1());

                    areaList.clear();
                    cityList.clear();
                    zoneList.clear();

                    areaList.addAll(marketspinner.get(market).getRegions().get(region).getAreaListCDS());
                    cityList.addAll(marketspinner.get(market).getRegions().get(region).getAreaListCDS().get(area).getCityListCDS());
                    zoneList.addAll(marketspinner.get(market).getRegions().get(region).getAreaListCDS().get(area).getCityListCDS().get(city).getZoneListCDS());

                    areaspiner.setAdapter(areaspinner);
                    cityspiner.setAdapter(cityspinner);
                    zonespiner.setAdapter(zonespinner);

                    areaspinner.notifyDataSetChanged();
                    cityspinner.notifyDataSetChanged();
                    zonespinner.notifyDataSetChanged();

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            areaspiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    area = i;
                    area_ed.setText(marketspinner.get(market).getRegions().get(region).getAreaListCDS().get(i).getArea1());

                    cityList.clear();
                    zoneList.clear();
                    cityList.addAll(marketspinner.get(market).getRegions().get(region).getAreaListCDS().get(area).getCityListCDS());
                    zoneList.addAll(marketspinner.get(market).getRegions().get(region).getAreaListCDS().get(area).getCityListCDS().get(city).getZoneListCDS());
                    cityspiner.setAdapter(cityspinner);
                    zonespiner.setAdapter(zonespinner);
                    cityspinner.notifyDataSetChanged();
                    zonespinner.notifyDataSetChanged();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


            cityspiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    city = i;
                    city_ed.setText(marketspinner.get(market).getRegions().get(region).getAreaListCDS().get(area).getCityListCDS().get(i).getCity1());

                    zoneList.clear();
                    zoneList.addAll(marketspinner.get(market).getRegions().get(region).getAreaListCDS().get(area).getCityListCDS().get(city).getZoneListCDS());
                    zonespiner.setAdapter(zonespinner);
                    zonespinner.notifyDataSetChanged();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            zonespiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    zone = i;
                    zone_ed.setText(marketspinner.get(market).getRegions().get(region).getAreaListCDS().get(area).getCityListCDS().get(city).getZoneListCDS().get(i).getZone1());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
    }


    private boolean validations() {
        strunic = tempory_ed.getText().toString().trim();
        stroutletname = outletname_ed.getText().toString().trim();
        strdistrict = district_ed.getText().toString().trim();
        strlatitude = latitude_ed.getText().toString().trim();
        strlongitude = longitude_ed.getText().toString().trim();
        strmobile = mobile_ed.getText().toString().trim();
        strStreetName = street_name.getText().toString();

        if (strunic.length() == 0) {
            tempory_ed.setError(getResources().getString(R.string.customer_unic__code));
            tempory_ed.setFocusable(true);
            return false;
        }
        else if (stroutletname.length() == 0) {
            outletname_ed.setFocusable(true);
            outletname_ed.setError(getResources().getString(R.string.customer_outlet_name));
            return false;
        }
        else if (strdistrict.length() == 0) {
            district_ed.setFocusable(true);

            district_ed.setError(getResources().getString(R.string.customer_district_enter_district));
            return false;
        }
        else if (strStreetName.length() == 0) {
            street_name.setFocusable(true);
            street_name.setError(getResources().getString(R.string.enter_street_name));
            return false;
        }
        else if (strlatitude.length() == 0) {
            latitude_ed.setFocusable(true);

            latitude_ed.setError(getResources().getString(R.string.customer_latitude));
            return false;
        }
        else if (strlongitude.length() == 0) {
            longitude_ed.setFocusable(true);

            longitude_ed.setError(getResources().getString(R.string.customer_longitude));
            return false;
        }
        else if (strmobile.length() == 0) {
            mobile_ed.setFocusable(true);

            mobile_ed.setError(getResources().getString(R.string.customer_mobile));
            return false;
        }else {

            return true;
        }
    }

}

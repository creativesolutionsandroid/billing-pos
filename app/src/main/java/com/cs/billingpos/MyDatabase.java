package com.cs.billingpos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.cs.billingpos.Models.OfflineData;
import com.cs.billingpos.Models.Order;

import java.util.ArrayList;
import java.util.HashMap;

public class MyDatabase extends SQLiteOpenHelper {

    private static final int DB_VERSION = 2;

    public MyDatabase(Context applicationcontext) {
        super(applicationcontext, "billingPos.db", null, DB_VERSION);
    }

    private static final String CREATE_ORDER_TABLE =
            "CREATE TABLE \"OrderTable\" (\"orderId\" INTEGER PRIMARY KEY  AUTOINCREMENT ,\"TypeId\" INTEGER ,\"SKUId\" INTEGER,\"Carton\" INTEGER, \"Pack\" INTEGER,\"FreeCarton\" INTEGER,\"FreePack\"INTEGER,\"TotalCarton\" INTEGER,\"TotalPack\" INTEGER,\"CartonPrice\" TEXT DEFAULT 0,\"PackPrice\" TEXT DEFAULT 0,\"TotalCartonPrice\" TEXT DEFAULT 0,\"TotalPackPrice\" TEXT DEFAULT 0,\"TotalPrice\" TEXT DEFAULT 0,\"itemName\" TEXT, \"packPerCarton\" INTEGER, \"subCatId\" TEXT, \"invoiceId\" TEXT, \"invoiceNumber\" TEXT, \"outstandingId\" TEXT, \"outletName\" TEXT)";

    private static final String CREATE_SYNC_TABLE =
            "CREATE TABLE \"tblOfflineData\" (\"id\" INTEGER PRIMARY KEY  AUTOINCREMENT ,\"Data\" VARCHAR,\"Type\" VARCHAR, \"Status\" INTEGER)";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ORDER_TABLE);
        db.execSQL(CREATE_SYNC_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query;
        query = "DROP TABLE IF EXISTS OrderTable";
        db.execSQL(query);
        onCreate(db);
    }

    public void insertOfflineData(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("Data", queryValues.get("Data"));
        values.put("Type", queryValues.get("Type"));
        values.put("Status", queryValues.get("Status"));

        database.insert("tblOfflineData", null, values);
        database.close();
    }

    public ArrayList<OfflineData> getOfflineDatalist(){
        ArrayList<OfflineData> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  tblOfflineData";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                OfflineData sc = new OfflineData();
                sc.setId(cursor.getInt(0));
                sc.setData(cursor.getString(1));
                sc.setType(cursor.getString(2));
                sc.setStatus(cursor.getInt(3));

                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }

    public void deleteDataFromOfflineTable(String id){
        SQLiteDatabase database = this.getWritableDatabase();
        String updateQuery = "DELETE FROM tblOfflineData where id = "+id;
        database.execSQL(updateQuery);
    }

    public void insertOrder(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("TypeId", queryValues.get("TypeId"));
        values.put("SKUId", queryValues.get("SKUId"));
        values.put("Carton", queryValues.get("Carton"));
        values.put("Pack", queryValues.get("Pack"));
        values.put("FreeCarton", queryValues.get("FreeCarton"));
        values.put("FreePack", queryValues.get("FreePack"));
        values.put("TotalCarton", queryValues.get("TotalCarton"));
        values.put("TotalPack", queryValues.get("TotalPack"));
        values.put("CartonPrice", queryValues.get("CartonPrice"));
        values.put("PackPrice", queryValues.get("PackPrice"));
        values.put("TotalCartonPrice", queryValues.get("TotalCartonPrice"));
        values.put("TotalPackPrice", queryValues.get("TotalPackPrice"));
        values.put("TotalPrice", queryValues.get("TotalPrice"));
        values.put("itemName", queryValues.get("itemName"));
        values.put("packPerCarton", queryValues.get("packPerCarton"));
        values.put("subCatId", queryValues.get("subCatId"));
        values.put("invoiceId", queryValues.get("invoiceId"));
        values.put("invoiceNumber", queryValues.get("invoiceNumber"));
        values.put("outstandingId", queryValues.get("outstandingId"));
        values.put("outletName", queryValues.get("outletName"));

        database.insert("OrderTable", null, values);
        database.close();
    }

    public void updateOrder(String Carton, String Pack, String FreeCarton, String FreePack, String TotalCarton,String TotalPack,
                            String TotalCartonPrice, String TotalPackPrice, String TotalPrice, String CartonPrice, String PackPrice, String orderId){
        SQLiteDatabase database = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put("qty", qty);
//        values.put("price", price);
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET Carton ="+Carton+ ",Pack ="+ Pack+ ",FreeCarton ="+ FreeCarton
                + ",FreePack ="+ FreePack+ ",TotalCarton ="+ TotalCarton+ ",TotalPack ="+ TotalPack+",TotalCartonPrice ="+ TotalCartonPrice+
                ",TotalPackPrice ="+ TotalPackPrice+ ",TotalPrice ="+ TotalPrice+",CartonPrice ="+ CartonPrice+",PackPrice ="+ PackPrice+" where orderId ="+orderId;
        database.execSQL(updateQuery);
    }

    public ArrayList<Order> getOrderInfo(){
        ArrayList<Order> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Order sc = new Order();
                sc.setOrderId(cursor.getString(0));
                sc.setTypeId(cursor.getInt(1));
                sc.setSKUId(cursor.getInt(2));
                sc.setCarton(cursor.getInt(3));
                sc.setPack(cursor.getInt(4));
                sc.setFreeCarton(cursor.getInt(5));
                sc.setFreePack(cursor.getInt(6));
                sc.setTotalCarton(cursor.getInt(7));
                sc.setTotalPack(cursor.getInt(8));
                sc.setCartonPrice(cursor.getFloat(9));
                sc.setPackPrice(cursor.getFloat(10));
                sc.setTotalCartonPrice(cursor.getFloat(11));
                sc.setTotalPackPrice(cursor.getFloat(12));
                sc.setTotalPrice(cursor.getFloat(13));
                sc.setItemName(cursor.getString(14));
                sc.setPackPerCarton(cursor.getInt(15));
                sc.setSubCatId(cursor.getString(16));
                sc.setInvoiceId(cursor.getString(17));
                sc.setInvoiceNumber(cursor.getString(18));
                sc.setOutstandingId(cursor.getString(19));
                sc.setOutletName(cursor.getString(20));

                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }

    public String getItemName(String itemId, String typeId){
        String cnt = "";
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT itemName FROM  OrderTable where SKUId="+itemId+" and TypeId="+typeId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getString(0);
        }
        c.close();
        return cnt;
    }

    public String getOrderId(String itemId, String typeId){
        String cnt = "";
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT orderId FROM  OrderTable where SKUId="+itemId+" and TypeId="+typeId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getString(0);
        }
        c.close();
        return cnt;
    }

    public int getCartonCount(String itemId, String typeId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(Carton) FROM  OrderTable where SKUId="+itemId+" and TypeId="+typeId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        c.close();
        return cnt;
    }

    public int getPackCount(String itemId, String typeId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(Pack) FROM  OrderTable where SKUId="+itemId+" and TypeId="+typeId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        c.close();
        return cnt;
    }

    public int getFreeCartonCount(String itemId, String typeId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(FreeCarton) FROM  OrderTable where SKUId="+itemId+" and TypeId="+typeId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        c.close();
        return cnt;
    }

    public int getFreePackCount(String itemId, String typeId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(FreePack) FROM  OrderTable where SKUId="+itemId+" and TypeId="+typeId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        c.close();
        return cnt;
    }

    public int getTotalCartonCount(String itemId, String typeId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(TotalCarton) FROM  OrderTable where SKUId="+itemId+" and TypeId="+typeId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        c.close();
        return cnt;
    }

    public int getTotalPackCount(String itemId, String typeId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(TotalPack) FROM  OrderTable where SKUId="+itemId+" and TypeId="+typeId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        c.close();
        return cnt;
    }

    public void deleteItemFromOrder(String orderId){
        SQLiteDatabase database = this.getWritableDatabase();
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable where orderId = "+orderId;
        database.execSQL(updateQuery);
    }

    public float getTotalOrderPrice(){
        float qty = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cur = database.rawQuery("SELECT SUM(TotalPrice) FROM OrderTable", null);
        if(cur.moveToFirst())
        {
            qty  = cur.getFloat(0);
        }

        return qty;
    }

    public void deleteOrderTable(){
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable";
        database.execSQL(updateQuery);
    }
}

package com.cs.billingpos.Adapters;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.billingpos.R;

import java.util.ArrayList;


/**
 * Created by Puli on 24-04-2019.
 */

public class BluetoothDevicesAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    private ArrayList<BluetoothDevice> devicesList = new ArrayList<>();

    public BluetoothDevicesAdapter(Context context, ArrayList<BluetoothDevice> devicesList) {
        this.context = context;
        this.devicesList = devicesList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return devicesList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public static class ViewHolder {
        TextView name, macAddress;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_bluetooth_devices, null);

            holder.name= (TextView) convertView.findViewById(R.id.device_name);
            holder.macAddress= (TextView) convertView.findViewById(R.id.device_address);

            holder.name.setText(devicesList.get(position).getName());
            holder.macAddress.setText(devicesList.get(position).getAddress());

        return convertView;
    }
}

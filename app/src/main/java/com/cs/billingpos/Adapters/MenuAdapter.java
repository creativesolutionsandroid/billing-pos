package com.cs.billingpos.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.billingpos.Models.MenuItems;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.MyDatabase;
import com.cs.billingpos.R;
import com.cs.billingpos.Utils.Constants;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

public class MenuAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    private ArrayList<MenuItems.SubCategory> menuList = new ArrayList<>();
    int cartonsAdded = 0, freeCartonsAdded = 0, packsAdded = 0, freepacksAdded = 0, packsUsed = 0;
    float cartonTotalPrice = 0, packTotalPrice = 0, netAmount = 0;
    float cartonPriceIndividual, packPriceIndividual;
    private MyDatabase myDbHelper;
    int catId;
    private SharedPreferences userPrefs;
    private SharedPreferences.Editor userPrefsEditor;
    private String outletName;


    public MenuAdapter(Context context, ArrayList<MenuItems.SubCategory> orderList, int catId, String outletName) {
        this.context = context;
        this.menuList = orderList;
        this.catId = catId;
        this.outletName = outletName;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.myDbHelper = new MyDatabase(context);
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
    }

    public int getCount() {
        return menuList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView subCategoryName;
        LinearLayout itemsLayout;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.menu_subcategory_list, null);

            holder.subCategoryName = (TextView) convertView.findViewById(R.id.sub_category_name);
            holder.itemsLayout = (LinearLayout) convertView.findViewById(R.id.items_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.subCategoryName.setText(""+ menuList.get(position).getCompanyName());

        holder.itemsLayout.removeAllViews();
        for (int i = 0; i < menuList.get(position).getItems().size(); i++) {
//            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.menu_item_list, null);
            TextView ItemName = v.findViewById(R.id.item_name);
            TextView ItemCount = v.findViewById(R.id.item_count);
            ImageView ItemImage = v.findViewById(R.id.item_image);
            RelativeLayout layout = v.findViewById(R.id.layout);

            try {
                if(menuList.get(position).getItems().get(i).getPackImage() != null) {
                    Glide.with(context).load(Constants.IMAGE_URL + menuList.get(position).getItems().get(i).getPackImage())
                            .into(ItemImage);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            ItemName.setText(menuList.get(position).getItems().get(i).getSKUName());

            if(myDbHelper.getItemName(""+menuList.get(position).getItems().get(i).getSKUId(), ""+catId)
                    .equalsIgnoreCase(menuList.get(position).getItems().get(i).getSKUName())) {
                int count = myDbHelper.getTotalCartonCount("" + menuList.get(position).getItems().get(i).getSKUId(), ""+catId);
                count = count * menuList.get(position).getItems().get(i).getPacksPerCarton();
                count = count + myDbHelper.getTotalPackCount("" + menuList.get(position).getItems().get(i).getSKUId(), ""+catId);
                if (count > 0) {
                    ItemCount.setText("" + count);
                    ItemCount.setVisibility(View.VISIBLE);
                }
            }

            final int finalI = i;
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMenuDialog(menuList.get(position).getItems(), finalI, menuList.get(position).getCompanyName());
                }
            });
            holder.itemsLayout.addView(v);
        }

        return convertView;
    }

    private void showMenuDialog(final ArrayList<MenuItems.Items> items, final int pos, String subCatId){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.menu_item_alert_dialog;

        final View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//        Button dialogbox1 =(Button) dialogView.findViewById(R.id.invoice_btn);
        Button addBtn =(Button) dialogView.findViewById(R.id.add_btn);
//
        ImageView itemImage = (ImageView) dialogView.findViewById(R.id.item_image);
        TextView cancel = (TextView) dialogView.findViewById(R.id.close_popup);
        TextView itemName = (TextView) dialogView.findViewById(R.id.item_name);
        final TextView cartonQty = (TextView) dialogView.findViewById(R.id.cartons_qty);
        final TextView packQty = (TextView) dialogView.findViewById(R.id.pack_qty);
        final TextView cartonPriceAddded = (TextView) dialogView.findViewById(R.id.carton_total_price);
        final TextView packPriceAddded = (TextView) dialogView.findViewById(R.id.pack_total_price);
        final TextView freeCartonQty = (TextView) dialogView.findViewById(R.id.free_cartons_qty);
        final TextView freePackQty = (TextView) dialogView.findViewById(R.id.free_pack_qty);
        final TextView netAmounttv = (TextView) dialogView.findViewById(R.id.total_amount);
        final EditText cartonPrice = (EditText) dialogView.findViewById(R.id.carton_price);
        EditText packPrice = (EditText) dialogView.findViewById(R.id.pack_price);

        TextView cartonMinus = (TextView) dialogView.findViewById(R.id.cartons_minus);
        TextView freeCartonMinus = (TextView) dialogView.findViewById(R.id.free_cartons_minus);
        TextView cartonPlus = (TextView) dialogView.findViewById(R.id.cartons_plus);
        TextView freeCartonPlus = (TextView) dialogView.findViewById(R.id.free_cartons_plus);
        TextView packMinus = (TextView) dialogView.findViewById(R.id.pack_minus);
        TextView freepackMinus = (TextView) dialogView.findViewById(R.id.free_pack_minus);
        TextView packPlus = (TextView) dialogView.findViewById(R.id.pack_plus);
        TextView freepackPlus = (TextView) dialogView.findViewById(R.id.free_pack_plus);

        itemName.setText(items.get(pos).getSKUName());
        cartonPrice.setText(""+items.get(pos).getCartonPrice());
        packPrice.setText(""+items.get(pos).getPackPrice());

        try {
            if(items.get(pos).getPackImage() != null) {
                Glide.with(context).load(Constants.IMAGE_URL + items.get(pos).getPackImage())
                        .into(itemImage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        final int cartonsBalance = items.get(pos).getCartonBalance();
        final int packBalance = items.get(pos).getPackBalance();
        final int packsPerCarton = items.get(pos).getPacksPerCarton();
        final int balance = (cartonsBalance * packsPerCarton) + packBalance;
        cartonPriceIndividual = items.get(pos).getCartonPrice();
        packPriceIndividual = items.get(pos).getPackPrice();

        cartonsAdded = myDbHelper.getCartonCount(""+items.get(pos).getSKUId(), ""+catId);
        packsUsed = cartonsAdded * packsPerCarton;
        cartonQty.setText(""+cartonsAdded);

        freeCartonsAdded = myDbHelper.getFreeCartonCount(""+items.get(pos).getSKUId(), ""+catId);
        packsUsed = packsUsed + (freeCartonsAdded * packsPerCarton);
        freeCartonQty.setText(""+freeCartonsAdded);

        packsAdded = myDbHelper.getPackCount(""+items.get(pos).getSKUId(), ""+catId);
        packsUsed = packsUsed + packsAdded;
        packQty.setText(""+packsAdded);

        freepacksAdded = myDbHelper.getFreePackCount(""+items.get(pos).getSKUId(), ""+catId);
        packsUsed = packsUsed + freepacksAdded;
        freePackQty.setText(""+freepacksAdded);

        cartonTotalPrice = (float) cartonPriceIndividual * cartonsAdded;
        cartonPriceAddded.setText(Constants.priceFormat.format(cartonTotalPrice));
        packTotalPrice = (float) packPriceIndividual * packsAdded;
        packPriceAddded.setText(Constants.priceFormat.format(packTotalPrice));
        netAmount = cartonTotalPrice + packTotalPrice;
        netAmounttv.setText(Constants.priceFormat.format(netAmount));

        cartonPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                if(str.length() > 0) {
                    cartonPriceIndividual = Float.parseFloat(str.toString());
                    cartonTotalPrice = (float) cartonPriceIndividual * cartonsAdded;
                    cartonPriceAddded.setText(Constants.priceFormat.format(cartonTotalPrice));
                    netAmount = cartonTotalPrice + packTotalPrice;
                    netAmounttv.setText(Constants.priceFormat.format(netAmount));
                }
                else {
                    packPriceIndividual = 0;
                    packTotalPrice = (float) packPriceIndividual * packsAdded;
                    packPriceAddded.setText(Constants.priceFormat.format(packTotalPrice));
                    netAmount = cartonTotalPrice + packTotalPrice;
                    netAmounttv.setText(Constants.priceFormat.format(netAmount));
                }
            }
        });

        packPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                if(str.length() > 0) {
                    packPriceIndividual = Float.parseFloat(str.toString());
                    packTotalPrice = (float) packPriceIndividual * packsAdded;
                    packPriceAddded.setText(Constants.priceFormat.format(packTotalPrice));
                    netAmount = cartonTotalPrice + packTotalPrice;
                    netAmounttv.setText(Constants.priceFormat.format(netAmount));
                }
                else {
                    packPriceIndividual = 0;
                    packTotalPrice = (float) packPriceIndividual * packsAdded;
                    packPriceAddded.setText(Constants.priceFormat.format(packTotalPrice));
                    netAmount = cartonTotalPrice + packTotalPrice;
                    netAmounttv.setText(Constants.priceFormat.format(netAmount));
                }
            }
        });

        cartonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((packsUsed + packsPerCarton) <= balance) {
                    cartonsAdded = cartonsAdded + 1;
                    packsUsed = packsUsed + packsPerCarton;
                    cartonQty.setText(""+cartonsAdded);
                    cartonTotalPrice = (float) cartonPriceIndividual * cartonsAdded;
                    cartonPriceAddded.setText(Constants.priceFormat.format(cartonTotalPrice));
                    netAmount = cartonTotalPrice + packTotalPrice;
                    netAmounttv.setText(Constants.priceFormat.format(netAmount));
                }
                else {
                    Toast.makeText(context, "No stock", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cartonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cartonsAdded != 0) {
                    cartonsAdded = cartonsAdded - 1;
                    packsUsed = packsUsed - packsPerCarton;
                    cartonQty.setText(""+cartonsAdded);
                    cartonTotalPrice = (float) cartonPriceIndividual * cartonsAdded;
                    cartonPriceAddded.setText(Constants.priceFormat.format(cartonTotalPrice));
                    netAmount = cartonTotalPrice + packTotalPrice;
                    netAmounttv.setText(Constants.priceFormat.format(netAmount));
                }
            }
        });

        freeCartonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((packsUsed + packsPerCarton) <= balance) {
                    freeCartonsAdded = freeCartonsAdded + 1;
                    packsUsed = packsUsed + packsPerCarton;
                    freeCartonQty.setText(""+freeCartonsAdded);
                }
                else {
                    Toast.makeText(context, "No stock", Toast.LENGTH_SHORT).show();
                }
            }
        });

        freeCartonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(freeCartonsAdded != 0) {
                    freeCartonsAdded = freeCartonsAdded- 1;
                    packsUsed = packsUsed - packsPerCarton;
                    freeCartonQty.setText(""+freeCartonsAdded);
                }
            }
        });

        packPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((packsUsed + 1) <= balance) {
                    packsAdded = packsAdded + 1;
                    packsUsed = packsUsed + 1;
                    packQty.setText(""+packsAdded);
                    packTotalPrice = (float) packPriceIndividual * packsAdded;
                    packPriceAddded.setText(""+packTotalPrice);
                    netAmount = cartonTotalPrice + packTotalPrice;
                    netAmounttv.setText(""+netAmount);
                }
                else {
                    Toast.makeText(context, "No stock", Toast.LENGTH_SHORT).show();
                }
            }
        });

        packMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(packsAdded != 0) {
                    packsAdded = packsAdded - 1;
                    packsUsed = packsUsed - 1;
                    packQty.setText(""+packsAdded);
                    packTotalPrice = (float) packPriceIndividual * packsAdded;
                    packPriceAddded.setText(Constants.priceFormat.format(packTotalPrice));
                    netAmount = cartonTotalPrice + packTotalPrice;
                    netAmounttv.setText(Constants.priceFormat.format(netAmount));
                }
            }
        });

        freepackPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((packsUsed + 1) <= balance) {
                    freepacksAdded = freepacksAdded + 1;
                    packsUsed = packsUsed + 1;
                    freePackQty.setText(""+freepacksAdded);
                }
                else {
                    Toast.makeText(context, "No stock", Toast.LENGTH_SHORT).show();
                }
            }
        });

        freepackMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(freepacksAdded != 0) {
                    freepacksAdded = freepacksAdded - 1;
                    packsUsed = packsUsed - 1;
                    freePackQty.setText(""+freepacksAdded);
                }
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (packsUsed > 0) {
                    int count = 0;
                    if (myDbHelper.getItemName("" + items.get(pos).getSKUId(), "" + catId)
                            .equalsIgnoreCase(items.get(pos).getSKUName())) {
                        count = myDbHelper.getTotalCartonCount("" + items.get(pos).getSKUId(), "" + catId);
                        count = count + myDbHelper.getTotalPackCount("" + items.get(pos).getSKUId(), "" + catId);
                    }

                    if (count > 0) {
                        myDbHelper.updateOrder("" + cartonsAdded, "" + packsAdded, "" + freeCartonsAdded, "" + freepacksAdded, "" + (cartonsAdded + freeCartonsAdded),
                                "" + (packsAdded + freepacksAdded), "" + cartonTotalPrice, "" + packTotalPrice, "" + netAmount, "" + cartonPriceIndividual, "" + packPriceIndividual,
                                myDbHelper.getOrderId("" + items.get(pos).getSKUId(), "" + catId));
                    } else {
                        HashMap<String, String> values = new HashMap<>();

                        values.put("TypeId", "" + catId);
                        values.put("SKUId", "" + items.get(pos).getsKUId());
                        values.put("Carton", "" + cartonsAdded);
                        values.put("Pack", "" + packsAdded);
                        values.put("FreeCarton", "" + freeCartonsAdded);
                        values.put("FreePack", "" + freepacksAdded);
                        values.put("TotalCarton", "" + (cartonsAdded + freeCartonsAdded));
                        values.put("TotalPack", "" + (packsAdded + freepacksAdded));
                        values.put("CartonPrice", "" + cartonPriceIndividual);
                        values.put("PackPrice", "" + packPriceIndividual);
                        values.put("TotalCartonPrice", "" + cartonTotalPrice);
                        values.put("TotalPackPrice", "" + packTotalPrice);
                        values.put("TotalPrice", "" + netAmount);
                        values.put("itemName", items.get(pos).getSKUName());
                        values.put("packPerCarton", "" + items.get(pos).getPacksPerCarton());
                        values.put("subCatId", "" + subCatId);
                        values.put("invoiceId", "0");
                        values.put("invoiceNumber", "");
                        values.put("outstandingId", "0");
                        values.put("outletName", outletName);
                        myDbHelper.insertOrder(values);
                    }


                    Intent intent = new Intent("Session");
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

                    Gson json = new Gson();
                    RoutePlanDetailsResponse routeResponse = json.fromJson(userPrefs.getString("json", ""), RoutePlanDetailsResponse.class);
                    routeResponse.getSKUlist().get(pos).setCartonPrice(cartonPriceIndividual);
                    routeResponse.getSKUlist().get(pos).setPackPrice(packPriceIndividual);
                    Gson gson = new Gson();
                    String json1 = gson.toJson(routeResponse);
                    userPrefsEditor.putString("json", json1);
                    userPrefsEditor.commit();

                    items.get(pos).setCartonPrice(cartonPriceIndividual);
                    items.get(pos).setPackPrice(packPriceIndividual);
                    finalCustomDialog.dismiss();
                }
                else {
                    Toast.makeText(context, "Please add items", Toast.LENGTH_SHORT).show();
                }
//                int cartonsUsed = (int) (packsUsed/packsPerCarton);
//                int packsUsed = 0;
//                if(cartonsUsed > cartonsBalance) {
//                    cartonsUsed = cartonsBalance;
//                    int p = cartonsUsed * packsPerCarton;
//                    packsUsed = packsUsed - p;
//                }
//                else {
//                    cartonsUsed = cartonsBalance - cartonsUsed;
//                    packsUsed = (int) (packsUsed % packsPerCarton);
//                }
//                items.get(pos).setCartonBalance(cartonsUsed);
//                items.get(pos).setPackBalance(packsUsed);


            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        DisplayMetrics display = context.getResources().getDisplayMetrics();
//        Point size = new Point();
//        display.w(size);
        int screenWidth = display.widthPixels;

        double d = screenWidth*0.95;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}

package com.cs.billingpos.Adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.cs.billingpos.Models.Items;
import com.cs.billingpos.R;

import java.util.ArrayList;


/**
 * Created by Puli on 23-04-2019.
 */

public class AddStockAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    public static ArrayList<Items> productuList = new ArrayList<>();


    public AddStockAdapter(Context context, ArrayList<Items> orderList) {
        this.context = context;
        this.productuList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return productuList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView sno, productname;
        EditText carton, pack;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        holder = new ViewHolder();
        convertView = inflater.inflate(R.layout.list_addproduct, null);

        holder.sno = (TextView) convertView.findViewById(R.id.s_number);
        holder.productname = (TextView) convertView.findViewById(R.id.product);
        holder.carton = (EditText) convertView.findViewById(R.id.carton);
        holder.pack = (EditText) convertView.findViewById(R.id.pack);

        holder.sno.setText("" + (position + 1));
        holder.productname.setText("" + productuList.get(position).getSKUName());
//        holder.carton.setText("" + productuList.get(position).getCartonBalance());
//        holder.pack.setText("" + productuList.get(position).getPackBalance());

        holder.carton.setText("");
        holder.pack.setText("");

        holder.carton.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length()>0 && !editable.toString().equalsIgnoreCase(" ")) {

                    int cartoncount = Integer.parseInt(holder.carton.getText().toString().trim());

                    productuList.get(position).setCartonBalance(cartoncount);
                    productuList.get(position).setModified(true);
                }
            }
        });

        holder.pack.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length()>0 && !editable.toString().equalsIgnoreCase(" ")) {
                    int packcount = Integer.parseInt(holder.pack.getText().toString().trim());

                    productuList.get(position).setPackBalance(packcount);
                    productuList.get(position).setModified(true);

                }
            }
        });

        return convertView;
    }
}

package com.cs.billingpos.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.billingpos.Models.DailyStockResponse;
import com.cs.billingpos.Models.MenuItemsWithoutSub;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.R;
import com.cs.billingpos.Utils.Constants;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Puli on 24-04-2019.
 */

public class ReportAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    private List<DailyStockResponse.SalesReportsDetail> reportsList = new ArrayList<>();

    public ReportAdapter(Context context, List<DailyStockResponse.SalesReportsDetail> orderList) {
        this.context = context;
        this.reportsList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return reportsList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public static class ViewHolder {
        TextView sno, productname, carton,pack;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_reports, null);

            holder.sno= (TextView) convertView.findViewById(R.id.s_number);
            holder.productname= (TextView) convertView.findViewById(R.id.product);
            holder.carton= (TextView) convertView.findViewById(R.id.carton);
            holder.pack= (TextView) convertView.findViewById(R.id.pack);


            if(position < 9) {
                holder.sno.setText("0"+(position+1));
            }
            else {
                holder.sno.setText(""+(position+1));
            }

            holder.productname.setText(""+ reportsList.get(position).getSKUName());

            if(reportsList.get(position).getCarton() < 10) {
                holder.carton.setText("0" + reportsList.get(position).getCarton());
            }
            else {
                holder.carton.setText("" + reportsList.get(position).getCarton());
            }

            if(reportsList.get(position).getPack() < 10) {
                holder.pack.setText("0" + reportsList.get(position).getPack());
            }
            else {
                holder.pack.setText("" + reportsList.get(position).getPack());
            }

        return convertView;
    }
}

package com.cs.billingpos.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.billingpos.Models.DailySalesReport;
import com.cs.billingpos.R;
import com.cs.billingpos.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class DailySalesReportAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    private List<DailySalesReport.SalesReportsDetails> reportsList = new ArrayList<>();

    public DailySalesReportAdapter(Context context, List<DailySalesReport.SalesReportsDetails> orderList) {
        this.context = context;
        this.reportsList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return reportsList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView code, name, price, pack;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        holder = new ViewHolder();
        convertView = inflater.inflate(R.layout.daily_sales_list, null);

        holder.code = (TextView) convertView.findViewById(R.id.code);
        holder.name = (TextView) convertView.findViewById(R.id.name);
        holder.price = (TextView) convertView.findViewById(R.id.price);
        holder.pack = (TextView) convertView.findViewById(R.id.pack);



        holder.name.setText("" + reportsList.get(position).getCustomerName());
        holder.code.setText("" + reportsList.get(position).getTrackingNo());
        holder.price.setText(Constants.priceFormat.format(reportsList.get(position).getTotalsales()));
        holder.pack.setText("" + reportsList.get(position).getPacks());

        return convertView;
    }
}

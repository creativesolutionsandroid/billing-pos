package com.cs.billingpos.Adapters;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.cs.billingpos.Activities.BillingActivity;
import com.cs.billingpos.Activities.CheckOutActivity;
import com.cs.billingpos.Activities.DashboardActivity;
import com.cs.billingpos.Activities.MenuActivity;
import com.cs.billingpos.Activities.ReportsActivity;
import com.cs.billingpos.Models.DailySalesReport;
import com.cs.billingpos.Models.DailyStockResponse;
import com.cs.billingpos.Models.InvoiceDetails;
import com.cs.billingpos.Models.Order;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.MyDatabase;
import com.cs.billingpos.R;
import com.cs.billingpos.Rest.APIInterface;
import com.cs.billingpos.Rest.ApiClient;
import com.cs.billingpos.Utils.Constants;
import com.cs.billingpos.Utils.GPSTracker;
import com.cs.billingpos.Utils.NetworkUtil;
import com.honeywell.mobility.print.LinePrinter;
import com.honeywell.mobility.print.LinePrinterException;
import com.honeywell.mobility.print.PrintProgressEvent;
import com.honeywell.mobility.print.PrintProgressListener;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvocieEditAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    private ArrayList<RoutePlanDetailsResponse.Salesinvoices> reportsList = new ArrayList<>();
    private SharedPreferences userPrefs;
    private SharedPreferences.Editor userPrefsEditor;
    private MyDatabase myDbHelper;
    private ArrayList<RoutePlanDetailsResponse.SKUlist> skUlists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList> nonTobaccoSKULists = new ArrayList<>();
    private ArrayList<RoutePlanDetailsResponse.RoutePlanDetail> customerList = new ArrayList<>();

    private BluetoothAdapter mBluetoothAdapter;
    private ProgressDialog mProgressDlg;
    private ArrayList<BluetoothDevice> mDeviceList = new ArrayList<BluetoothDevice>();

    private int BLUETOOTH_PAIRED = 12;
    private int BLUETOOTH_NOT_PAIRED = 10;
    int deviceSelection = -1;
    private String jsonCmdAttribStr = null;
    ArrayList<InvoiceDetails.EditLastInvoiceDetail> invoices = new ArrayList<>();
    boolean isOffline;
    long trackingNo;
    float outstanding;

    public InvocieEditAdapter(Context context, ArrayList<RoutePlanDetailsResponse.Salesinvoices> orderList,
                              ArrayList<RoutePlanDetailsResponse.SKUlist> skUlists,
                              ArrayList<RoutePlanDetailsResponse.NonTobaccoSKUList> nonTobaccoSKULists,
                              ArrayList<RoutePlanDetailsResponse.RoutePlanDetail> customerList,
                              boolean isOffline, long trackingNo, float outstanding) {
        this.context = context;
        this.reportsList = orderList;
        this.skUlists = skUlists;
        this.isOffline = isOffline;
        this.outstanding = outstanding;
        this.customerList = customerList;
        this.trackingNo = trackingNo;
        this.nonTobaccoSKULists = nonTobaccoSKULists;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        myDbHelper = new MyDatabase(context);

        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
    }

    @Override
    public int getCount() {
        return reportsList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView code, name, price;
        LinearLayout edit, layout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        holder = new ViewHolder();
        convertView = inflater.inflate(R.layout.daily_sales_edit_list, null);

        holder.code = (TextView) convertView.findViewById(R.id.code);
        holder.name = (TextView) convertView.findViewById(R.id.name);
        holder.price = (TextView) convertView.findViewById(R.id.price);
        holder.edit = (LinearLayout) convertView.findViewById(R.id.edit_icon);
        holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);

        holder.name.setText("" + reportsList.get(position).getCustomername());
        holder.code.setText("" + reportsList.get(position).getTrackingno());
        holder.price.setText(Constants.priceFormat.format(reportsList.get(position).getTotalsales()));

        if(position == 0) {
            holder.edit.setVisibility(View.VISIBLE);
        }
        else {
            holder.edit.setVisibility(View.INVISIBLE);
        }

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOffline) {
                    int customerPos = 0;
                    for (int j = 0; j < customerList.size(); j++) {
                        if(trackingNo == customerList.get(j).getTrackingNo()) {
                            customerList.get(j).setOutStandingBalance(outstanding);
                            customerPos = j;
                            break;
                        }
                    }

                    Intent intent = new Intent(context, MenuActivity.class);
                    intent.putExtra("customer", customerList);
                    intent.putExtra("customerpos", customerPos);
                    intent.putExtra("sku", skUlists);
                    intent.putExtra("non_tobacco", nonTobaccoSKULists);
                    context.startActivity(intent);
                }
                else {
                    getInvoiceDetailsApi("" + reportsList.get(position).getId(), true);
                }
            }
        });

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getInvoiceDetailsApi(""+reportsList.get(position).getId(), false);
            }
        });

        return convertView;
    }

    private void getInvoiceDetailsApi(String id, boolean isEdit) {
        final ACProgressFlower dialog = new ACProgressFlower.Builder(context)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        dialog.show();

        final String networkStatus = NetworkUtil.getConnectivityStatusString(context);

        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<InvoiceDetails> call = apiService.editLastInvoice(id, userPrefs.getString("EmpId", ""));
        call.enqueue(new Callback<InvoiceDetails>() {

            public void onResponse(Call<InvoiceDetails> call, Response<InvoiceDetails> response) {
                if (response.isSuccessful()) {
                    Log.d("TAG", "onResponse: " + response);
                    InvoiceDetails routeResponse = response.body();
                    int customerPos = 0;
                    try {
                        //status true case
                        invoices = routeResponse.getEditLastInvoiceDetails();
                        if(invoices.size() > 0) {
                            myDbHelper.deleteOrderTable();
                            for (int i = 0; i < invoices.size(); i++) {
                                HashMap<String, String> values = new HashMap<>();

                                double cartonPrice = invoices.get(i).getCarton() * invoices.get(i).getCartonPrice();
                                double packPrice = invoices.get(i).getPack() * invoices.get(i).getPackPrice();

                                values.put("TypeId", "" + invoices.get(i).getTypeId());
                                values.put("SKUId", "" + invoices.get(i).getSKUId());
                                values.put("Carton", "" + invoices.get(i).getCarton());
                                values.put("Pack", "" + invoices.get(i).getPack());
                                values.put("FreeCarton", "" + invoices.get(i).getFreeCarton());
                                values.put("FreePack", "" + invoices.get(i).getFreePack());
                                values.put("TotalCarton", "" + (invoices.get(i).getCarton() + invoices.get(i).getFreeCarton()));
                                values.put("TotalPack", "" + (invoices.get(i).getPack() + invoices.get(i).getFreePack()));
                                values.put("CartonPrice", "" + invoices.get(i).getCartonPrice());
                                values.put("PackPrice", "" + invoices.get(i).getPackPrice());
                                values.put("TotalCartonPrice", "" + cartonPrice);
                                values.put("TotalPackPrice", "" + packPrice);
                                values.put("TotalPrice", "" + (cartonPrice + packPrice));

                                if(invoices.get(i).getTypeId() == 1) {
                                    for (int j = 0; j < skUlists.size(); j++) {
                                        if(invoices.get(i).getSKUId() == skUlists.get(j).getSKUId()) {
                                            values.put("itemName", skUlists.get(j).getSKUName());
                                            values.put("packPerCarton", "" + skUlists.get(j).getPacksPerCarton());
                                            values.put("subCatId", "" + skUlists.get(j).getCompanyName());
                                            break;
                                        }
                                    }
                                }
                                else if(invoices.get(i).getTypeId() == 2) {
                                    for (int j = 0; j < nonTobaccoSKULists.size(); j++) {
                                        if(invoices.get(i).getSKUId() == nonTobaccoSKULists.get(j).getSKUId()) {
                                            values.put("itemName", nonTobaccoSKULists.get(j).getSKUName());
                                            values.put("packPerCarton", "" + nonTobaccoSKULists.get(j).getPackPerCarton());
                                            values.put("subCatId", "" + nonTobaccoSKULists.get(j).getCompanyName());
                                            break;
                                        }
                                    }
                                }
                                values.put("invoiceId", ""+invoices.get(i).getId());
                                values.put("invoiceNumber", ""+invoices.get(i).getInvoiceNo());
                                values.put("outstandingId", invoices.get(i).getOutstandingBalanceid());
                                values.put("outletName", invoices.get(i).getCustomerName());
                                myDbHelper.insertOrder(values);

                                for (int j = 0; j < customerList.size(); j++) {
                                    if(invoices.get(0).getTrackingNo() == customerList.get(j).getTrackingNo()) {
                                        customerList.get(j).setOutStandingBalance(invoices.get(0).getPreviousOutstanding());
                                        customerPos = j;
                                        break;
                                    }
                                }
                            }

                            if(isEdit) {
                                Intent intent = new Intent(context, MenuActivity.class);
                                intent.putExtra("customer", customerList);
                                intent.putExtra("customerpos", customerPos);
                                intent.putExtra("sku", skUlists);
                                intent.putExtra("non_tobacco", nonTobaccoSKULists);
                                context.startActivity(intent);
                            }
                            else {
                                showMenuDialog();
                            }
                        }
                    } catch (Exception e) {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null)
                        dialog.dismiss();
                } else {
                    if (dialog != null)
                        dialog.dismiss();

                    Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
            }

            public void onFailure(Call<InvoiceDetails> call, Throwable t) {
                Log.d("TAG", "onFailure: "+t.toString());
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }

    private void showMenuDialog(){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.print_alert_dialog;

        final View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        RelativeLayout dialogbox1 =(RelativeLayout) dialogView.findViewById(R.id.pdf);
        RelativeLayout dialogbox2 =(RelativeLayout) dialogView.findViewById(R.id.print);

        ImageView cancel = (ImageView) dialogView.findViewById(R.id.close_popup);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
            }
        });

        dialogbox1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    createPDF();
            }
        });

        dialogbox2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mBluetoothAdapter == null) {
                    Toast.makeText(context, "Bluetooth is unsupported by this device", Toast.LENGTH_SHORT).show();
                }
                else {
                    readAssetFiles();
                    if(!mBluetoothAdapter.isEnabled()) {
                        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        ((Activity) context).startActivityForResult(intent, 1000);
                    }
                    else {
                        Log.d("TAG", "searching devices: ");
                        mProgressDlg = new ProgressDialog(context);

                        mProgressDlg.setMessage("Scanning devices...");
                        mProgressDlg.setCancelable(false);
                        mProgressDlg.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                mBluetoothAdapter.cancelDiscovery();
                            }
                        });
                        mProgressDlg.show();
                        mDeviceList.clear();
                        mBluetoothAdapter.startDiscovery();
                    }
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        DisplayMetrics display = context.getResources().getDisplayMetrics();
//        Point size = new Point();
//        display.w(size);
        int screenWidth = display.widthPixels;

        double d = screenWidth*0.95;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void createPDF(){
        ACProgressFlower dialog = new ACProgressFlower.Builder(context)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        dialog.show();
        Document doc = new Document();
        doc.newPage();

        try {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BillingPos";

            File dir = new File(path);
            if(!dir.exists())
                dir.mkdirs();

            File file = new File(dir, invoices.get(0).getInvoiceNo()+".pdf");
            FileOutputStream fOut = new FileOutputStream(file);

            PdfWriter.getInstance(doc, fOut);

            Font paraFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
                    Font.NORMAL);

            Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 22,
                    Font.BOLD);

            //open the document
            doc.open();
            doc.add(new Chunk(""));

            String text1 ="Billing POS";
            Paragraph p1 = new Paragraph(text1);
            p1.setAlignment(Paragraph.ALIGN_CENTER);
            p1.setFont(boldFont);
            doc.add(p1);

            String text2 = invoices.get(0).getInvoiceNo() ;
            Paragraph p2 = new Paragraph(text2);
            p2.setAlignment(Paragraph.ALIGN_CENTER);
            p2.setFont(boldFont);
            doc.add(p2);

            String text3 = "Cashier    : "+invoices.get(0).getCasier();
            Paragraph p3 = new Paragraph(text3);
            p3.setAlignment(Paragraph.ALIGN_LEFT);
            p3.setFont(paraFont);
            doc.add(p3);

            String text4 = getTodayDate()+" "+getTodayTime();
            Paragraph p4 = new Paragraph(text4);
            p4.setAlignment(Paragraph.ALIGN_LEFT);
            p4.setFont(paraFont);
            doc.add(p4);

            String text5 = "Customer    : "+ invoices.get(0).getCustomerName();
            Paragraph p5 = new Paragraph(text5);
            p5.setAlignment(Paragraph.ALIGN_LEFT);
            p5.setFont(paraFont);
            doc.add(p5);

            String text6 = "Phone         : "+ invoices.get(0).getMobileNo();
            Paragraph p6 = new Paragraph(text6);
            p6.setAlignment(Paragraph.ALIGN_LEFT);
            p6.setFont(paraFont);
            doc.add(p6);

            String text7 = "Addresss     : "+invoices.get(0).getAddress();
            Paragraph p7 = new Paragraph(text7);
            p7.setAlignment(Paragraph.ALIGN_LEFT);
            p7.setFont(paraFont);
            doc.add(p7);

            doc.add(new Chunk(""));

            String text8 ="-------------------------------------------------------------------------------------------------------------------------------";
            Paragraph p8 = new Paragraph(text8);
            p8.setAlignment(Paragraph.ALIGN_RIGHT);
            p8.setFont(paraFont);
//            doc.add(p8);

            String text9 ="Item                                                  "+"UOM                    "+"Qty                 "+"Rate                   "+"Total";
            Paragraph p9 = new Paragraph(text9);
            p9.setAlignment(Paragraph.ALIGN_RIGHT);
            p9.setFont(paraFont);
//            doc.add(p9);

            String text10 ="--------------------------------------------------------------------------------------------------------------------------------";
            Paragraph p10 = new Paragraph(text10);
            p10.setAlignment(Paragraph.ALIGN_RIGHT);
            p10.setFont(paraFont);
//            doc.add(p10);

            PdfPTable table = new PdfPTable(5);
            table.setTotalWidth(1000);
            table.setComplete(true);
            table.setWidthPercentage(100);
            table.setWidths(new int[] {350, 75, 75, 200, 200});
            PdfPCell c1 = new PdfPCell(new Phrase("Item"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setMinimumHeight(20);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("UOM"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setMinimumHeight(20);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Qty"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setMinimumHeight(20);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Rate"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setMinimumHeight(20);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Total"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setMinimumHeight(20);
            table.addCell(c1);
            table.setHeaderRows(1);

            PdfPCell[] cells = table.getRow(0).getCells();
            ArrayList<Order> orderList = new ArrayList<>();
            orderList = myDbHelper.getOrderInfo();
            for (int i = 0; i < orderList.size(); i++){
                String text11 = orderList.get(i).getItemName();
                StringBuilder text123 = new StringBuilder(text11.length());
                text123.append(orderList.get(i).getItemName());
                int lenght = text11.length();

                if(orderList.get(i).getCarton() > 0) {

                    String text001 = Constants.priceFormat.format(orderList.get(i).getCartonPrice());
                    String text002 = Constants.priceFormat.format(orderList.get(i).getCarton() * orderList.get(i).getCartonPrice());
                    c1 = new PdfPCell(new Phrase(text123.toString()));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setMinimumHeight(20);
                    c1.setPaddingLeft(5);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("Ct"));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(""+orderList.get(i).getCarton()));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text001));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setMinimumHeight(20);
                    c1.setPaddingRight(5);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text002));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setMinimumHeight(20);
                    c1.setPaddingRight(5);
                    table.addCell(c1);
                }

                if(orderList.get(i).getPack() > 0) {
                    String text001 = Constants.priceFormat.format(orderList.get(i).getPackPrice());
                    String text002 = Constants.priceFormat.format(orderList.get(i).getPack() * orderList.get(i).getPackPrice());

                    c1 = new PdfPCell(new Phrase(text123.toString()));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setMinimumHeight(20);
                    c1.setPaddingLeft(5);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("Pk"));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(""+orderList.get(i).getPack()));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text001));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setPaddingRight(5);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text002));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setPaddingRight(5);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);
                }

                if(orderList.get(i).getFreeCarton() > 0) {

                    String text001 = "0.00";
                    String text002 = "0.00";
                    c1 = new PdfPCell(new Phrase(text123.toString()));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setMinimumHeight(20);
                    c1.setPaddingLeft(5);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("Ct"));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(""+orderList.get(i).getFreeCarton()));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text001));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setPaddingRight(5);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text002));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setPaddingRight(5);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);
                }

                if(orderList.get(i).getFreePack() > 0) {
                    String text001 = "0.00";
                    String text002 = "0.00";

                    c1 = new PdfPCell(new Phrase(text123.toString()));
                    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c1.setMinimumHeight(20);
                    c1.setPaddingLeft(5);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase("Pk"));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(""+orderList.get(i).getFreePack()));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text001));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setPaddingRight(5);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);

                    c1 = new PdfPCell(new Phrase(text002));
                    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c1.setPaddingRight(5);
                    c1.setMinimumHeight(20);
                    table.addCell(c1);
                }

            }
            doc.add(table);

            String text12 ="---------------------------------------------------------------------------------------------------------------------------------";
            Paragraph p12 = new Paragraph(text12);
            p12.setAlignment(Paragraph.ALIGN_RIGHT);
            p12.setFont(paraFont);
//            doc.add(p12);

            String text001 = Constants.priceFormat.format(invoices.get(0).getSubtotal());
            int lenght2 = text001.length();
            if(text001.length() < 25){
                for (int j = 0; j < (25 - lenght2); j++){
                    text001 =  " "+text001;
                }
            }

            String text13 = "Sub Total "+text001;
            Paragraph p13 = new Paragraph(text13);
            p13.setAlignment(Paragraph.ALIGN_RIGHT);
            p13.setFont(paraFont);
            doc.add(p13);

            String text002 = Constants.priceFormat.format(invoices.get(0).getVatAmount());
            int lenght1 = text002.length();
            if(text002.length() < 25){
                for (int j = 0; j < (25 - lenght1); j++){
                    text002 =  " "+text002;
                }
            }

            String text14 ="VAT(5%) "+text002;
            Paragraph p14 = new Paragraph(text14);
            p14.setAlignment(Paragraph.ALIGN_RIGHT);
            p14.setFont(paraFont);
            doc.add(p14);

            String text003 = Constants.priceFormat.format(invoices.get(0).getInvoiceAmount());
            int lenght = text003.length();
            if(text003.length() < 25){
                for (int j = 0; j < (25 - lenght); j++){
                    text003 =  " "+text003;
                }
            }


            String text15 ="Total "+text003;
            Paragraph p15 = new Paragraph(text15);
            p15.setAlignment(Paragraph.ALIGN_RIGHT);
            p15.setFont(paraFont);
            doc.add(p15);

            String text19 ="----------------------------------------------------------------------------------------------------------------------------------";
            Paragraph p19 = new Paragraph(text19);
            p19.setAlignment(Paragraph.ALIGN_RIGHT);
            p19.setFont(paraFont);
            doc.add(p19);

            String text004 = Constants.priceFormat.format(invoices.get(0).getPreviousOutstanding());
            int lenght4 = text004.length();
            if(text004.length() < 25){
                for (int j = 0; j < (25 - lenght4); j++){
                    text004 =  " "+text004;
                }
            }


            String text16 ="Previous Balance "+text004;
            Paragraph p16 = new Paragraph(text16);
            p16.setAlignment(Paragraph.ALIGN_RIGHT);
            p16.setFont(paraFont);
            doc.add(p16);

            String text005 = Constants.priceFormat.format(invoices.get(0).getInvoiceAmount());
            int lenght5 = text005.length();
            if(text005.length() < 25){
                for (int j = 0; j < (25 - lenght5); j++){
                    text005 =  " "+text005;
                }
            }

            String text25 ="Invoice Amount "+text005;
            Paragraph p17 = new Paragraph(text25);
            p17.setAlignment(Paragraph.ALIGN_RIGHT);
            p17.setFont(paraFont);
            doc.add(p17);

            String text006 = Constants.priceFormat.format(invoices.get(0).getAmountPaid());
            int lenght6 = text006.length();
            if(text006.length() < 25){
                for (int j = 0; j < (25 - lenght6); j++){
                    text006 =  " "+text006;
                }
            }


            String text18 ="Amount Paid "+text006;
            Paragraph p18 = new Paragraph(text18);
            p18.setAlignment(Paragraph.ALIGN_RIGHT);
            p18.setFont(paraFont);
            doc.add(p18);

            String text007 = Constants.priceFormat.format(invoices.get(0).getOutStandingBalance());
            int lenght7 = text007.length();
            if(text007.length() < 25){
                for (int j = 0; j < (25 - lenght7); j++){
                    text007 =  " "+text007;
                }
            }

            String text20 ="Outstanding Balance "+text007;
            Paragraph p20 = new Paragraph(text20);
            p20.setAlignment(Paragraph.ALIGN_RIGHT);
            p20.setFont(paraFont);
            doc.add(p20);


            String text21 ="----------------------------------------------------------------------------------------------------------------------------------";
            Paragraph p21 = new Paragraph(text21);
            p21.setAlignment(Paragraph.ALIGN_RIGHT);
            p21.setFont(paraFont);
            doc.add(p21);


            String text22 ="                                                 "+"SIGN...........................................";
            Paragraph p22 = new Paragraph(text22);
            p22.setAlignment(Paragraph.ALIGN_LEFT);
            p22.setFont(paraFont);
            doc.add(p22);

            String text23 ="                                                 "+"SALES MAN .....................................";
            Paragraph p23 = new Paragraph(text23);
            p23.setAlignment(Paragraph.ALIGN_LEFT);
            p23.setFont(paraFont);
            doc.add(p23);

            String text24 ="                                                 "+"RECEIVER.......................................";
            Paragraph p24 = new Paragraph(text24);
            p24.setAlignment(Paragraph.ALIGN_LEFT);
            p24.setFont(paraFont);
            doc.add(p24);

            String text35 ="                                                           "+"''WE AR PROUD TO SERVE YOU''";
            Paragraph p25 = new Paragraph(text35);
            p25.setAlignment(Paragraph.ALIGN_LEFT);
            p25.setFont(boldFont);
            doc.add(p25);
            //add paragraph to document
            Log.d("PDFCreator", "createPDF: ");

        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        }
        finally {
            doc.close();

        }

        if(dialog!=null) {
            dialog.dismiss();
        }

        viewPdf(invoices.get(0).getInvoiceNo()+".pdf");
    }
    // Method for opening a pdf file
    private void viewPdf(String file) {

        Intent intentShareFile = new Intent(Intent.ACTION_SEND);
        File fileWithinMyDir = new File(Environment.getExternalStorageDirectory() + "/BillingPos/" + file);

        if(fileWithinMyDir.exists()) {
            intentShareFile.setType("application/pdf");
            if(Build.VERSION.SDK_INT>=24){
                try{
                    Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                    m.invoke(null);
                    intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + Environment.getExternalStorageDirectory() + "/BillingPos/" + file));
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
            else {
                intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + Environment.getExternalStorageDirectory() + "/BillingPos/" + file));
            }
            intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                    "Sharing invoice...");
            intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing invoice...");

            context.startActivity(Intent.createChooser(intentShareFile, "Share invoice"));
        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                mProgressDlg.dismiss();

                if(mDeviceList.size() > 0) {
                    showBluetoothDevicesDialog();
                }
                else {
                    Toast.makeText(context, "No devices found", Toast.LENGTH_SHORT).show();
                }
            }else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                mDeviceList.add(device);
            }
        }
    };

    private void showBluetoothDevicesDialog(){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.bluetooth_devices_alert_dialog;

        final View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        ListView devicesListView =(ListView) dialogView.findViewById(R.id.devices_list);
        ImageView cancel = (ImageView) dialogView.findViewById(R.id.close_popup);

        BluetoothDevicesAdapter mAdapter = new BluetoothDevicesAdapter(context, mDeviceList);
        devicesListView.setAdapter(mAdapter);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
            }
        });

        devicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(mDeviceList.get(position).getBondState() == BLUETOOTH_NOT_PAIRED) {
                    pairDevice(mDeviceList.get(position));
                    deviceSelection = position;
                }
                else if(mDeviceList.get(position).getBondState() == BLUETOOTH_PAIRED) {
                    deviceSelection = position;
                    PrintTask task = new PrintTask();
                    // Executes PrintTask with the specified parameter which is passed
                    // to the PrintTask.doInBackground method.
                    task.execute("PR3", mDeviceList.get(deviceSelection).getAddress());
                }
                finalCustomDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        DisplayMetrics display = context.getResources().getDisplayMetrics();
        //        Point size = new Point();
//        display.getSize(size);
        int screenWidth = display.widthPixels;

        double d = screenWidth*0.95;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void pairDevice(BluetoothDevice device) {
        try {
            Method method = device.getClass().getMethod("createBond", (Class[]) null);
            method.invoke(device, (Object[]) null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final BroadcastReceiver mPairReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final int state 		= intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                final int prevState	= intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, BluetoothDevice.ERROR);

                if (state == BluetoothDevice.BOND_BONDED && prevState == BluetoothDevice.BOND_BONDING) {
                    PrintTask task = new PrintTask();
                    // Executes PrintTask with the specified parameter which is passed
                    // to the PrintTask.doInBackground method.
                    task.execute("PR3", mDeviceList.get(deviceSelection).getAddress());
                }
            }
        }
    };

    private void readAssetFiles()
    {
        InputStream input = null;
        ByteArrayOutputStream output = null;
        AssetManager assetManager = context.getAssets();
        String[] files = { "printer_profiles.JSON"};
        int fileIndex = 0;
        int initialBufferSize;

        try
        {
            for (String filename : files)
            {
                input = assetManager.open(filename);
                initialBufferSize = (fileIndex == 0) ? 8000 : 2500;
                output = new ByteArrayOutputStream(initialBufferSize);

                byte[] buf = new byte[1024];
                int len;
                while ((len = input.read(buf)) > 0)
                {
                    output.write(buf, 0, len);
                }
                input.close();
                input = null;

                output.flush();
                output.close();
                switch (fileIndex)
                {
                    case 0:
                        jsonCmdAttribStr = output.toString();
                        break;
                }

                fileIndex++;
                output = null;
            }
        }
        catch (Exception ex)
        {
            Constants.showOneButtonAlertDialog("Error reading asset file: " + files[fileIndex], "Billing Pos", "Ok", (Activity) context);
        }
        finally
        {
            try
            {
                if (input != null)
                {
                    input.close();
                    input = null;
                }

                if (output != null)
                {
                    output.close();
                    output = null;
                }
            }
            catch (IOException e){}
        }
    }

    /**
     * This exception is thrown by the background thread to halt printing attempts and
     * return an error to the UI when the printer status indicates conditions that would
     * prevent successful printing such as "lid open" or "paper out".
     */
    public class BadPrinterStateException extends Exception
    {
        static final long serialVersionUID = 1;

        public BadPrinterStateException(String message)
        {
            super(message);
        }
    }

    /**
     * This class demonstrates printing in a background thread and updates
     * the UI in the UI thread.
     */
    public class PrintTask extends AsyncTask<String, Integer, String> {
        private static final String PROGRESS_CANCEL_MSG = "Printing cancelled\n";
        private static final String PROGRESS_COMPLETE_MSG = "Printing completed\n";
        private static final String PROGRESS_ENDDOC_MSG = "End of document\n";
        private static final String PROGRESS_FINISHED_MSG = "Printer connection closed\n";
        private static final String PROGRESS_NONE_MSG = "Unknown progress message\n";
        private static final String PROGRESS_STARTDOC_MSG = "Start printing document\n";
        ACProgressFlower dialog;

        /**
         * Runs on the UI thread before doInBackground(Params...).
         */
        @Override
        protected void onPreExecute()
        {
            dialog = new ACProgressFlower.Builder(context)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        /**
         * This method runs on a background thread. The specified parameters
         * are the parameters passed to the execute method by the caller of
         * this task. This method can call publishProgress to publish updates
         * on the UI thread.
         */
        @Override
        protected String doInBackground(String... args)
        {
            LinePrinter lp = null;
            String sResult = null;
            String sPrinterID = args[0];
            String sPrinterAddr = args[1];
            String sDocNumber = "1234567890";
            String sPrinterURI = null;

            // The printer address should be a Bluetooth MAC address.
            if (sPrinterAddr.contains(":") == false && sPrinterAddr.length() == 12)
            {
                // If the MAC address only contains hex digits without the
                // ":" delimiter, then add ":" to the MAC address string.
                char[] cAddr = new char[17];

                for (int i=0, j=0; i < 12; i += 2)
                {
                    sPrinterAddr.getChars(i, i+2, cAddr, j);
                    j += 2;
                    if (j < 17)
                    {
                        cAddr[j++] = ':';
                    }
                }

                sPrinterAddr = new String(cAddr);
            }

            sPrinterURI = "bt://" + sPrinterAddr;

            LinePrinter.ExtraSettings exSettings = new LinePrinter.ExtraSettings();

            exSettings.setContext(context);

            PrintProgressListener progressListener =
                    new PrintProgressListener()
                    {
                        @Override
                        public void receivedStatus(PrintProgressEvent aEvent)
                        {
                            // Publishes updates on the UI thread.
                            publishProgress(aEvent.getMessageType());
                        }
                    };

            try
            {
                lp = new LinePrinter(
                        jsonCmdAttribStr,
                        sPrinterID,
                        sPrinterURI,
                        exSettings);

                // Registers to listen for the print progress events.
                lp.addPrintProgressListener(progressListener);

                //A retry sequence in case the bluetooth socket is temporarily not ready
                int numtries = 0;
                int maxretry = 2;
                while(numtries < maxretry)
                {
                    try{
                        lp.connect();  // Connects to the printer
                        break;
                    }
                    catch(LinePrinterException ex){
                        numtries++;
                        Log.d("TAG", "doInBackground: "+ex.getMessage());
                        Thread.sleep(1000);
                    }
                }
                if (numtries == maxretry) lp.connect();//Final retry

                // Check the state of the printer and abort printing if there are
                // any critical errors detected.
                int[] results = lp.getStatus();
                if (results != null)
                {
                    for (int err = 0; err < results.length; err++)
                    {
                        if (results[err] == 223)
                        {
                            // Paper out.
                            throw new BadPrinterStateException("Paper out");
                        }
                        else if (results[err] == 227)
                        {
                            // Lid open.
                            throw new BadPrinterStateException("Printer lid open");
                        }
                    }
                }

                lp.setBold(true);
                lp.write("                Billing POS");
                lp.setBold(false);
                lp.newLine(1);
                lp.write("                    "+invoices.get(0).getInvoiceNo());
                lp.newLine(2);

                lp.write("Cashier  : "+invoices.get(0).getCasier());
                lp.newLine(1);
                lp.write("Date     : "+getTodayDate()+" "+getTodayTime());
                lp.newLine(1);
                lp.write("Customer : "+ invoices.get(0).getCustomerName());
                lp.newLine(1);
                lp.write("Phone    : "+ invoices.get(0).getMobileNo());
                lp.newLine(1);
                lp.write("Addresss : "+invoices.get(0).getAddress());
                lp.newLine(1);
                lp.write("---------------------------------------------------------");
                lp.newLine(1);

                lp.write("Item                    "+" UOM "+"Qty "+"       Rate"+"      Total");
                lp.newLine(1);
                lp.write("---------------------------------------------------------");
                lp.newLine(1);
                ArrayList<Order> orderList = new ArrayList<>();
                orderList = myDbHelper.getOrderInfo();
                for (int i = 0; i < orderList.size(); i++) {
                    String text11 = orderList.get(i).getItemName();
                    StringBuilder text123 = new StringBuilder(text11.length());
                    text123.append(orderList.get(i).getItemName());
                    int lenght = text11.length();
                    if(text11.length() < 25){
                        for (int j = 0; j < (26 - lenght); j++){
                            text123.append(" ");
                        }
                    }
                    else {
                        text123.substring(0,25);
                    }

                    if (orderList.get(i).getCarton() > 0) {

                        String text001 = Constants.priceFormat.format(orderList.get(i).getCartonPrice());
                        int lenght1 = text001.length();
                        if(text001.length() < 11){
                            for (int j = 0; j < (12 - lenght1); j++){
                                text001 =  " "+text001;
                            }
                        }

                        String text002 = Constants.priceFormat.format(orderList.get(i).getCarton() * orderList.get(i).getCartonPrice());
                        int lenght2 = text002.length();
                        if(text002.length() < 11){
                            for (int j = 0; j < (12 - lenght2); j++){
                                text002 =  " "+text002;
                            }
                        }

                        text11 = text123.toString() + "Ct  " + orderList.get(i).getCarton() + " " + text001 +" "+text002;
                        lp.write(text11);
                        lp.newLine(1);
                    }

                    if (orderList.get(i).getPack() > 0) {
                        String text001 = Constants.priceFormat.format(orderList.get(i).getPackPrice());
                        int lenght1 = text001.length();
                        if(text001.length() < 11){
                            for (int j = 0; j < (12 - lenght1); j++){
                                text001 =  " "+text001;
                            }
                        }

                        String text002 = Constants.priceFormat.format(orderList.get(i).getPack() * orderList.get(i).getPackPrice());
                        int lenght2 = text002.length();
                        if(text002.length() < 11){
                            for (int j = 0; j < (12 - lenght2); j++){
                                text002 =  " "+text002;
                            }
                        }

                        text11 = text123.toString() + "Pk  " + orderList.get(i).getPack() + " " + text001 + " "+text002;
                        lp.write(text11);
                        lp.newLine(1);
                    }

                    if (orderList.get(i).getFreeCarton() > 0) {

                        String text001 = "0.00";
                        int lenght2 = text001.length();
                        for (int j = 0; j < (12 - lenght2); j++){
                            text001 =  " "+text001;
                        }

                        String text002 = "0.00";
                        int lenght3 = text002.length();
                        for (int j = 0; j < (12 - lenght3); j++){
                            text002 =  " "+text002;
                        }

                        text11 = text123.toString() +"Ct  " + orderList.get(i).getFreeCarton() + " " + text001+ " "+text002;
                        lp.write(text11);
                        lp.newLine(1);
                    }

                    if (orderList.get(i).getFreePack() > 0) {
                        String text001 = "0.00";
                        int lenght3 = text001.length();
                        for (int j = 0; j < (12 - lenght3); j++){
                            text001 =  " "+text001;
                        }

                        String text002 = "0.00";
                        int lenght2 = text002.length();
                        for (int j = 0; j < (12 - lenght2); j++) {
                            text002 = " " + text002;
                        }

                        text11 = text123.toString() + "Pk  " + orderList.get(i).getFreePack() + " " + text001 + " " + text002;
                        lp.write(text11);
                        lp.newLine(1);
                    }
                }

                lp.write("---------------------------------------------------------");
                lp.newLine(1);

                String text009 = Constants.priceFormat.format(invoices.get(0).getSubtotal());
                int lenght9 = text009.length();
                if(text009.length() < 17){
                    for (int j = 0; j <= (17 - lenght9); j++){
                        text009 =  " "+text009;
                    }
                }
                lp.write("                         Sub Total: "+text009);
                lp.newLine(1);

                String text0010 = Constants.priceFormat.format(invoices.get(0).getVatAmount());
                int lenght10 = text0010.length();
                if(text0010.length() < 17){
                    for (int j = 0; j <= (17 - lenght10); j++){
                        text0010 =  " "+text0010;
                    }
                }
                lp.write("                          VAT(5%): "+text0010);
                lp.newLine(1);

                String text0011 = Constants.priceFormat.format(invoices.get(0).getInvoiceAmount());
                int lenght11 = text0011.length();
                if(text0011.length() < 17){
                    for (int j = 0; j <= (17 - lenght11); j++){
                        text0011 =  " "+text0011;
                    }
                }
                lp.write("                             Total: "+text0011);
                lp.newLine(1);

                lp.write("---------------------------------------------------------");
                lp.newLine(1);

                String text004 = Constants.priceFormat.format(invoices.get(0).getPreviousOutstanding());
                int lenght = text004.length();
                if(text004.length() < 17){
                    for (int j = 0; j <= (17 - lenght); j++){
                        text004 =  " "+text004;
                    }
                }
                lp.write("                  Previous Balance: "+text004);
                lp.newLine(1);

                String text0012 = Constants.priceFormat.format(invoices.get(0).getInvoiceAmount());
                int lenght12 = text0012.length();
                if(text0012.length() < 17){
                    for (int j = 0; j <= (17 - lenght12); j++){
                        text0012 =  " "+text0012;
                    }
                }
                lp.write("                    Invoice Amount: "+text0011);
                lp.newLine(1);

                String text006 = Constants.priceFormat.format(invoices.get(0).getAmountPaid());
                int lenght1 = text006.length();
                if(text006.length() < 17){
                    for (int j = 0; j <= (17 - lenght1); j++){
                        text006 =  " "+text006;
                    }
                }
                lp.write("                       Amount Paid: "+text006);
                lp.newLine(1);


                String text007 = Constants.priceFormat.format(invoices.get(0).getOutStandingBalance());
                int lenght2 = text007.length();
                if(text007.length() < 17){
                    for (int j = 0; j <= (17 - lenght2); j++){
                        text007 =  " "+text007;
                    }
                }
                lp.write("               Outstanding Balance: "+text007);
                lp.newLine(2);
                lp.write("---------------------------------------------------------");
                lp.newLine(2);

                lp.write("      SIGN................................");
                lp.newLine(1);
                lp.write("      SALES MAN...........................");
                lp.newLine(1);
                lp.write("      RECEIVER............................");
                lp.newLine(2);
                lp.write("            'WE AR PROUD TO SERVE YOU'");
                lp.newLine(1);
                lp.newLine(4);


                sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
            }
            catch (BadPrinterStateException ex)
            {
                // Stop listening for printer events.
                lp.removePrintProgressListener(progressListener);
                sResult = "Printer error detected: " + ex.getMessage() + ". Please correct the error and try again.";
            }
            catch (LinePrinterException ex)
            {
                sResult = "LinePrinterException: " + ex.getMessage();
            }
            catch (Exception ex)
            {
                if (ex.getMessage() != null)
                    sResult = "Unexpected exception: " + ex.getMessage();
                else
                    sResult = "Unexpected exception.";
            }
            finally
            {
                if (lp != null)
                {
                    try
                    {
                        lp.disconnect();  // Disconnects from the printer
                        lp.close();  // Releases resources
                    }
                    catch (Exception ex) {}
                }
            }

            // The result string will be passed to the onPostExecute method
            // for display in the the Progress and Status text box.
            return sResult;
        }

        /**
         * Runs on the UI thread after doInBackground method. The specified
         * result parameter is the value returned by doInBackground.
         */
        @Override
        protected void onPostExecute(String result)
        {
            // Displays the result (number of bytes sent to the printer or
            // exception message) in the Progress and Status text box.
            if (result != null)
            {
                if(result.contains("Number of bytes sent to printer:")) {
                    myDbHelper.deleteOrderTable();
                }
                else {
                    Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
                }
            }

            if(dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private String getTodayDate(){
        String dateStr = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        Calendar calendar = Calendar.getInstance();
        dateStr = sdf.format(calendar.getTime());

        return dateStr;
    }

    private String getTodayTime(){
        String dateStr = "";
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.US);

        Calendar calendar = Calendar.getInstance();
        dateStr = sdf.format(calendar.getTime());

        return dateStr;
    }
}

package com.cs.billingpos.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.billingpos.Models.Customers;
import com.cs.billingpos.Models.MenuItems;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.R;
import com.cs.billingpos.Utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomersAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    private ArrayList<Customers> customersLists = new ArrayList<>();

    public CustomersAdapter(Context context, ArrayList<Customers> orderList) {
        this.context = context;
        this.customersLists = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return customersLists.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView  name;
        ImageView status;
        LinearLayout customersList;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_zone, null);

            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.status = (ImageView) convertView.findViewById(R.id.zone_status);
            holder.customersList = (LinearLayout) convertView.findViewById(R.id.customers_list);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(customersLists.get(position).getZone());

        holder.customersList.removeAllViews();
        boolean allSeen = true;
        for (int i = 0; i < customersLists.get(position).getList().size(); i++){
            View v = inflater.inflate(R.layout.list_customer, null);
            TextView customerName = v.findViewById(R.id.name);
            final ImageView status = v.findViewById(R.id.customer_status);
            RelativeLayout layout = v.findViewById(R.id.layout);

            customerName.setText(customersLists.get(position).getList().get(i).getCustomer());
            if(customersLists.get(position).getList().get(i).getSeen()){
                status.setImageDrawable(context.getResources().getDrawable(R.drawable.customer_seen));
            }
            else {
                allSeen = false;
                status.setImageDrawable(context.getResources().getDrawable(R.drawable.customer_unseen));
            }

            final int finalI = i;
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMenuDialog(customersLists.get(position).getList(), finalI);
                    status.setImageDrawable(context.getResources().getDrawable(R.drawable.customer_seen));
                    customersLists.get(position).getList().get(finalI).setSeen(true);
                }
            });
            holder.customersList.addView(v);
        }

        if(allSeen) {
            holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.zone_seen));
        }
        else {
            holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.zone_unseen));
        }

        return convertView;
    }

    private void showMenuDialog(final ArrayList<Customers.customerList> items, final int pos){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.customer_details_dialog;

        final View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

        TextView area = (TextView) dialogView.findViewById(R.id.area);
        TextView name = (TextView) dialogView.findViewById(R.id.name);
        TextView phone = (TextView) dialogView.findViewById(R.id.phone);
        TextView email = (TextView) dialogView.findViewById(R.id.email);
        TextView address = (TextView) dialogView.findViewById(R.id.address);
        TextView bal = (TextView) dialogView.findViewById(R.id.balance);
        ImageView cancel = (ImageView) dialogView.findViewById(R.id.close_popup);

        area.setText(items.get(pos).getArea());
        name.setText(items.get(pos).getCustomer());
        phone.setText(items.get(pos).getMobile());
        email.setText(items.get(pos).getEmail());
        address.setText(items.get(pos).getAddress());
        bal.setText(Constants.priceFormat.format(items.get(pos).getOutstanding()));

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalCustomDialog.dismiss();
                notifyDataSetChanged();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        DisplayMetrics display = context.getResources().getDisplayMetrics();
//        Point size = new Point();
//        display.w(size);
        int screenWidth = display.widthPixels;

        double d = screenWidth*0.95;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}

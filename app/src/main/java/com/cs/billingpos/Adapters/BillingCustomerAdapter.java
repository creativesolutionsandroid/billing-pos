package com.cs.billingpos.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.R;
import com.cs.billingpos.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class BillingCustomerAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    private ArrayList<RoutePlanDetailsResponse.RoutePlanDetail> customersLists = new ArrayList<>();

    public BillingCustomerAdapter(Context context, ArrayList<RoutePlanDetailsResponse.RoutePlanDetail> orderList) {
        this.context = context;
        this.customersLists = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return customersLists.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView code, name, balance;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_billing_customers, null);

            holder.code = (TextView) convertView.findViewById(R.id.code);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.balance = (TextView) convertView.findViewById(R.id.balance);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.code.setText(""+customersLists.get(position).getTrackingNo());
        holder.name.setText(customersLists.get(position).getOutletName());

        try {
            holder.balance.setText(Constants.priceFormat.format(customersLists.get(position).getOutStandingBalance()));
        } catch (Exception e) {
            e.printStackTrace();
            holder.balance.setText("0.00");
        }

        return convertView;
    }
}

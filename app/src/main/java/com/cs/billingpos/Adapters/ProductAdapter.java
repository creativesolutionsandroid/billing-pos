package com.cs.billingpos.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.billingpos.Models.MenuItems;
import com.cs.billingpos.Models.MenuItemsWithoutSub;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.R;
import com.cs.billingpos.Utils.Constants;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


/**
 * Created by Puli on 24-04-2019.
 */

public class ProductAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    private ArrayList<MenuItemsWithoutSub.Items> productuList = new ArrayList<>();

    public ProductAdapter(Context context, ArrayList<MenuItemsWithoutSub.Items> orderList) {
        this.context = context;
        this.productuList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return productuList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView sno, productname, carton,pack, cartonPrice, packPrice;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_product, null);

            holder.sno= (TextView) convertView.findViewById(R.id.s_number);
            holder.productname= (TextView) convertView.findViewById(R.id.product);
            holder.carton= (TextView) convertView.findViewById(R.id.carton);
            holder.pack= (TextView) convertView.findViewById(R.id.pack);
            holder.cartonPrice= (TextView) convertView.findViewById(R.id.carton_price);
            holder.packPrice= (TextView) convertView.findViewById(R.id.pack_price);

            if(position < 9) {
                holder.sno.setText("0"+(position+1));
            }
            else {
                holder.sno.setText(""+(position+1));
            }

            holder.productname.setText(""+productuList.get(position).getSKUName());
            holder.cartonPrice.setText(Constants.priceFormat.format(productuList.get(position).getCartonPrice()));
            holder.packPrice.setText(Constants.priceFormat.format(productuList.get(position).getPackPrice()));

            if(productuList.get(position).getCartonBalance() < 10) {
                holder.carton.setText("0" + productuList.get(position).getCartonBalance());
            }
            else {
                holder.carton.setText("" + productuList.get(position).getCartonBalance());
            }

            if(productuList.get(position).getPackBalance() < 10) {
                holder.pack.setText("0" + productuList.get(position).getPackBalance());
            }
            else {
                holder.pack.setText("" + productuList.get(position).getPackBalance());
            }

        return convertView;
    }
}

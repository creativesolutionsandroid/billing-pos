package com.cs.billingpos.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InsertProductDetails {


    @Expose
    @SerializedName("Success")
    private Success Success;

    public Success getSuccess() {
        return Success;
    }

    public void setSuccess(Success Success) {
        this.Success = Success;
    }

    public static class Success {
        @Expose
        @SerializedName("Message")
        private String Message;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }
    }
}

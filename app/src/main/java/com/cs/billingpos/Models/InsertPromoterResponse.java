package com.cs.billingpos.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InsertPromoterResponse {


    @Expose
    @SerializedName("Success")
    private Success Success;

    @Expose
    @SerializedName("Failure")
    private String Failure;

    public String getFailure() {
        return Failure;
    }

    public void setFailure(String failure) {
        Failure = failure;
    }

    public Success getSuccess() {
        return Success;
    }

    public void setSuccess(Success Success) {
        this.Success = Success;
    }

    public class Success {
        @Expose
        @SerializedName("Message")
        private String Message;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }
    }
}

package com.cs.billingpos.Models;

import java.util.ArrayList;


public class BrandItems {

    private String brandName;
    private int brandId;
    private ArrayList<Items> items = new ArrayList<>();

    public ArrayList<Items> getItems() {
        return items;
    }

    public void setItems(ArrayList<Items> items) {
        this.items = items;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public static class Items {
        private int packBalance;
        private int cartonBalance;
        private float packPrice;
        private float cartonPrice;
        private String packImage;
        private String sKUName;
        private int sKUId;

        public int getPackBalance() {
            return packBalance;
        }

        public void setPackBalance(int packBalance) {
            this.packBalance = packBalance;
        }

        public int getCartonBalance() {
            return cartonBalance;
        }

        public void setCartonBalance(int cartonBalance) {
            this.cartonBalance = cartonBalance;
        }

        public float getPackPrice() {
            return packPrice;
        }

        public void setPackPrice(float packPrice) {
            this.packPrice = packPrice;
        }

        public float getCartonPrice() {
            return cartonPrice;
        }

        public void setCartonPrice(float cartonPrice) {
            this.cartonPrice = cartonPrice;
        }

        public String getPackImage() {
            return packImage;
        }

        public void setPackImage(String packImage) {
            this.packImage = packImage;
        }

        public String getSKUName() {
            return sKUName;
        }

        public void setSKUName(String sKUName) {
            this.sKUName = sKUName;
        }

        public int getSKUId() {
            return sKUId;
        }

        public void setSKUId(int sKUId) {
            this.sKUId = sKUId;
        }
    }
}

package com.cs.billingpos.Models;

public class Items {

    private int packBalance;
    private int cartonBalance;
    private float packPrice;
    private float cartonPrice;
    private String packImage;
    private String sKUName;
    private int sKUId;
    private Integer typeId;
    private boolean isModified = false;

    public boolean getModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    private Integer PackPerCarton;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getsKUName() {
        return sKUName;
    }

    public void setsKUName(String sKUName) {
        this.sKUName = sKUName;
    }

    public int getsKUId() {
        return sKUId;
    }

    public void setsKUId(int sKUId) {
        this.sKUId = sKUId;
    }

    public Integer getPackPerCarton() {
        return PackPerCarton;
    }

    public void setPackPerCarton(Integer packPerCarton) {
        PackPerCarton = packPerCarton;
    }

    public int getPackBalance() {
            return packBalance;
        }

        public void setPackBalance(int packBalance) {
            this.packBalance = packBalance;
        }

        public int getCartonBalance() {
            return cartonBalance;
        }

        public void setCartonBalance(int cartonBalance) {
            this.cartonBalance = cartonBalance;
        }

        public float getPackPrice() {
            return packPrice;
        }

        public void setPackPrice(float packPrice) {
            this.packPrice = packPrice;
        }

        public float getCartonPrice() {
            return cartonPrice;
        }

        public void setCartonPrice(float cartonPrice) {
            this.cartonPrice = cartonPrice;
        }

        public String getPackImage() {
            return packImage;
        }

        public void setPackImage(String packImage) {
            this.packImage = packImage;
        }

        public String getSKUName() {
            return sKUName;
        }

        public void setSKUName(String sKUName) {
            this.sKUName = sKUName;
        }

        public int getSKUId() {
            return sKUId;
        }

        public void setSKUId(int sKUId) {
            this.sKUId = sKUId;
        }
}

package com.cs.billingpos.Models;

public class Order  {

    String orderId, itemName, subCatId, invoiceId, invoiceNumber, outstandingId, outletName;
    int TypeId, SKUId, Carton, Pack, FreeCarton, FreePack, TotalCarton, TotalPack, packPerCarton;
    float CartonPrice, PackPrice, TotalCartonPrice, TotalPackPrice, TotalPrice;

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getOutstandingId() {
        return outstandingId;
    }

    public void setOutstandingId(String outstandingId) {
        this.outstandingId = outstandingId;
    }

    public int getPackPerCarton() {
        return packPerCarton;
    }

    public void setPackPerCarton(int packPerCarton) {
        this.packPerCarton = packPerCarton;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public float getCartonPrice() {
        return CartonPrice;
    }

    public void setCartonPrice(float cartonPrice) {
        CartonPrice = cartonPrice;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getTypeId() {
        return TypeId;
    }

    public void setTypeId(int typeId) {
        TypeId = typeId;
    }

    public int getSKUId() {
        return SKUId;
    }

    public void setSKUId(int SKUId) {
        this.SKUId = SKUId;
    }

    public int getCarton() {
        return Carton;
    }

    public void setCarton(int carton) {
        Carton = carton;
    }

    public int getPack() {
        return Pack;
    }

    public void setPack(int pack) {
        Pack = pack;
    }

    public int getFreeCarton() {
        return FreeCarton;
    }

    public void setFreeCarton(int freeCarton) {
        FreeCarton = freeCarton;
    }

    public int getFreePack() {
        return FreePack;
    }

    public void setFreePack(int freePack) {
        FreePack = freePack;
    }

    public int getTotalCarton() {
        return TotalCarton;
    }

    public void setTotalCarton(int totalCarton) {
        TotalCarton = totalCarton;
    }

    public int getTotalPack() {
        return TotalPack;
    }

    public void setTotalPack(int totalPack) {
        TotalPack = totalPack;
    }

    public float getPackPrice() {
        return PackPrice;
    }

    public void setPackPrice(float packPrice) {
        PackPrice = packPrice;
    }

    public float getTotalCartonPrice() {
        return TotalCartonPrice;
    }

    public void setTotalCartonPrice(float totalCartonPrice) {
        TotalCartonPrice = totalCartonPrice;
    }

    public float getTotalPackPrice() {
        return TotalPackPrice;
    }

    public void setTotalPackPrice(float totalPackPrice) {
        TotalPackPrice = totalPackPrice;
    }

    public float getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        TotalPrice = totalPrice;
    }
}

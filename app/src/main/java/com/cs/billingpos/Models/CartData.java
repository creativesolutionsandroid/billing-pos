package com.cs.billingpos.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class CartData {


    @Expose
    @SerializedName("OutStandingBalance")
    private Outstandingbalance outstandingbalance;
    @Expose
    @SerializedName("SalesDetail")
    private ArrayList<Salesdetail> salesdetail;
    @Expose
    @SerializedName("SalesMaster")
    private Salesmaster salesmaster;

    public Outstandingbalance getOutstandingbalance() {
        return outstandingbalance;
    }

    public void setOutstandingbalance(Outstandingbalance outstandingbalance) {
        this.outstandingbalance = outstandingbalance;
    }

    public ArrayList<Salesdetail> getSalesdetail() {
        return salesdetail;
    }

    public void setSalesdetail(ArrayList<Salesdetail> salesdetail) {
        this.salesdetail = salesdetail;
    }

    public Salesmaster getSalesmaster() {
        return salesmaster;
    }

    public void setSalesmaster(Salesmaster salesmaster) {
        this.salesmaster = salesmaster;
    }

    public static class Outstandingbalance {
        @Expose
        @SerializedName("IsInvoice")
        private boolean isinvoice;
        @Expose
        @SerializedName("DateofVisit")
        private String dateofvisit;
        @Expose
        @SerializedName("OutStandingBalance")
        private double outstandingbalance;
        @Expose
        @SerializedName("AmountPaid")
        private float amountpaid;
        @Expose
        @SerializedName("PreviousOutstanding")
        private float previousoutstanding;
        @Expose
        @SerializedName("InvoiceNo")
        private String invoiceno;
        @Expose
        @SerializedName("TrackingNo")
        private long trackingno;

        public boolean getIsinvoice() {
            return isinvoice;
        }

        public void setIsinvoice(boolean isinvoice) {
            this.isinvoice = isinvoice;
        }

        public String getDateofvisit() {
            return dateofvisit;
        }

        public void setDateofvisit(String dateofvisit) {
            this.dateofvisit = dateofvisit;
        }

        public double getOutstandingbalance() {
            return outstandingbalance;
        }

        public void setOutstandingbalance(double outstandingbalance) {
            this.outstandingbalance = outstandingbalance;
        }

        public float getAmountpaid() {
            return amountpaid;
        }

        public void setAmountpaid(float amountpaid) {
            this.amountpaid = amountpaid;
        }

        public float getPreviousoutstanding() {
            return previousoutstanding;
        }

        public void setPreviousoutstanding(float previousoutstanding) {
            this.previousoutstanding = previousoutstanding;
        }

        public String getInvoiceno() {
            return invoiceno;
        }

        public void setInvoiceno(String invoiceno) {
            this.invoiceno = invoiceno;
        }

        public long getTrackingno() {
            return trackingno;
        }

        public void setTrackingno(long trackingno) {
            this.trackingno = trackingno;
        }
    }

    public static class Salesdetail {
        @Expose
        @SerializedName("isActive")
        private boolean isactive;
        @Expose
        @SerializedName("DateofVisit")
        private String dateofvisit;
        @Expose
        @SerializedName("TotalPrice")
        private float totalprice;
        @Expose
        @SerializedName("TotalPackPrice")
        private float totalpackprice;
        @Expose
        @SerializedName("TotalCartonPrice")
        private float totalcartonprice;
        @Expose
        @SerializedName("PackPrice")
        private float packprice;
        @Expose
        @SerializedName("CartonPrice")
        private float cartonprice;
        @Expose
        @SerializedName("FreeTotalPack")
        private int freetotalpack;
        @Expose
        @SerializedName("FreePack")
        private int freepack;
        @Expose
        @SerializedName("FreeCarton")
        private int freecarton;
        @Expose
        @SerializedName("TotalPack")
        private int totalpack;
        @Expose
        @SerializedName("Pack")
        private int pack;
        @Expose
        @SerializedName("Carton")
        private int carton;
        @Expose
        @SerializedName("SKUId")
        private int skuid;
        @Expose
        @SerializedName("TypeId")
        private int typeid;
        @Expose
        @SerializedName("InvoiceNo")
        private String invoiceno;

        public boolean getIsactive() {
            return isactive;
        }

        public void setIsactive(boolean isactive) {
            this.isactive = isactive;
        }

        public String getDateofvisit() {
            return dateofvisit;
        }

        public void setDateofvisit(String dateofvisit) {
            this.dateofvisit = dateofvisit;
        }

        public float getTotalprice() {
            return totalprice;
        }

        public void setTotalprice(float totalprice) {
            this.totalprice = totalprice;
        }

        public float getTotalpackprice() {
            return totalpackprice;
        }

        public void setTotalpackprice(float totalpackprice) {
            this.totalpackprice = totalpackprice;
        }

        public float getTotalcartonprice() {
            return totalcartonprice;
        }

        public void setTotalcartonprice(float totalcartonprice) {
            this.totalcartonprice = totalcartonprice;
        }

        public float getPackprice() {
            return packprice;
        }

        public void setPackprice(float packprice) {
            this.packprice = packprice;
        }

        public float getCartonprice() {
            return cartonprice;
        }

        public void setCartonprice(float cartonprice) {
            this.cartonprice = cartonprice;
        }

        public int getFreetotalpack() {
            return freetotalpack;
        }

        public void setFreetotalpack(int freetotalpack) {
            this.freetotalpack = freetotalpack;
        }

        public int getFreepack() {
            return freepack;
        }

        public void setFreepack(int freepack) {
            this.freepack = freepack;
        }

        public int getFreecarton() {
            return freecarton;
        }

        public void setFreecarton(int freecarton) {
            this.freecarton = freecarton;
        }

        public int getTotalpack() {
            return totalpack;
        }

        public void setTotalpack(int totalpack) {
            this.totalpack = totalpack;
        }

        public int getPack() {
            return pack;
        }

        public void setPack(int pack) {
            this.pack = pack;
        }

        public int getCarton() {
            return carton;
        }

        public void setCarton(int carton) {
            this.carton = carton;
        }

        public int getSkuid() {
            return skuid;
        }

        public void setSkuid(int skuid) {
            this.skuid = skuid;
        }

        public int getTypeid() {
            return typeid;
        }

        public void setTypeid(int typeid) {
            this.typeid = typeid;
        }

        public String getInvoiceno() {
            return invoiceno;
        }

        public void setInvoiceno(String invoiceno) {
            this.invoiceno = invoiceno;
        }
    }

    public static class Salesmaster {
        @Expose
        @SerializedName("EndTime")
        private String endtime;
        @Expose
        @SerializedName("StartTime")
        private String starttime;
        @Expose
        @SerializedName("Longitude")
        private double longitude;
        @Expose
        @SerializedName("Latitude")
        private double latitude;
        @Expose
        @SerializedName("DateofVisit")
        private String dateofvisit;
        @Expose
        @SerializedName("customerName")
        private String customerName;
        @Expose
        @SerializedName("GrandTotalAmount")
        private float grandtotalamount;
        @Expose
        @SerializedName("DiscountAmount")
        private int discountamount;
        @Expose
        @SerializedName("Quantity")
        private int quantity;
        @Expose
        @SerializedName("NoofItems")
        private int noofitems;
        @Expose
        @SerializedName("InvoiceAmount")
        private double invoiceamount;
        @Expose
        @SerializedName("VatAmount")
        private double vatamount;
        @Expose
        @SerializedName("VatPercent")
        private String vatpercent;
        @Expose
        @SerializedName("Subtotal")
        private int subtotal;
        @Expose
        @SerializedName("TrackingNo")
        private long trackingno;
        @Expose
        @SerializedName("salesmanid")
        private String salesmanid;
        @Expose
        @SerializedName("InvoiceNo")
        private String invoiceno;
        @Expose
        @SerializedName("id")
        private String id;

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public String getDateofvisit() {
            return dateofvisit;
        }

        public void setDateofvisit(String dateofvisit) {
            this.dateofvisit = dateofvisit;
        }

        public float getGrandtotalamount() {
            return grandtotalamount;
        }

        public void setGrandtotalamount(float grandtotalamount) {
            this.grandtotalamount = grandtotalamount;
        }

        public int getDiscountamount() {
            return discountamount;
        }

        public void setDiscountamount(int discountamount) {
            this.discountamount = discountamount;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public int getNoofitems() {
            return noofitems;
        }

        public void setNoofitems(int noofitems) {
            this.noofitems = noofitems;
        }

        public double getInvoiceamount() {
            return invoiceamount;
        }

        public void setInvoiceamount(double invoiceamount) {
            this.invoiceamount = invoiceamount;
        }

        public double getVatamount() {
            return vatamount;
        }

        public void setVatamount(double vatamount) {
            this.vatamount = vatamount;
        }

        public String getVatpercent() {
            return vatpercent;
        }

        public void setVatpercent(String vatpercent) {
            this.vatpercent = vatpercent;
        }

        public int getSubtotal() {
            return subtotal;
        }

        public void setSubtotal(int subtotal) {
            this.subtotal = subtotal;
        }

        public long getTrackingno() {
            return trackingno;
        }

        public void setTrackingno(long trackingno) {
            this.trackingno = trackingno;
        }

        public String getSalesmanid() {
            return salesmanid;
        }

        public void setSalesmanid(String salesmanid) {
            this.salesmanid = salesmanid;
        }

        public String getInvoiceno() {
            return invoiceno;
        }

        public void setInvoiceno(String invoiceno) {
            this.invoiceno = invoiceno;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}

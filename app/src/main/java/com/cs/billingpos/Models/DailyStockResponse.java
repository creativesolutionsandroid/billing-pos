package com.cs.billingpos.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DailyStockResponse {
    @SerializedName("SalesReportsDetails")
    @Expose
    private ArrayList<SalesReportsDetail> salesReportsDetails = null;

    public ArrayList<SalesReportsDetail> getSalesReportsDetails() {
        return salesReportsDetails;
    }

    public void setSalesReportsDetails(ArrayList<SalesReportsDetail> salesReportsDetails) {
        this.salesReportsDetails = salesReportsDetails;
    }


    public static class SalesReportsDetail {

        @SerializedName("SKUId")
        @Expose
        private Integer sKUId;
        @SerializedName("SKUName")
        @Expose
        private String sKUName;
        @SerializedName("typeid")
        @Expose
        private Integer typeid;
        @SerializedName("Carton")
        @Expose
        private Integer carton;
        @SerializedName("Pack")
        @Expose
        private Integer pack;

        public Integer getSKUId() {
            return sKUId;
        }

        public void setSKUId(Integer sKUId) {
            this.sKUId = sKUId;
        }

        public String getSKUName() {
            return sKUName;
        }

        public void setSKUName(String sKUName) {
            this.sKUName = sKUName;
        }

        public Integer getTypeid() {
            return typeid;
        }

        public void setTypeid(Integer typeid) {
            this.typeid = typeid;
        }

        public Integer getCarton() {
            return carton;
        }

        public void setCarton(Integer carton) {
            this.carton = carton;
        }

        public Integer getPack() {
            return pack;
        }

        public void setPack(Integer pack) {
            this.pack = pack;
        }

    }
}

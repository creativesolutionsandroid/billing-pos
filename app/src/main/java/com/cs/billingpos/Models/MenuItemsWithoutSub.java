package com.cs.billingpos.Models;

import java.util.ArrayList;


public class MenuItemsWithoutSub {

    private String sKUCategoryName;
    private int sKUCategoryId;
    private ArrayList<Items> items;

    public ArrayList<Items> getItems() {
        return items;
    }

    public void setItems(ArrayList<Items> items) {
        this.items = items;
    }


    public String getSKUCategoryName() {
        return sKUCategoryName;
    }

    public void setSKUCategoryName(String sKUCategoryName) {
        this.sKUCategoryName = sKUCategoryName;
    }

    public int getSKUCategoryId() {
        return sKUCategoryId;
    }

    public void setSKUCategoryId(int sKUCategoryId) {
        this.sKUCategoryId = sKUCategoryId;
    }



    public static class Items {
        private int packBalance;
        private int cartonBalance;
        private float packPrice;
        private float cartonPrice;
        private String packImage;
        private String sKUName;
        private int sKUId;

        public int getPackBalance() {
            return packBalance;
        }

        public void setPackBalance(int packBalance) {
            this.packBalance = packBalance;
        }

        public int getCartonBalance() {
            return cartonBalance;
        }

        public void setCartonBalance(int cartonBalance) {
            this.cartonBalance = cartonBalance;
        }

        public float getPackPrice() {
            return packPrice;
        }

        public void setPackPrice(float packPrice) {
            this.packPrice = packPrice;
        }

        public float getCartonPrice() {
            return cartonPrice;
        }

        public void setCartonPrice(float cartonPrice) {
            this.cartonPrice = cartonPrice;
        }

        public String getPackImage() {
            return packImage;
        }

        public void setPackImage(String packImage) {
            this.packImage = packImage;
        }

        public String getSKUName() {
            return sKUName;
        }

        public void setSKUName(String sKUName) {
            this.sKUName = sKUName;
        }

        public int getSKUId() {
            return sKUId;
        }

        public void setSKUId(int sKUId) {
            this.sKUId = sKUId;
        }
    }
}

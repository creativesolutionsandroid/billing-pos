package com.cs.billingpos.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class MenuItems {

    public ArrayList<SubCategory> SubCategory;
    private String sKUCategoryName;
    private int sKUCategoryId;

    public ArrayList<SubCategory> getSubCategory() {
        return SubCategory;
    }

    public void setSubCategory(ArrayList<SubCategory> SubCategory) {
        this.SubCategory = SubCategory;
    }

    public String getSKUCategoryName() {
        return sKUCategoryName;
    }

    public void setSKUCategoryName(String sKUCategoryName) {
        this.sKUCategoryName = sKUCategoryName;
    }

    public int getSKUCategoryId() {
        return sKUCategoryId;
    }

    public void setSKUCategoryId(int sKUCategoryId) {
        this.sKUCategoryId = sKUCategoryId;
    }

    public static class SubCategory {
        private ArrayList<Items> items;
        private String companyName;
        private int companyId;

        public ArrayList<Items> getItems() {
            return items;
        }

        public void setItems(ArrayList<Items> items) {
            this.items = items;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public int getCompanyId() {
            return companyId;
        }

        public void setCompanyId(int companyId) {
            this.companyId = companyId;
        }
    }

    public static class Items {
        private int packBalance;
        private int cartonBalance;
        private int packsPerCarton;
        private float packPrice;
        private float cartonPrice;
        private String packImage;
        private String sKUName;
        private int sKUId;

        public int getPacksPerCarton() {
            return packsPerCarton;
        }

        public void setPacksPerCarton(int packsPerCarton) {
            this.packsPerCarton = packsPerCarton;
        }

        public String getsKUName() {
            return sKUName;
        }

        public void setsKUName(String sKUName) {
            this.sKUName = sKUName;
        }

        public int getsKUId() {
            return sKUId;
        }

        public void setsKUId(int sKUId) {
            this.sKUId = sKUId;
        }

        public int getPackBalance() {
            return packBalance;
        }

        public void setPackBalance(int packBalance) {
            this.packBalance = packBalance;
        }

        public int getCartonBalance() {
            return cartonBalance;
        }

        public void setCartonBalance(int cartonBalance) {
            this.cartonBalance = cartonBalance;
        }

        public float getPackPrice() {
            return packPrice;
        }

        public void setPackPrice(float packPrice) {
            this.packPrice = packPrice;
        }

        public float getCartonPrice() {
            return cartonPrice;
        }

        public void setCartonPrice(float cartonPrice) {
            this.cartonPrice = cartonPrice;
        }

        public String getPackImage() {
            return packImage;
        }

        public void setPackImage(String packImage) {
            this.packImage = packImage;
        }

        public String getSKUName() {
            return sKUName;
        }

        public void setSKUName(String sKUName) {
            this.sKUName = sKUName;
        }

        public int getSKUId() {
            return sKUId;
        }

        public void setSKUId(int sKUId) {
            this.sKUId = sKUId;
        }
    }
}

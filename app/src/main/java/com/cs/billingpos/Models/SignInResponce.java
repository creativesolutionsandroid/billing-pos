package com.cs.billingpos.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class SignInResponce {

    @Expose
    @SerializedName("Success")
    private Success success;

    @Expose
    @SerializedName("Failure")
    private String failure;

    public Success getSuccess() {
        return success;
    }

    public void setSuccess(Success success) {
        this.success = success;
    }

    public String getFailure() {
        return failure;
    }

    public void setFailure(String failure) {
        this.failure = failure;
    }

    public static class Success {
        @Expose
        @SerializedName("Role")
        private String role;
        @Expose
        @SerializedName("EmpName")
        private String empname;
        @Expose
        @SerializedName("EmpId")
        private String empid;

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getEmpname() {
            return empname;
        }

        public void setEmpname(String empname) {
            this.empname = empname;
        }

        public String getEmpid() {
            return empid;
        }

        public void setEmpid(String empid) {
            this.empid = empid;
        }
    }
}

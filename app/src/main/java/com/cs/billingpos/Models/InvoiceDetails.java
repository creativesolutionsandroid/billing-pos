package com.cs.billingpos.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class InvoiceDetails {

    @SerializedName("EditLastInvoiceDetails")
    @Expose
    private ArrayList<EditLastInvoiceDetail> editLastInvoiceDetails = new ArrayList<>();

    public ArrayList<EditLastInvoiceDetail> getEditLastInvoiceDetails() {
        return editLastInvoiceDetails;
    }

    public void setEditLastInvoiceDetails(ArrayList<EditLastInvoiceDetail> editLastInvoiceDetails) {
        this.editLastInvoiceDetails = editLastInvoiceDetails;
    }

    public class EditLastInvoiceDetail {

        @SerializedName("id")
        @Expose
        private Long id;
        @SerializedName("TrackingNo")
        @Expose
        private Long trackingNo;
        @SerializedName("CustomerName")
        @Expose
        private String customerName;
        @SerializedName("InvoiceNo")
        @Expose
        private String invoiceNo;
        @SerializedName("Subtotal")
        @Expose
        private float subtotal;
        @SerializedName("VatPercent")
        @Expose
        private float vatPercent;
        @SerializedName("VatAmount")
        @Expose
        private float vatAmount;
        @SerializedName("InvoiceAmount")
        @Expose
        private float invoiceAmount;
        @SerializedName("NoofItems")
        @Expose
        private Long noofItems;
        @SerializedName("Quantity")
        @Expose
        private Long quantity;
        @SerializedName("DiscountAmount")
        @Expose
        private float discountAmount;
        @SerializedName("GrandTotalAmount")
        @Expose
        private float grandTotalAmount;
        @SerializedName("PreviousOutstanding")
        @Expose
        private float previousOutstanding;
        @SerializedName("AmountPaid")
        @Expose
        private float amountPaid;
        @SerializedName("OutStandingBalance")
        @Expose
        private float outStandingBalance;
        @SerializedName("TypeId")
        @Expose
        private Long typeId;
        @SerializedName("SKUId")
        @Expose
        private int sKUId;
        @SerializedName("Carton")
        @Expose
        private Long carton;
        @SerializedName("Pack")
        @Expose
        private Long pack;
        @SerializedName("FreeCarton")
        @Expose
        private Long freeCarton;
        @SerializedName("FreePack")
        @Expose
        private Long freePack;
        @SerializedName("CartonPrice")
        @Expose
        private float cartonPrice;
        @SerializedName("PackPrice")
        @Expose
        private float packPrice;
        @SerializedName("outstandingBalanceid")
        @Expose
        private String outstandingBalanceid;
        @SerializedName("MobileNo")
        @Expose
        private String MobileNo;
        @SerializedName("Address")
        @Expose
        private String Address;
        @SerializedName("Casier")
        @Expose
        private String Casier;

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String mobileNo) {
            MobileNo = mobileNo;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getCasier() {
            return Casier;
        }

        public void setCasier(String casier) {
            Casier = casier;
        }

        public String getOutstandingBalanceid() {
            return outstandingBalanceid;
        }

        public void setOutstandingBalanceid(String outstandingBalanceid) {
            this.outstandingBalanceid = outstandingBalanceid;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Long getTrackingNo() {
            return trackingNo;
        }

        public void setTrackingNo(Long trackingNo) {
            this.trackingNo = trackingNo;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getInvoiceNo() {
            return invoiceNo;
        }

        public void setInvoiceNo(String invoiceNo) {
            this.invoiceNo = invoiceNo;
        }

        public float getSubtotal() {
            return subtotal;
        }

        public void setSubtotal(float subtotal) {
            this.subtotal = subtotal;
        }

        public float getVatPercent() {
            return vatPercent;
        }

        public void setVatPercent(float vatPercent) {
            this.vatPercent = vatPercent;
        }

        public float getVatAmount() {
            return vatAmount;
        }

        public void setVatAmount(float vatAmount) {
            this.vatAmount = vatAmount;
        }

        public float getInvoiceAmount() {
            return invoiceAmount;
        }

        public void setInvoiceAmount(float invoiceAmount) {
            this.invoiceAmount = invoiceAmount;
        }

        public Long getNoofItems() {
            return noofItems;
        }

        public void setNoofItems(Long noofItems) {
            this.noofItems = noofItems;
        }

        public Long getQuantity() {
            return quantity;
        }

        public void setQuantity(Long quantity) {
            this.quantity = quantity;
        }

        public float getDiscountAmount() {
            return discountAmount;
        }

        public void setDiscountAmount(float discountAmount) {
            this.discountAmount = discountAmount;
        }

        public float getGrandTotalAmount() {
            return grandTotalAmount;
        }

        public void setGrandTotalAmount(float grandTotalAmount) {
            this.grandTotalAmount = grandTotalAmount;
        }

        public float getPreviousOutstanding() {
            return previousOutstanding;
        }

        public void setPreviousOutstanding(float previousOutstanding) {
            this.previousOutstanding = previousOutstanding;
        }

        public float getAmountPaid() {
            return amountPaid;
        }

        public void setAmountPaid(float amountPaid) {
            this.amountPaid = amountPaid;
        }

        public float getOutStandingBalance() {
            return outStandingBalance;
        }

        public void setOutStandingBalance(float outStandingBalance) {
            this.outStandingBalance = outStandingBalance;
        }

        public Long getTypeId() {
            return typeId;
        }

        public void setTypeId(Long typeId) {
            this.typeId = typeId;
        }

        public int getSKUId() {
            return sKUId;
        }

        public void setSKUId(int sKUId) {
            this.sKUId = sKUId;
        }

        public Long getCarton() {
            return carton;
        }

        public void setCarton(Long carton) {
            this.carton = carton;
        }

        public Long getPack() {
            return pack;
        }

        public void setPack(Long pack) {
            this.pack = pack;
        }

        public Long getFreeCarton() {
            return freeCarton;
        }

        public void setFreeCarton(Long freeCarton) {
            this.freeCarton = freeCarton;
        }

        public Long getFreePack() {
            return freePack;
        }

        public void setFreePack(Long freePack) {
            this.freePack = freePack;
        }

        public float getCartonPrice() {
            return cartonPrice;
        }

        public void setCartonPrice(float cartonPrice) {
            this.cartonPrice = cartonPrice;
        }

        public float getPackPrice() {
            return packPrice;
        }

        public void setPackPrice(float packPrice) {
            this.packPrice = packPrice;
        }
    }
}

package com.cs.billingpos.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DailySalesReport {


    @Expose
    @SerializedName("SalesReportsDetails")
    private List<SalesReportsDetails> SalesReportsDetails;


    public List<SalesReportsDetails> getSalesReportsDetails() {
        return SalesReportsDetails;
    }

    public void setSalesReportsDetails(List<SalesReportsDetails> SalesReportsDetails) {
        this.SalesReportsDetails = SalesReportsDetails;
    }

    public static class SalesReportsDetails {
        @Expose
        @SerializedName("totalsales")
        private double totalsales;
        @Expose
        @SerializedName("Packs")
        private int packs;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("TrackingNo")
        private long TrackingNo;

        public double getTotalsales() {
            return totalsales;
        }

        public void setTotalsales(double totalsales) {
            this.totalsales = totalsales;
        }

        public int getPacks() {
            return packs;
        }

        public void setPacks(int packs) {
            this.packs = packs;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public long getTrackingNo() {
            return TrackingNo;
        }

        public void setTrackingNo(long TrackingNo) {
            this.TrackingNo = TrackingNo;
        }
    }
}

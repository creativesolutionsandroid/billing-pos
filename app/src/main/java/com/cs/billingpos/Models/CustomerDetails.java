package com.cs.billingpos.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class CustomerDetails implements Serializable {

    String market_id, market_name;
    ArrayList<RegionlistCD> regions = new ArrayList<>();

    public String getMarket_id() {
        return market_id;
    }

    public void setMarket_id(String market_id) {
        this.market_id = market_id;
    }

    public String getMarket_name() {
        return market_name;
    }

    public void setMarket_name(String market_name) {
        this.market_name = market_name;
    }

    public ArrayList<RegionlistCD> getRegions() {
        return regions;
    }

    public void setRegions(ArrayList<RegionlistCD> regions) {
        this.regions = regions;
    }

    public static class RegionlistCD implements Serializable {

        String RID, Region1, MID;

        ArrayList<AreaListCD> areaListCDS = new ArrayList<>();
        public String getRID() {
            return RID;
        }

        public void setRID(String RID) {
            this.RID = RID;
        }

        public String getRegion1() {
            return Region1;
        }

        public void setRegion1(String region1) {
            Region1 = region1;
        }

        public String getMID() {
            return MID;
        }

        public void setMID(String MID) {
            this.MID = MID;
        }

        public ArrayList<AreaListCD> getAreaListCDS() {
            return areaListCDS;
        }

        public void setAreaListCDS(ArrayList<AreaListCD> areaListCDS) {
            this.areaListCDS = areaListCDS;
        }
    }

    public static class AreaListCD implements Serializable {

        String AID, Area1, MID, RID;
        ArrayList<CityListCD> cityListCDS = new ArrayList<>();

        public String getAID() {
            return AID;
        }

        public void setAID(String AID) {
            this.AID = AID;
        }

        public String getArea1() {
            return Area1;
        }

        public void setArea1(String area1) {
            Area1 = area1;
        }

        public String getMID() {
            return MID;
        }

        public void setMID(String MID) {
            this.MID = MID;
        }

        public String getRID() {
            return RID;
        }

        public void setRID(String RID) {
            this.RID = RID;
        }

        public ArrayList<CityListCD> getCityListCDS() {
            return cityListCDS;
        }

        public void setCityListCDS(ArrayList<CityListCD> cityListCDS) {
            this.cityListCDS = cityListCDS;
        }
    }

    public static class CityListCD implements Serializable{

        String CID, City1, AID, MID, RID;
        ArrayList<ZoneListCD> zoneListCDS = new ArrayList<>();

        public String getCID() {
            return CID;
        }

        public void setCID(String CID) {
            this.CID = CID;
        }

        public String getCity1() {
            return City1;
        }

        public void setCity1(String city1) {
            City1 = city1;
        }

        public String getAID() {
            return AID;
        }

        public void setAID(String AID) {
            this.AID = AID;
        }

        public String getMID() {
            return MID;
        }

        public void setMID(String MID) {
            this.MID = MID;
        }

        public String getRID() {
            return RID;
        }

        public void setRID(String RID) {
            this.RID = RID;
        }

        public ArrayList<ZoneListCD> getZoneListCDS() {
            return zoneListCDS;
        }

        public void setZoneListCDS(ArrayList<ZoneListCD> zoneListCDS) {
            this.zoneListCDS = zoneListCDS;
        }
    }

    public static class ZoneListCD implements Serializable {

        String ZID, Zone1, CID, City1, AID, MID, RID;
        ArrayList<ZoneListCD> zoneListCDS = new ArrayList<>();

        public String getZID() {
            return ZID;
        }

        public void setZID(String ZID) {
            this.ZID = ZID;
        }

        public String getZone1() {
            return Zone1;
        }

        public void setZone1(String zone1) {
            Zone1 = zone1;
        }

        public String getCID() {
            return CID;
        }

        public void setCID(String CID) {
            this.CID = CID;
        }

        public String getCity1() {
            return City1;
        }

        public void setCity1(String city1) {
            City1 = city1;
        }

        public String getAID() {
            return AID;
        }

        public void setAID(String AID) {
            this.AID = AID;
        }

        public String getMID() {
            return MID;
        }

        public void setMID(String MID) {
            this.MID = MID;
        }

        public String getRID() {
            return RID;
        }

        public void setRID(String RID) {
            this.RID = RID;
        }

        public ArrayList<ZoneListCD> getZoneListCDS() {
            return zoneListCDS;
        }

        public void setZoneListCDS(ArrayList<ZoneListCD> zoneListCDS) {
            this.zoneListCDS = zoneListCDS;
        }
    }
}

package com.cs.billingpos.Models;

import java.util.ArrayList;
import java.util.List;

public class Customers {

    String Zone;
    Boolean isSeen;
    ArrayList<customerList> list;

    public ArrayList<customerList> getList() {
        return list;
    }

    public void setList(ArrayList<customerList> list) {
        this.list = list;
    }

    public String getZone() {
        return Zone;
    }

    public void setZone(String zone) {
        Zone = zone;
    }

    public Boolean getSeen() {
        return isSeen;
    }

    public void setSeen(Boolean seen) {
        isSeen = seen;
    }

    public static class  customerList{
        String customer, area, mobile, email, address;
        double outstanding, trackingNo;
        Boolean isSeen;

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public double getOutstanding() {
            return outstanding;
        }

        public void setOutstanding(double outstanding) {
            this.outstanding = outstanding;
        }

        public double getTrackingNo() {
            return trackingNo;
        }

        public void setTrackingNo(double trackingNo) {
            this.trackingNo = trackingNo;
        }

        public String getCustomer() {
            return customer;
        }

        public void setCustomer(String customer) {
            this.customer = customer;
        }

        public Boolean getSeen() {
            return isSeen;
        }

        public void setSeen(Boolean seen) {
            isSeen = seen;
        }
    }
}

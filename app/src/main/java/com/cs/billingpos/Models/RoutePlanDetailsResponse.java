package com.cs.billingpos.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RoutePlanDetailsResponse implements Serializable {

    @Expose
    @SerializedName("SalesInvoices")
    private List<Salesinvoices> salesinvoices;
    @Expose
    @SerializedName("BrandList")
    private List<BrandList> BrandList;
    @Expose
    @SerializedName("CategoryList")
    private List<CategoryList> CategoryList;
    @Expose
    @SerializedName("ZoneList")
    private List<ZoneList> ZoneList;
    @Expose
    @SerializedName("CityList")
    private List<CityList> CityList;
    @Expose
    @SerializedName("AreaList")
    private List<AreaList> AreaList;
    @Expose
    @SerializedName("RegionList")
    private List<RegionList> RegionList;
    @Expose
    @SerializedName("MarketList")
    private List<MarketList> MarketList;
    @SerializedName("RoutePlanDetails")
    @Expose
    private List<RoutePlanDetail> routePlanDetails = null;
    @SerializedName("TotaloutstandingBalance")
    @Expose
    private double totaloutstandingBalance;
    @SerializedName("Todaysales")
    @Expose
    private double todaysales;
    @SerializedName("SKUlist")
    @Expose
    private List<SKUlist> sKUlist = null;
    @SerializedName("NonTobaccoSKUList")
    @Expose
    private List<NonTobaccoSKUList> nonTobaccoSKUList = null;
    @SerializedName("CustomersList")
    @Expose
    private List<CustomersList> customersList = null;

    public List<Salesinvoices> getSalesinvoices() {
        return salesinvoices;
    }

    public void setSalesinvoices(List<Salesinvoices> salesinvoices) {
        this.salesinvoices = salesinvoices;
    }

    public List<BrandList> getBrandList() {
        return BrandList;
    }

    public void setBrandList(List<BrandList> BrandList) {
        this.BrandList = BrandList;
    }

    public static class Salesinvoices implements Serializable{
        @Expose
        @SerializedName("totalsales")
        private float totalsales;
        @Expose
        @SerializedName("InvoiceNo")
        private String invoiceno;
        @Expose
        @SerializedName("CustomerName")
        private String customername;
        @Expose
        @SerializedName("TrackingNo")
        private long trackingno;
        @Expose
        @SerializedName("id")
        private long id;

        public float getTotalsales() {
            return totalsales;
        }

        public void setTotalsales(float totalsales) {
            this.totalsales = totalsales;
        }

        public String getInvoiceno() {
            return invoiceno;
        }

        public void setInvoiceno(String invoiceno) {
            this.invoiceno = invoiceno;
        }

        public String getCustomername() {
            return customername;
        }

        public void setCustomername(String customername) {
            this.customername = customername;
        }

        public long getTrackingno() {
            return trackingno;
        }

        public void setTrackingno(long trackingno) {
            this.trackingno = trackingno;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }
    }


    public static class BrandList implements Serializable {
        @Expose
        @SerializedName("BrandName")
        private String BrandName;
        @Expose
        @SerializedName("BrandId")
        private int BrandId;

        public String getBrandName() {
            return BrandName;
        }

        public void setBrandName(String BrandName) {
            this.BrandName = BrandName;
        }

        public int getBrandId() {
            return BrandId;
        }

        public void setBrandId(int BrandId) {
            this.BrandId = BrandId;
        }
    }


    public List<CategoryList> getCategoryList() {
        return CategoryList;
    }

    public void setCategoryList(List<CategoryList> CategoryList) {
        this.CategoryList = CategoryList;
    }

    public List<ZoneList> getZoneList() {
        return ZoneList;
    }

    public void setZoneList(List<ZoneList> ZoneList) {
        this.ZoneList = ZoneList;
    }

    public List<CityList> getCityList() {
        return CityList;
    }

    public void setCityList(List<CityList> CityList) {
        this.CityList = CityList;
    }

    public List<AreaList> getAreaList() {
        return AreaList;
    }

    public void setAreaList(List<AreaList> AreaList) {
        this.AreaList = AreaList;
    }

    public List<RegionList> getRegionList() {
        return RegionList;
    }

    public void setRegionList(List<RegionList> RegionList) {
        this.RegionList = RegionList;
    }

    public List<MarketList> getMarketList() {
        return MarketList;
    }

    public void setMarketList(List<MarketList> MarketList) {
        this.MarketList = MarketList;
    }

    public List<RoutePlanDetail> getRoutePlanDetails() {
        return routePlanDetails;
    }

    public void setRoutePlanDetails(List<RoutePlanDetail> routePlanDetails) {
        this.routePlanDetails = routePlanDetails;
    }

    public double getTotaloutstandingBalance() {
        return totaloutstandingBalance;
    }

    public void setTotaloutstandingBalance(double totaloutstandingBalance) {
        this.totaloutstandingBalance = totaloutstandingBalance;
    }

    public double getTodaysales() {
        return todaysales;
    }

    public void setTodaysales(double todaysales) {
        this.todaysales = todaysales;
    }

    public List<SKUlist> getSKUlist() {
        return sKUlist;
    }

    public void setSKUlist(List<SKUlist> sKUlist) {
        this.sKUlist = sKUlist;
    }

    public List<NonTobaccoSKUList> getNonTobaccoSKUList() {
        return nonTobaccoSKUList;
    }

    public void setNonTobaccoSKUList(List<NonTobaccoSKUList> nonTobaccoSKUList) {
        this.nonTobaccoSKUList = nonTobaccoSKUList;
    }

    public List<CustomersList> getCustomersList() {
        return customersList;
    }

    public void setCustomersList(List<CustomersList> customersList) {
        this.customersList = customersList;
    }

    public static class CategoryList implements Serializable {
        @Expose
        @SerializedName("SKUCategoryName")
        private String SKUCategoryName;
        @Expose
        @SerializedName("SKUCategoryId")
        private int SKUCategoryId;

        public String getSKUCategoryName() {
            return SKUCategoryName;
        }

        public void setSKUCategoryName(String SKUCategoryName) {
            this.SKUCategoryName = SKUCategoryName;
        }

        public int getSKUCategoryId() {
            return SKUCategoryId;
        }

        public void setSKUCategoryId(int SKUCategoryId) {
            this.SKUCategoryId = SKUCategoryId;
        }
    }

    public static class ZoneList implements Serializable {
        @Expose
        @SerializedName("CID")
        private int CID;
        @Expose
        @SerializedName("AID")
        private int AID;
        @Expose
        @SerializedName("RID")
        private int RID;
        @Expose
        @SerializedName("MID")
        private int MID;
        @Expose
        @SerializedName("Zone1")
        private String Zone1;
        @Expose
        @SerializedName("ZID")
        private int ZID;

        public int getCID() {
            return CID;
        }

        public void setCID(int CID) {
            this.CID = CID;
        }

        public int getAID() {
            return AID;
        }

        public void setAID(int AID) {
            this.AID = AID;
        }

        public int getRID() {
            return RID;
        }

        public void setRID(int RID) {
            this.RID = RID;
        }

        public int getMID() {
            return MID;
        }

        public void setMID(int MID) {
            this.MID = MID;
        }

        public String getZone1() {
            return Zone1;
        }

        public void setZone1(String Zone1) {
            this.Zone1 = Zone1;
        }

        public int getZID() {
            return ZID;
        }

        public void setZID(int ZID) {
            this.ZID = ZID;
        }
    }

    public static class CityList implements Serializable {
        @Expose
        @SerializedName("AID")
        private int AID;
        @Expose
        @SerializedName("RID")
        private int RID;
        @Expose
        @SerializedName("MID")
        private int MID;
        @Expose
        @SerializedName("City1")
        private String City1;
        @Expose
        @SerializedName("CID")
        private int CID;

        public int getAID() {
            return AID;
        }

        public void setAID(int AID) {
            this.AID = AID;
        }

        public int getRID() {
            return RID;
        }

        public void setRID(int RID) {
            this.RID = RID;
        }

        public int getMID() {
            return MID;
        }

        public void setMID(int MID) {
            this.MID = MID;
        }

        public String getCity1() {
            return City1;
        }

        public void setCity1(String City1) {
            this.City1 = City1;
        }

        public int getCID() {
            return CID;
        }

        public void setCID(int CID) {
            this.CID = CID;
        }
    }

    public static class AreaList implements Serializable {
        @Expose
        @SerializedName("RID")
        private int RID;
        @Expose
        @SerializedName("MID")
        private int MID;
        @Expose
        @SerializedName("Area1")
        private String Area1;
        @Expose
        @SerializedName("AID")
        private int AID;

        public int getRID() {
            return RID;
        }

        public void setRID(int RID) {
            this.RID = RID;
        }

        public int getMID() {
            return MID;
        }

        public void setMID(int MID) {
            this.MID = MID;
        }

        public String getArea1() {
            return Area1;
        }

        public void setArea1(String Area1) {
            this.Area1 = Area1;
        }

        public int getAID() {
            return AID;
        }

        public void setAID(int AID) {
            this.AID = AID;
        }
    }

    public static class RegionList implements Serializable {
        @Expose
        @SerializedName("MID")
        private int MID;
        @Expose
        @SerializedName("Region1")
        private String Region1;
        @Expose
        @SerializedName("RID")
        private int RID;

        public int getMID() {
            return MID;
        }

        public void setMID(int MID) {
            this.MID = MID;
        }

        public String getRegion1() {
            return Region1;
        }

        public void setRegion1(String Region1) {
            this.Region1 = Region1;
        }

        public int getRID() {
            return RID;
        }

        public void setRID(int RID) {
            this.RID = RID;
        }
    }

    public static class MarketList implements Serializable {
        @Expose
        @SerializedName("Market1")
        private String Market1;
        @Expose
        @SerializedName("MID")
        private int MID;

        public String getMarket1() {
            return Market1;
        }

        public void setMarket1(String Market1) {
            this.Market1 = Market1;
        }

        public int getMID() {
            return MID;
        }

        public void setMID(int MID) {
            this.MID = MID;
        }
    }

    public class CustomersList implements Serializable {

        @SerializedName("PID")
        @Expose
        private Integer pID;
        @SerializedName("TrackingNo")
        @Expose
        private long trackingNo;
        @SerializedName("OutletName")
        @Expose
        private String outletName;
        @SerializedName("DistrictName")
        @Expose
        private String districtName;
        @SerializedName("CityID")
        @Expose
        private Integer cityID;
        @SerializedName("MID")
        @Expose
        private Integer mID;
        @SerializedName("Market")
        @Expose
        private String market;
        @SerializedName("RID")
        @Expose
        private Integer rID;
        @SerializedName("Region")
        @Expose
        private String region;
        @SerializedName("AID")
        @Expose
        private Integer aID;
        @SerializedName("Area")
        @Expose
        private String area;
        @SerializedName("City")
        @Expose
        private String city;
        @SerializedName("ZoneID")
        @Expose
        private Integer zoneID;
        @SerializedName("Zone")
        @Expose
        private String zone;
        @SerializedName("Latitude")
        @Expose
        private Double latitude;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;
        @SerializedName("MobileNo")
        @Expose
        private String mobileNo;
        @SerializedName("Email")
        @Expose
        private String email;
        @SerializedName("OutStandingBalance")
        @Expose
        private double outStandingBalance;
        @SerializedName("isActive")
        @Expose
        private Boolean isActive;
        @SerializedName("DayID")
        @Expose
        private Integer dayID;
        @SerializedName("MonID")
        @Expose
        private Integer monID;
        @SerializedName("YearID")
        @Expose
        private Integer yearID;
        @SerializedName("SalesmanName")
        @Expose
        private String salesmanName;
        private boolean isSeen;


        public boolean isSeen() {
            return isSeen;
        }

        public void setSeen(boolean seen) {
            isSeen = seen;
        }


        public Integer getpID() {
            return pID;
        }

        public void setpID(Integer pID) {
            this.pID = pID;
        }

        public Integer getmID() {
            return mID;
        }

        public void setmID(Integer mID) {
            this.mID = mID;
        }

        public Integer getrID() {
            return rID;
        }

        public void setrID(Integer rID) {
            this.rID = rID;
        }

        public Integer getaID() {
            return aID;
        }

        public void setaID(Integer aID) {
            this.aID = aID;
        }

        public Boolean getActive() {
            return isActive;
        }

        public void setActive(Boolean active) {
            isActive = active;
        }

        public Integer getPID() {
            return pID;
        }

        public void setPID(Integer pID) {
            this.pID = pID;
        }

        public long getTrackingNo() {
            return trackingNo;
        }

        public void setTrackingNo(long trackingNo) {
            this.trackingNo = trackingNo;
        }

        public String getOutletName() {
            return outletName;
        }

        public void setOutletName(String outletName) {
            this.outletName = outletName;
        }

        public String getDistrictName() {
            return districtName;
        }

        public void setDistrictName(String districtName) {
            this.districtName = districtName;
        }

        public Integer getCityID() {
            return cityID;
        }

        public void setCityID(Integer cityID) {
            this.cityID = cityID;
        }

        public Integer getMID() {
            return mID;
        }

        public void setMID(Integer mID) {
            this.mID = mID;
        }

        public String getMarket() {
            return market;
        }

        public void setMarket(String market) {
            this.market = market;
        }

        public Integer getRID() {
            return rID;
        }

        public void setRID(Integer rID) {
            this.rID = rID;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public Integer getAID() {
            return aID;
        }

        public void setAID(Integer aID) {
            this.aID = aID;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public Integer getZoneID() {
            return zoneID;
        }

        public void setZoneID(Integer zoneID) {
            this.zoneID = zoneID;
        }

        public String getZone() {
            return zone;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public double getOutStandingBalance() {
            return outStandingBalance;
        }

        public void setOutStandingBalance(double outStandingBalance) {
            this.outStandingBalance = outStandingBalance;
        }

        public Boolean getIsActive() {
            return isActive;
        }

        public void setIsActive(Boolean isActive) {
            this.isActive = isActive;
        }

        public Integer getDayID() {
            return dayID;
        }

        public void setDayID(Integer dayID) {
            this.dayID = dayID;
        }

        public Integer getMonID() {
            return monID;
        }

        public void setMonID(Integer monID) {
            this.monID = monID;
        }

        public Integer getYearID() {
            return yearID;
        }

        public void setYearID(Integer yearID) {
            this.yearID = yearID;
        }

        public String getSalesmanName() {
            return salesmanName;
        }

        public void setSalesmanName(String salesmanName) {
            this.salesmanName = salesmanName;
        }
    }

    public class NonTobaccoSKUList implements Serializable {

        @SerializedName("SKUId")
        @Expose
        private Integer sKUId;
        @SerializedName("SalesmanId")
        @Expose
        private Integer salesmanId;
        @SerializedName("SalesmanName")
        @Expose
        private String salesmanName;
        @SerializedName("CategoryId")
        @Expose
        private Integer categoryId;
        @SerializedName("SKUCategoryName")
        @Expose
        private String sKUCategoryName;
        @SerializedName("SKUName")
        @Expose
        private String sKUName;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("PackImage")
        @Expose
        private String packImage;
        @SerializedName("CartonPrice")
        @Expose
        private Integer cartonPrice;
        @SerializedName("PackPrice")
        @Expose
        private Integer packPrice;
        @SerializedName("CartonBalance")
        @Expose
        private Integer cartonBalance;
        @SerializedName("PackBalance")
        @Expose
        private Integer packBalance;
        @SerializedName("PackPerCarton")
        @Expose
        private Integer PackPerCarton;
        @SerializedName("MeasureUnit")
        @Expose
        private String MeasureUnit;

        public Integer getsKUId() {
            return sKUId;
        }

        public void setsKUId(Integer sKUId) {
            this.sKUId = sKUId;
        }

        public String getsKUCategoryName() {
            return sKUCategoryName;
        }

        public void setsKUCategoryName(String sKUCategoryName) {
            this.sKUCategoryName = sKUCategoryName;
        }

        public String getsKUName() {
            return sKUName;
        }

        public void setsKUName(String sKUName) {
            this.sKUName = sKUName;
        }

        public Integer getPackPerCarton() {
            return PackPerCarton;
        }

        public void setPackPerCarton(Integer packPerCarton) {
            PackPerCarton = packPerCarton;
        }

        public String getMeasureUnit() {
            return MeasureUnit;
        }

        public void setMeasureUnit(String measureUnit) {
            MeasureUnit = measureUnit;
        }

        public Integer getSKUId() {
            return sKUId;
        }

        public void setSKUId(Integer sKUId) {
            this.sKUId = sKUId;
        }

        public Integer getSalesmanId() {
            return salesmanId;
        }

        public void setSalesmanId(Integer salesmanId) {
            this.salesmanId = salesmanId;
        }

        public String getSalesmanName() {
            return salesmanName;
        }

        public void setSalesmanName(String salesmanName) {
            this.salesmanName = salesmanName;
        }

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public String getSKUCategoryName() {
            return sKUCategoryName;
        }

        public void setSKUCategoryName(String sKUCategoryName) {
            this.sKUCategoryName = sKUCategoryName;
        }

        public String getSKUName() {
            return sKUName;
        }

        public void setSKUName(String sKUName) {
            this.sKUName = sKUName;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getPackImage() {
            return packImage;
        }

        public void setPackImage(String packImage) {
            this.packImage = packImage;
        }

        public Integer getCartonPrice() {
            return cartonPrice;
        }

        public void setCartonPrice(Integer cartonPrice) {
            this.cartonPrice = cartonPrice;
        }

        public Integer getPackPrice() {
            return packPrice;
        }

        public void setPackPrice(Integer packPrice) {
            this.packPrice = packPrice;
        }

        public Integer getCartonBalance() {
            return cartonBalance;
        }

        public void setCartonBalance(Integer cartonBalance) {
            this.cartonBalance = cartonBalance;
        }

        public Integer getPackBalance() {
            return packBalance;
        }

        public void setPackBalance(Integer packBalance) {
            this.packBalance = packBalance;
        }

    }

    public class RoutePlanDetail implements Serializable {

        @SerializedName("SalesmanId")
        @Expose
        private Integer salesmanId;
        @SerializedName("EmpName")
        @Expose
        private String empName;
        @SerializedName("Mobile")
        @Expose
        private String Mobile;
        @SerializedName("TrackingNo")
        @Expose
        private long trackingNo;
        @SerializedName("OutletName")
        @Expose
        private String outletName;
        @SerializedName("Latitude")
        @Expose
        private Double latitude;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;
        @SerializedName("StartDate")
        @Expose
        private String startDate;
        @SerializedName("EndDate")
        @Expose
        private String endDate;
        @SerializedName("OutStandingBalance")
        @Expose
        private float outStandingBalance;
        @SerializedName("market")
        @Expose
        private String market;
        @SerializedName("Region")
        @Expose
        private String region;
        @SerializedName("area")
        @Expose
        private String area;
        @SerializedName("City")
        @Expose
        private String city;
        @SerializedName("Zone")
        @Expose
        private String zone;
        @SerializedName("MID")
        @Expose
        private Integer mID;
        @SerializedName("RID")
        @Expose
        private Integer rID;
        @SerializedName("AID")
        @Expose
        private Integer aID;
        @SerializedName("CityID")
        @Expose
        private Integer cityID;
        @SerializedName("ZoneID")
        @Expose
        private Integer zoneID;
        @SerializedName("WholesalerName")
        @Expose
        private String WholesalerName;
        @SerializedName("VATNumber")
        @Expose
        private String VATNumber;
        @SerializedName("StreatName")
        @Expose
        private String StreatName;

        public String getWholesalerName() {
            return WholesalerName;
        }

        public void setWholesalerName(String wholesalerName) {
            WholesalerName = wholesalerName;
        }

        public String getVATNumber() {
            return VATNumber;
        }

        public void setVATNumber(String VATNumber) {
            this.VATNumber = VATNumber;
        }

        public String getStreatName() {
            return StreatName;
        }

        public void setStreatName(String streatName) {
            StreatName = streatName;
        }

        public String getMobile() {
            return Mobile;
        }

        public void setMobile(String mobile) {
            Mobile = mobile;
        }

        public Integer getSalesmanId() {
            return salesmanId;
        }

        public void setSalesmanId(Integer salesmanId) {
            this.salesmanId = salesmanId;
        }

        public String getEmpName() {
            return empName;
        }

        public void setEmpName(String empName) {
            this.empName = empName;
        }

        public long getTrackingNo() {
            return trackingNo;
        }

        public void setTrackingNo(long trackingNo) {
            this.trackingNo = trackingNo;
        }

        public String getOutletName() {
            return outletName;
        }

        public void setOutletName(String outletName) {
            this.outletName = outletName;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public float getOutStandingBalance() {
            return outStandingBalance;
        }

        public void setOutStandingBalance(float outStandingBalance) {
            this.outStandingBalance = outStandingBalance;
        }

        public String getMarket() {
            return market;
        }

        public void setMarket(String market) {
            this.market = market;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getZone() {
            return zone;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public Integer getMID() {
            return mID;
        }

        public void setMID(Integer mID) {
            this.mID = mID;
        }

        public Integer getRID() {
            return rID;
        }

        public void setRID(Integer rID) {
            this.rID = rID;
        }

        public Integer getAID() {
            return aID;
        }

        public void setAID(Integer aID) {
            this.aID = aID;
        }

        public Integer getCityID() {
            return cityID;
        }

        public void setCityID(Integer cityID) {
            this.cityID = cityID;
        }

        public Integer getZoneID() {
            return zoneID;
        }

        public void setZoneID(Integer zoneID) {
            this.zoneID = zoneID;
        }

    }


    public class SKUlist implements Serializable {

        @SerializedName("MasterSKUTypeid")
        @Expose
        private Integer masterSKUTypeid;
        @SerializedName("MasterSKUType")
        @Expose
        private String masterSKUType;
        @SerializedName("SKUCategoryId")
        @Expose
        private Integer sKUCategoryId;
        @SerializedName("SKUCategoryName")
        @Expose
        private String sKUCategoryName;
        @SerializedName("CompanyId")
        @Expose
        private Integer companyId;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("SKUId")
        @Expose
        private Integer sKUId;
        @SerializedName("SKUName")
        @Expose
        private String sKUName;
        @SerializedName("packImage")
        @Expose
        private String packImage;
        @SerializedName("CartonPrice")
        @Expose
        private float cartonPrice;
        @SerializedName("PackPrice")
        @Expose
        private float packPrice;
        @SerializedName("CartonBalance")
        @Expose
        private Integer cartonBalance;
        @SerializedName("PackBalance")
        @Expose
        private Integer packBalance;
        @SerializedName("PacksPerCarton")
        @Expose
        private Integer PacksPerCarton;
        @SerializedName("BrandId")
        @Expose
        private Integer BrandId;

        public Integer getBrandId() {
            return BrandId;
        }

        public void setBrandId(Integer brandId) {
            BrandId = brandId;
        }

        public Integer getPacksPerCarton() {
            return PacksPerCarton;
        }

        public void setPacksPerCarton(Integer packsPerCarton) {
            PacksPerCarton = packsPerCarton;
        }

        public Integer getMasterSKUTypeid() {
            return masterSKUTypeid;
        }

        public void setMasterSKUTypeid(Integer masterSKUTypeid) {
            this.masterSKUTypeid = masterSKUTypeid;
        }

        public String getMasterSKUType() {
            return masterSKUType;
        }

        public void setMasterSKUType(String masterSKUType) {
            this.masterSKUType = masterSKUType;
        }

        public Integer getSKUCategoryId() {
            return sKUCategoryId;
        }

        public void setSKUCategoryId(Integer sKUCategoryId) {
            this.sKUCategoryId = sKUCategoryId;
        }

        public String getSKUCategoryName() {
            return sKUCategoryName;
        }

        public void setSKUCategoryName(String sKUCategoryName) {
            this.sKUCategoryName = sKUCategoryName;
        }

        public Integer getCompanyId() {
            return companyId;
        }

        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public Integer getSKUId() {
            return sKUId;
        }

        public void setSKUId(Integer sKUId) {
            this.sKUId = sKUId;
        }

        public String getSKUName() {
            return sKUName;
        }

        public void setSKUName(String sKUName) {
            this.sKUName = sKUName;
        }

        public String getPackImage() {
            return packImage;
        }

        public void setPackImage(String packImage) {
            this.packImage = packImage;
        }

        public float getCartonPrice() {
            return cartonPrice;
        }

        public void setCartonPrice(float cartonPrice) {
            this.cartonPrice = cartonPrice;
        }

        public float getPackPrice() {
            return packPrice;
        }

        public void setPackPrice(float packPrice) {
            this.packPrice = packPrice;
        }

        public Integer getCartonBalance() {
            return cartonBalance;
        }

        public void setCartonBalance(Integer cartonBalance) {
            this.cartonBalance = cartonBalance;
        }

        public Integer getPackBalance() {
            return packBalance;
        }

        public void setPackBalance(Integer packBalance) {
            this.packBalance = packBalance;
        }
    }
}

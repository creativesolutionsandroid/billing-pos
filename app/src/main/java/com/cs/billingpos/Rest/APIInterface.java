package com.cs.billingpos.Rest;



import com.cs.billingpos.Models.ChangePasswordResponse;
import com.cs.billingpos.Models.DailySalesReport;
import com.cs.billingpos.Models.DailyStockResponse;
import com.cs.billingpos.Models.InsertProductDetails;
import com.cs.billingpos.Models.InsertPromoterResponse;
import com.cs.billingpos.Models.InsertStockDetails;
import com.cs.billingpos.Models.InvoiceDetails;
import com.cs.billingpos.Models.RoutePlanDetailsResponse;
import com.cs.billingpos.Models.SignInResponce;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("api/UserDetails")
    Call<SignInResponce> signIn(@Query("UserName") String username, @Query("Password") String password);

    @GET("api/UserDetails")
    Call<ChangePasswordResponse> changePassword(@Query("UserId") String username, @Query("oldPassword") String oldpassword
            , @Query("newPassword") String newpassword);

    @GET("api/IVSRoutePlan")
    Call<RoutePlanDetailsResponse> dashboard(@Query("date") String date, @Query("ExecutorId") String executorId);

    @POST("Invoice/InsertIVSVisits")
    Call<InsertPromoterResponse> insertInvoice(@Body RequestBody body);

    @POST("Invoice/InsertIVSCollection")
    Call<InsertPromoterResponse> insertCollection(@Body RequestBody body);

    @POST("Invoice/InsertProducts")
    Call<InsertProductDetails> GetInsertproduction(@Body RequestBody body);

    @POST("Invoice/InsertAddStock")
    Call<InsertStockDetails> GetInsertstock(@Body RequestBody body);

    @POST("Invoice/InsertStockBalance")
    Call<InsertStockDetails> InsertstockBalance(@Body RequestBody body);

    @POST("Invoice/InsertCustomerDetails")
    Call<InsertProductDetails> GetCustomerdetails(@Body RequestBody body);

    @GET("GetSalesReport")
    Call<DailySalesReport> dailysalesreport(@Query("ExecutorId") String executorId, @Query("Startdate") String start_date,
                                            @Query("Enddate") String end_date, @Query("Skuid") String skuid, @Query("Brandid") String brandid);

    @GET("GetStockReport")
    Call<DailyStockResponse> dailystockreport(@Query("ExecutorId") String executorId, @Query("Startdate") String start_date,
                                              @Query("Enddate") String end_date);

    @GET("EditLastInvoice")
    Call<InvoiceDetails> editLastInvoice(@Query("id") String id, @Query("ExecutorId") String executorId);

 }

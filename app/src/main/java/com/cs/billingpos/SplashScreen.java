package com.cs.billingpos;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import android.util.Log;

import com.cs.billingpos.Activities.DashboardActivity;
import com.cs.billingpos.Activities.SignInActivity;

/**
 * Created by SKT on 31-12-2015.
 */
public class SplashScreen extends Activity {
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1000;
    SharedPreferences userPrefs;
    String stage;
    Context context;
    BroadcastReceiver mRegistrationBroadcastReceiver;
    public static String regId = "";
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.splash_screen);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        stage = userPrefs.getString("stage", "0");
        context = getApplicationContext();
        if (userPrefs.getString("EmpId","").equals("")) {
            Intent i = new Intent(SplashScreen.this, SignInActivity.class);
            startActivity(i);
        }
        else {
            Intent i = new Intent(SplashScreen.this, DashboardActivity.class);
            startActivity(i);
        }
        finish();
    }
}
